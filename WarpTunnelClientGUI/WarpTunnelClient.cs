using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Text;
using System.Net.Sockets;
using WarpTunnel;
using ConnectionStatistics;
using WarpTunnelUtils;
using WarpTunnel.WarpTunnelUtils;

namespace WarpTunnelClientGUI
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class WarpTunnelClient : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Timer timerControll;
		private ClientTunnel Htc = null;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabClient;
		private System.Windows.Forms.TabPage tabOptions;
		private System.Windows.Forms.TabPage tabStatistics;
		private System.Windows.Forms.GroupBox groupProxy;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txProxyPort;
		private System.Windows.Forms.TextBox txProxyAddress;
		private System.Windows.Forms.GroupBox groupAuth;
		private System.Windows.Forms.TextBox txProxyPass;
		private System.Windows.Forms.TextBox txProxyUser;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox chkProxyAuth;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox chkProxy;
		private System.Windows.Forms.GroupBox groupENDPoints;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txLocPort;
		private System.Windows.Forms.TextBox txLocAddress;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txRemPort;
		private System.Windows.Forms.TextBox txRemAddress;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox chkCompress;
		private System.Windows.Forms.CheckBox chkCompatible;
		private System.Windows.Forms.GroupBox groupLogger;
		private System.Windows.Forms.ComboBox cmbLoggerLevel;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox chkLogger;
		private System.Windows.Forms.GroupBox groupAgent;
		private System.Windows.Forms.GroupBox groupSettings;
		private System.Windows.Forms.Button btloadsettings;
		private System.Windows.Forms.Button btSaveSettings;
		private System.Windows.Forms.Button btStop;
		private System.Windows.Forms.Button btStart;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox cmbProxyAuth;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.GroupBox groupOptions;
		private System.Windows.Forms.GroupBox groupContentLength;
		private System.Windows.Forms.TrackBar trackContent;
		private System.Windows.Forms.TextBox txValueContent;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox cmbProtocol;
		private System.Windows.Forms.GroupBox groupProxyAdvance;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox txPBufferValue;
		private System.Windows.Forms.TextBox txPBufferTimeout;
		private System.Windows.Forms.TrackBar trackProxyBuffer;
		private System.Windows.Forms.TrackBar trackProxyTimeout;
		private System.Windows.Forms.CheckBox chkTunnelAuth;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txTunnelUsername;
		private System.Windows.Forms.TextBox txTunnelPassword;
		private System.Windows.Forms.GroupBox groupTunnelDest;
		private System.Windows.Forms.GroupBox gruopTunnelAuth;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox txTunnelDestHost;
		private System.Windows.Forms.TextBox txTunnelDestPort;
		private System.Windows.Forms.GroupBox groupStat;
		private System.Windows.Forms.Button btRefresh;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ListView lvStat;
		private System.Windows.Forms.ColumnHeader clmName1;
		private System.Windows.Forms.ColumnHeader clmValue1;
		private System.Windows.Forms.ColumnHeader clmName2;
		private System.Windows.Forms.ColumnHeader clmValue2;
		private System.Windows.Forms.ListView lvConn;
		private System.Windows.Forms.ColumnHeader clmID;
		private System.Windows.Forms.ColumnHeader clmSrvHost;
		private System.Windows.Forms.ColumnHeader clmSrvPort;
		private System.Windows.Forms.ColumnHeader clmDate;
		private System.Windows.Forms.CheckBox chkStats;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.CheckBox chkFixedDestAddress;
		private System.Windows.Forms.CheckBox chkLetProgDecideAddress;
		private System.Windows.Forms.ComboBox cmbAcceptConnLike;
		private System.Windows.Forms.GroupBox groupProxyEmu;
		private System.Windows.Forms.RadioButton radioSoftThread;
		private System.Windows.Forms.RadioButton radioHeavyThread;
		private System.Windows.Forms.ColumnHeader clmDstHost;
		private System.Windows.Forms.ColumnHeader clmDstPort;
		private System.Windows.Forms.CheckBox chkStrictProxy;
		private System.Windows.Forms.TextBox txUserAgent;
		private System.Windows.Forms.Button btClearStats;
		private System.Windows.Forms.CheckBox chkStealthMode;
		private System.Windows.Forms.CheckBox chkProxyAutoConf;
		private System.Windows.Forms.NotifyIcon nIcon;
		private System.Windows.Forms.CheckBox chkEncription;
		private ListViewColumnSorter lvSorter = new ListViewColumnSorter();

		public bool ifStartAtStartup = false;

		public WarpTunnelClient()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.lvConn.ListViewItemSorter = lvSorter;

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			this.txRemPort.Text = Convert.ToString(Tunnel.DEFAULT_HOST_PORT);
			this.cmbLoggerLevel.SelectedIndex = 0;
			this.cmbProxyAuth.SelectedIndex = 0;
			this.cmbProtocol.SelectedIndex = 0;
			this.cmbAcceptConnLike.SelectedIndex = 0;
			this.txValueContent.Text = Convert.ToString(Tunnel.DEFAULT_BUFFER_LENGTH);
			this.txPBufferValue.Text = Convert.ToString(this.trackProxyBuffer.Minimum);
			this.txPBufferTimeout.Text = Convert.ToString(this.trackProxyTimeout.Minimum);
			loadSettings();

			this.nIcon.Text = this.Text;
			this.nIcon.Icon = this.Icon;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(WarpTunnelClient));
			this.timerControll = new System.Windows.Forms.Timer(this.components);
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabClient = new System.Windows.Forms.TabPage();
			this.gruopTunnelAuth = new System.Windows.Forms.GroupBox();
			this.groupProxyEmu = new System.Windows.Forms.GroupBox();
			this.cmbAcceptConnLike = new System.Windows.Forms.ComboBox();
			this.label19 = new System.Windows.Forms.Label();
			this.chkLetProgDecideAddress = new System.Windows.Forms.CheckBox();
			this.groupTunnelDest = new System.Windows.Forms.GroupBox();
			this.chkFixedDestAddress = new System.Windows.Forms.CheckBox();
			this.txTunnelDestPort = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.txTunnelDestHost = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.txTunnelPassword = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txTunnelUsername = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.chkTunnelAuth = new System.Windows.Forms.CheckBox();
			this.groupSettings = new System.Windows.Forms.GroupBox();
			this.btloadsettings = new System.Windows.Forms.Button();
			this.btSaveSettings = new System.Windows.Forms.Button();
			this.btStop = new System.Windows.Forms.Button();
			this.btStart = new System.Windows.Forms.Button();
			this.groupProxy = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txProxyPort = new System.Windows.Forms.TextBox();
			this.txProxyAddress = new System.Windows.Forms.TextBox();
			this.groupAuth = new System.Windows.Forms.GroupBox();
			this.chkProxyAutoConf = new System.Windows.Forms.CheckBox();
			this.label10 = new System.Windows.Forms.Label();
			this.cmbProxyAuth = new System.Windows.Forms.ComboBox();
			this.txProxyPass = new System.Windows.Forms.TextBox();
			this.txProxyUser = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.chkProxyAuth = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.chkProxy = new System.Windows.Forms.CheckBox();
			this.groupENDPoints = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txLocPort = new System.Windows.Forms.TextBox();
			this.txLocAddress = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txRemPort = new System.Windows.Forms.TextBox();
			this.txRemAddress = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tabOptions = new System.Windows.Forms.TabPage();
			this.groupProxyAdvance = new System.Windows.Forms.GroupBox();
			this.trackProxyTimeout = new System.Windows.Forms.TrackBar();
			this.trackProxyBuffer = new System.Windows.Forms.TrackBar();
			this.txPBufferTimeout = new System.Windows.Forms.TextBox();
			this.txPBufferValue = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.groupContentLength = new System.Windows.Forms.GroupBox();
			this.txValueContent = new System.Windows.Forms.TextBox();
			this.trackContent = new System.Windows.Forms.TrackBar();
			this.groupOptions = new System.Windows.Forms.GroupBox();
			this.chkEncription = new System.Windows.Forms.CheckBox();
			this.chkStealthMode = new System.Windows.Forms.CheckBox();
			this.chkStrictProxy = new System.Windows.Forms.CheckBox();
			this.radioSoftThread = new System.Windows.Forms.RadioButton();
			this.radioHeavyThread = new System.Windows.Forms.RadioButton();
			this.chkStats = new System.Windows.Forms.CheckBox();
			this.cmbProtocol = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.chkCompress = new System.Windows.Forms.CheckBox();
			this.chkCompatible = new System.Windows.Forms.CheckBox();
			this.groupLogger = new System.Windows.Forms.GroupBox();
			this.cmbLoggerLevel = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.chkLogger = new System.Windows.Forms.CheckBox();
			this.groupAgent = new System.Windows.Forms.GroupBox();
			this.txUserAgent = new System.Windows.Forms.TextBox();
			this.tabStatistics = new System.Windows.Forms.TabPage();
			this.groupStat = new System.Windows.Forms.GroupBox();
			this.btClearStats = new System.Windows.Forms.Button();
			this.btRefresh = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lvStat = new System.Windows.Forms.ListView();
			this.clmName1 = new System.Windows.Forms.ColumnHeader();
			this.clmValue1 = new System.Windows.Forms.ColumnHeader();
			this.clmName2 = new System.Windows.Forms.ColumnHeader();
			this.clmValue2 = new System.Windows.Forms.ColumnHeader();
			this.lvConn = new System.Windows.Forms.ListView();
			this.clmID = new System.Windows.Forms.ColumnHeader();
			this.clmSrvHost = new System.Windows.Forms.ColumnHeader();
			this.clmSrvPort = new System.Windows.Forms.ColumnHeader();
			this.clmDstHost = new System.Windows.Forms.ColumnHeader();
			this.clmDstPort = new System.Windows.Forms.ColumnHeader();
			this.clmDate = new System.Windows.Forms.ColumnHeader();
			this.label11 = new System.Windows.Forms.Label();
			this.nIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.tabControl1.SuspendLayout();
			this.tabClient.SuspendLayout();
			this.gruopTunnelAuth.SuspendLayout();
			this.groupProxyEmu.SuspendLayout();
			this.groupTunnelDest.SuspendLayout();
			this.groupSettings.SuspendLayout();
			this.groupProxy.SuspendLayout();
			this.groupAuth.SuspendLayout();
			this.groupENDPoints.SuspendLayout();
			this.tabOptions.SuspendLayout();
			this.groupProxyAdvance.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackProxyTimeout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackProxyBuffer)).BeginInit();
			this.groupContentLength.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackContent)).BeginInit();
			this.groupOptions.SuspendLayout();
			this.groupLogger.SuspendLayout();
			this.groupAgent.SuspendLayout();
			this.tabStatistics.SuspendLayout();
			this.groupStat.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// timerControll
			// 
			this.timerControll.Tick += new System.EventHandler(this.timerControll_Tick);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabClient);
			this.tabControl1.Controls.Add(this.tabOptions);
			this.tabControl1.Controls.Add(this.tabStatistics);
			this.tabControl1.Location = new System.Drawing.Point(0, 8);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(560, 464);
			this.tabControl1.TabIndex = 1;
			this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
			// 
			// tabClient
			// 
			this.tabClient.Controls.Add(this.gruopTunnelAuth);
			this.tabClient.Controls.Add(this.groupSettings);
			this.tabClient.Controls.Add(this.btStop);
			this.tabClient.Controls.Add(this.btStart);
			this.tabClient.Controls.Add(this.groupProxy);
			this.tabClient.Controls.Add(this.groupENDPoints);
			this.tabClient.Location = new System.Drawing.Point(4, 22);
			this.tabClient.Name = "tabClient";
			this.tabClient.Size = new System.Drawing.Size(552, 438);
			this.tabClient.TabIndex = 0;
			this.tabClient.Text = "Client Settings";
			// 
			// gruopTunnelAuth
			// 
			this.gruopTunnelAuth.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.gruopTunnelAuth.Controls.Add(this.groupProxyEmu);
			this.gruopTunnelAuth.Controls.Add(this.groupTunnelDest);
			this.gruopTunnelAuth.Controls.Add(this.txTunnelPassword);
			this.gruopTunnelAuth.Controls.Add(this.label16);
			this.gruopTunnelAuth.Controls.Add(this.txTunnelUsername);
			this.gruopTunnelAuth.Controls.Add(this.label15);
			this.gruopTunnelAuth.Controls.Add(this.chkTunnelAuth);
			this.gruopTunnelAuth.Location = new System.Drawing.Point(8, 224);
			this.gruopTunnelAuth.Name = "gruopTunnelAuth";
			this.gruopTunnelAuth.Size = new System.Drawing.Size(536, 168);
			this.gruopTunnelAuth.TabIndex = 4;
			this.gruopTunnelAuth.TabStop = false;
			// 
			// groupProxyEmu
			// 
			this.groupProxyEmu.Controls.Add(this.cmbAcceptConnLike);
			this.groupProxyEmu.Controls.Add(this.label19);
			this.groupProxyEmu.Controls.Add(this.chkLetProgDecideAddress);
			this.groupProxyEmu.Enabled = false;
			this.groupProxyEmu.Location = new System.Drawing.Point(8, 56);
			this.groupProxyEmu.Name = "groupProxyEmu";
			this.groupProxyEmu.Size = new System.Drawing.Size(520, 48);
			this.groupProxyEmu.TabIndex = 6;
			this.groupProxyEmu.TabStop = false;
			// 
			// cmbAcceptConnLike
			// 
			this.cmbAcceptConnLike.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbAcceptConnLike.Enabled = false;
			this.cmbAcceptConnLike.Items.AddRange(new object[] {
																   "1 - HTTP/1.1 CONNECT",
																   "2 - SOCKS 4/4A",
																   "3 - SOCKS 5"});
			this.cmbAcceptConnLike.Location = new System.Drawing.Point(136, 20);
			this.cmbAcceptConnLike.Name = "cmbAcceptConnLike";
			this.cmbAcceptConnLike.Size = new System.Drawing.Size(304, 21);
			this.cmbAcceptConnLike.TabIndex = 2;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(16, 24);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(192, 16);
			this.label19.TabIndex = 1;
			this.label19.Text = "Client Emulate :";
			// 
			// chkLetProgDecideAddress
			// 
			this.chkLetProgDecideAddress.Location = new System.Drawing.Point(8, 0);
			this.chkLetProgDecideAddress.Name = "chkLetProgDecideAddress";
			this.chkLetProgDecideAddress.Size = new System.Drawing.Size(256, 16);
			this.chkLetProgDecideAddress.TabIndex = 0;
			this.chkLetProgDecideAddress.Text = "Use Variable Destination Point :";
			this.chkLetProgDecideAddress.CheckedChanged += new System.EventHandler(this.chkLetProgDecideAddress_CheckedChanged);
			// 
			// groupTunnelDest
			// 
			this.groupTunnelDest.Controls.Add(this.chkFixedDestAddress);
			this.groupTunnelDest.Controls.Add(this.txTunnelDestPort);
			this.groupTunnelDest.Controls.Add(this.label18);
			this.groupTunnelDest.Controls.Add(this.txTunnelDestHost);
			this.groupTunnelDest.Controls.Add(this.label17);
			this.groupTunnelDest.Enabled = false;
			this.groupTunnelDest.Location = new System.Drawing.Point(8, 112);
			this.groupTunnelDest.Name = "groupTunnelDest";
			this.groupTunnelDest.Size = new System.Drawing.Size(520, 48);
			this.groupTunnelDest.TabIndex = 5;
			this.groupTunnelDest.TabStop = false;
			// 
			// chkFixedDestAddress
			// 
			this.chkFixedDestAddress.Checked = true;
			this.chkFixedDestAddress.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkFixedDestAddress.Location = new System.Drawing.Point(8, 0);
			this.chkFixedDestAddress.Name = "chkFixedDestAddress";
			this.chkFixedDestAddress.Size = new System.Drawing.Size(200, 16);
			this.chkFixedDestAddress.TabIndex = 4;
			this.chkFixedDestAddress.Text = "Use Fixed Destination Address :";
			this.chkFixedDestAddress.CheckedChanged += new System.EventHandler(this.chkFixedDestAddress_CheckedChanged);
			// 
			// txTunnelDestPort
			// 
			this.txTunnelDestPort.Location = new System.Drawing.Point(424, 20);
			this.txTunnelDestPort.Name = "txTunnelDestPort";
			this.txTunnelDestPort.Size = new System.Drawing.Size(88, 20);
			this.txTunnelDestPort.TabIndex = 3;
			this.txTunnelDestPort.Text = "0";
			this.txTunnelDestPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txTunnelDestPort.TextChanged += new System.EventHandler(this.txTunnelDestPort_TextChanged);
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(416, 20);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(8, 16);
			this.label18.TabIndex = 2;
			this.label18.Text = ":";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txTunnelDestHost
			// 
			this.txTunnelDestHost.Location = new System.Drawing.Point(136, 20);
			this.txTunnelDestHost.Name = "txTunnelDestHost";
			this.txTunnelDestHost.Size = new System.Drawing.Size(280, 20);
			this.txTunnelDestHost.TabIndex = 1;
			this.txTunnelDestHost.Text = "";
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(8, 24);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(136, 16);
			this.label17.TabIndex = 0;
			this.label17.Text = "Tunnel Destination Host :";
			// 
			// txTunnelPassword
			// 
			this.txTunnelPassword.Enabled = false;
			this.txTunnelPassword.Location = new System.Drawing.Point(336, 28);
			this.txTunnelPassword.Name = "txTunnelPassword";
			this.txTunnelPassword.PasswordChar = '*';
			this.txTunnelPassword.Size = new System.Drawing.Size(188, 20);
			this.txTunnelPassword.TabIndex = 4;
			this.txTunnelPassword.Text = "";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(272, 32);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(64, 16);
			this.label16.TabIndex = 3;
			this.label16.Text = "Password :";
			// 
			// txTunnelUsername
			// 
			this.txTunnelUsername.Enabled = false;
			this.txTunnelUsername.Location = new System.Drawing.Point(72, 28);
			this.txTunnelUsername.Name = "txTunnelUsername";
			this.txTunnelUsername.Size = new System.Drawing.Size(188, 20);
			this.txTunnelUsername.TabIndex = 2;
			this.txTunnelUsername.Text = "";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(8, 32);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(64, 16);
			this.label15.TabIndex = 1;
			this.label15.Text = "Username :";
			// 
			// chkTunnelAuth
			// 
			this.chkTunnelAuth.Location = new System.Drawing.Point(8, 0);
			this.chkTunnelAuth.Name = "chkTunnelAuth";
			this.chkTunnelAuth.Size = new System.Drawing.Size(168, 16);
			this.chkTunnelAuth.TabIndex = 0;
			this.chkTunnelAuth.Text = "Use Tunnel Authentication :";
			this.chkTunnelAuth.CheckedChanged += new System.EventHandler(this.chkTunnelAuth_CheckedChanged);
			// 
			// groupSettings
			// 
			this.groupSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupSettings.Controls.Add(this.btloadsettings);
			this.groupSettings.Controls.Add(this.btSaveSettings);
			this.groupSettings.Location = new System.Drawing.Point(12, 392);
			this.groupSettings.Name = "groupSettings";
			this.groupSettings.Size = new System.Drawing.Size(272, 40);
			this.groupSettings.TabIndex = 5;
			this.groupSettings.TabStop = false;
			// 
			// btloadsettings
			// 
			this.btloadsettings.Location = new System.Drawing.Point(144, 10);
			this.btloadsettings.Name = "btloadsettings";
			this.btloadsettings.Size = new System.Drawing.Size(120, 24);
			this.btloadsettings.TabIndex = 1;
			this.btloadsettings.Text = "Load Settings";
			this.btloadsettings.Click += new System.EventHandler(this.btloadsettings_Click);
			// 
			// btSaveSettings
			// 
			this.btSaveSettings.Location = new System.Drawing.Point(8, 10);
			this.btSaveSettings.Name = "btSaveSettings";
			this.btSaveSettings.Size = new System.Drawing.Size(128, 24);
			this.btSaveSettings.TabIndex = 0;
			this.btSaveSettings.Text = "Save Settings";
			this.btSaveSettings.Click += new System.EventHandler(this.btSaveSettings_Click);
			// 
			// btStop
			// 
			this.btStop.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.btStop.Enabled = false;
			this.btStop.Location = new System.Drawing.Point(428, 400);
			this.btStop.Name = "btStop";
			this.btStop.Size = new System.Drawing.Size(120, 24);
			this.btStop.TabIndex = 7;
			this.btStop.Text = "Stop Client";
			this.btStop.Click += new System.EventHandler(this.btStop_Click);
			// 
			// btStart
			// 
			this.btStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.btStart.Location = new System.Drawing.Point(292, 400);
			this.btStart.Name = "btStart";
			this.btStart.Size = new System.Drawing.Size(128, 24);
			this.btStart.TabIndex = 6;
			this.btStart.Text = "Start Client";
			this.btStart.Click += new System.EventHandler(this.btStart_Click);
			// 
			// groupProxy
			// 
			this.groupProxy.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupProxy.Controls.Add(this.label9);
			this.groupProxy.Controls.Add(this.txProxyPort);
			this.groupProxy.Controls.Add(this.txProxyAddress);
			this.groupProxy.Controls.Add(this.groupAuth);
			this.groupProxy.Controls.Add(this.label6);
			this.groupProxy.Controls.Add(this.chkProxy);
			this.groupProxy.Location = new System.Drawing.Point(8, 88);
			this.groupProxy.Name = "groupProxy";
			this.groupProxy.Size = new System.Drawing.Size(536, 136);
			this.groupProxy.TabIndex = 3;
			this.groupProxy.TabStop = false;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(432, 24);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(16, 16);
			this.label9.TabIndex = 5;
			this.label9.Text = ":";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txProxyPort
			// 
			this.txProxyPort.Enabled = false;
			this.txProxyPort.Location = new System.Drawing.Point(448, 24);
			this.txProxyPort.MaxLength = 5;
			this.txProxyPort.Name = "txProxyPort";
			this.txProxyPort.Size = new System.Drawing.Size(80, 20);
			this.txProxyPort.TabIndex = 2;
			this.txProxyPort.Text = "0";
			this.txProxyPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txProxyPort.TextChanged += new System.EventHandler(this.txProxyPort_TextChanged);
			// 
			// txProxyAddress
			// 
			this.txProxyAddress.Enabled = false;
			this.txProxyAddress.Location = new System.Drawing.Point(120, 24);
			this.txProxyAddress.Name = "txProxyAddress";
			this.txProxyAddress.Size = new System.Drawing.Size(312, 20);
			this.txProxyAddress.TabIndex = 1;
			this.txProxyAddress.Text = "";
			// 
			// groupAuth
			// 
			this.groupAuth.Controls.Add(this.chkProxyAutoConf);
			this.groupAuth.Controls.Add(this.label10);
			this.groupAuth.Controls.Add(this.cmbProxyAuth);
			this.groupAuth.Controls.Add(this.txProxyPass);
			this.groupAuth.Controls.Add(this.txProxyUser);
			this.groupAuth.Controls.Add(this.label8);
			this.groupAuth.Controls.Add(this.label7);
			this.groupAuth.Controls.Add(this.chkProxyAuth);
			this.groupAuth.Enabled = false;
			this.groupAuth.Location = new System.Drawing.Point(8, 48);
			this.groupAuth.Name = "groupAuth";
			this.groupAuth.Size = new System.Drawing.Size(520, 80);
			this.groupAuth.TabIndex = 2;
			this.groupAuth.TabStop = false;
			// 
			// chkProxyAutoConf
			// 
			this.chkProxyAutoConf.Location = new System.Drawing.Point(8, 56);
			this.chkProxyAutoConf.Name = "chkProxyAutoConf";
			this.chkProxyAutoConf.Size = new System.Drawing.Size(152, 16);
			this.chkProxyAutoConf.TabIndex = 15;
			this.chkProxyAutoConf.Text = "Auto Proxy Configuration";
			this.chkProxyAutoConf.CheckedChanged += new System.EventHandler(this.chkProxyAutoConf_CheckedChanged);
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(176, 56);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(152, 16);
			this.label10.TabIndex = 14;
			this.label10.Text = "Proxy Authentication Mode :";
			// 
			// cmbProxyAuth
			// 
			this.cmbProxyAuth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbProxyAuth.Items.AddRange(new object[] {
															  "0 - Basic",
															  "1 - Digest",
															  "2 - SOCKS4",
															  "3 - SOCKS5"});
			this.cmbProxyAuth.Location = new System.Drawing.Point(328, 56);
			this.cmbProxyAuth.Name = "cmbProxyAuth";
			this.cmbProxyAuth.Size = new System.Drawing.Size(184, 21);
			this.cmbProxyAuth.TabIndex = 5;
			// 
			// txProxyPass
			// 
			this.txProxyPass.Enabled = false;
			this.txProxyPass.Location = new System.Drawing.Point(328, 24);
			this.txProxyPass.Name = "txProxyPass";
			this.txProxyPass.PasswordChar = '*';
			this.txProxyPass.Size = new System.Drawing.Size(184, 20);
			this.txProxyPass.TabIndex = 4;
			this.txProxyPass.Text = "";
			// 
			// txProxyUser
			// 
			this.txProxyUser.Enabled = false;
			this.txProxyUser.Location = new System.Drawing.Point(80, 24);
			this.txProxyUser.Name = "txProxyUser";
			this.txProxyUser.Size = new System.Drawing.Size(168, 20);
			this.txProxyUser.TabIndex = 3;
			this.txProxyUser.Text = "";
			// 
			// label8
			// 
			this.label8.Enabled = false;
			this.label8.Location = new System.Drawing.Point(264, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 16);
			this.label8.TabIndex = 2;
			this.label8.Text = "Password :";
			// 
			// label7
			// 
			this.label7.Enabled = false;
			this.label7.Location = new System.Drawing.Point(8, 24);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 16);
			this.label7.TabIndex = 1;
			this.label7.Text = "UserName :";
			// 
			// chkProxyAuth
			// 
			this.chkProxyAuth.Location = new System.Drawing.Point(8, 0);
			this.chkProxyAuth.Name = "chkProxyAuth";
			this.chkProxyAuth.Size = new System.Drawing.Size(128, 16);
			this.chkProxyAuth.TabIndex = 0;
			this.chkProxyAuth.Text = "Use Authentication :";
			this.chkProxyAuth.CheckedChanged += new System.EventHandler(this.chkProxyAuth_CheckedChanged);
			// 
			// label6
			// 
			this.label6.Enabled = false;
			this.label6.Location = new System.Drawing.Point(8, 24);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(128, 16);
			this.label6.TabIndex = 1;
			this.label6.Text = "Proxy Address/Port :";
			// 
			// chkProxy
			// 
			this.chkProxy.Location = new System.Drawing.Point(8, 0);
			this.chkProxy.Name = "chkProxy";
			this.chkProxy.Size = new System.Drawing.Size(128, 16);
			this.chkProxy.TabIndex = 0;
			this.chkProxy.Text = "Use Proxy Settings :";
			this.chkProxy.CheckedChanged += new System.EventHandler(this.chkProxy_CheckedChanged);
			// 
			// groupENDPoints
			// 
			this.groupENDPoints.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupENDPoints.Controls.Add(this.label4);
			this.groupENDPoints.Controls.Add(this.txLocPort);
			this.groupENDPoints.Controls.Add(this.txLocAddress);
			this.groupENDPoints.Controls.Add(this.label3);
			this.groupENDPoints.Controls.Add(this.txRemPort);
			this.groupENDPoints.Controls.Add(this.txRemAddress);
			this.groupENDPoints.Controls.Add(this.label2);
			this.groupENDPoints.Controls.Add(this.label1);
			this.groupENDPoints.Location = new System.Drawing.Point(8, 8);
			this.groupENDPoints.Name = "groupENDPoints";
			this.groupENDPoints.Size = new System.Drawing.Size(536, 80);
			this.groupENDPoints.TabIndex = 2;
			this.groupENDPoints.TabStop = false;
			this.groupENDPoints.Text = "Remote/Local EndPoints :";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(432, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(16, 16);
			this.label4.TabIndex = 7;
			this.label4.Text = ":";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txLocPort
			// 
			this.txLocPort.Location = new System.Drawing.Point(448, 48);
			this.txLocPort.MaxLength = 5;
			this.txLocPort.Name = "txLocPort";
			this.txLocPort.Size = new System.Drawing.Size(80, 20);
			this.txLocPort.TabIndex = 6;
			this.txLocPort.Text = "0";
			this.txLocPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txLocPort.TextChanged += new System.EventHandler(this.txLocPort_TextChanged);
			// 
			// txLocAddress
			// 
			this.txLocAddress.Location = new System.Drawing.Point(120, 48);
			this.txLocAddress.Name = "txLocAddress";
			this.txLocAddress.Size = new System.Drawing.Size(312, 20);
			this.txLocAddress.TabIndex = 5;
			this.txLocAddress.Text = "0.0.0.0";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(432, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(16, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = ":";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txRemPort
			// 
			this.txRemPort.Location = new System.Drawing.Point(448, 24);
			this.txRemPort.MaxLength = 5;
			this.txRemPort.Name = "txRemPort";
			this.txRemPort.Size = new System.Drawing.Size(80, 20);
			this.txRemPort.TabIndex = 3;
			this.txRemPort.Text = "0";
			this.txRemPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txRemPort.TextChanged += new System.EventHandler(this.txRemPort_TextChanged);
			// 
			// txRemAddress
			// 
			this.txRemAddress.Location = new System.Drawing.Point(120, 24);
			this.txRemAddress.Name = "txRemAddress";
			this.txRemAddress.Size = new System.Drawing.Size(312, 20);
			this.txRemAddress.TabIndex = 2;
			this.txRemAddress.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Local Address/Port :";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Remote Server/Port :";
			// 
			// tabOptions
			// 
			this.tabOptions.Controls.Add(this.groupProxyAdvance);
			this.tabOptions.Controls.Add(this.groupContentLength);
			this.tabOptions.Controls.Add(this.groupOptions);
			this.tabOptions.Controls.Add(this.groupLogger);
			this.tabOptions.Controls.Add(this.groupAgent);
			this.tabOptions.Location = new System.Drawing.Point(4, 22);
			this.tabOptions.Name = "tabOptions";
			this.tabOptions.Size = new System.Drawing.Size(552, 438);
			this.tabOptions.TabIndex = 1;
			this.tabOptions.Text = "Options";
			// 
			// groupProxyAdvance
			// 
			this.groupProxyAdvance.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupProxyAdvance.Controls.Add(this.trackProxyTimeout);
			this.groupProxyAdvance.Controls.Add(this.trackProxyBuffer);
			this.groupProxyAdvance.Controls.Add(this.txPBufferTimeout);
			this.groupProxyAdvance.Controls.Add(this.txPBufferValue);
			this.groupProxyAdvance.Controls.Add(this.label14);
			this.groupProxyAdvance.Controls.Add(this.label13);
			this.groupProxyAdvance.Location = new System.Drawing.Point(12, 136);
			this.groupProxyAdvance.Name = "groupProxyAdvance";
			this.groupProxyAdvance.Size = new System.Drawing.Size(536, 96);
			this.groupProxyAdvance.TabIndex = 1;
			this.groupProxyAdvance.TabStop = false;
			this.groupProxyAdvance.Text = "Proxy Advance Options :";
			// 
			// trackProxyTimeout
			// 
			this.trackProxyTimeout.AutoSize = false;
			this.trackProxyTimeout.LargeChange = 100;
			this.trackProxyTimeout.Location = new System.Drawing.Point(104, 64);
			this.trackProxyTimeout.Maximum = 4000;
			this.trackProxyTimeout.Minimum = 500;
			this.trackProxyTimeout.Name = "trackProxyTimeout";
			this.trackProxyTimeout.Size = new System.Drawing.Size(328, 24);
			this.trackProxyTimeout.SmallChange = 50;
			this.trackProxyTimeout.TabIndex = 1;
			this.trackProxyTimeout.TickFrequency = 50;
			this.trackProxyTimeout.Value = 500;
			this.trackProxyTimeout.ValueChanged += new System.EventHandler(this.trackProxyTimeout_ValueChanged);
			// 
			// trackProxyBuffer
			// 
			this.trackProxyBuffer.AutoSize = false;
			this.trackProxyBuffer.LargeChange = 1024;
			this.trackProxyBuffer.Location = new System.Drawing.Point(104, 20);
			this.trackProxyBuffer.Maximum = 65536;
			this.trackProxyBuffer.Name = "trackProxyBuffer";
			this.trackProxyBuffer.Size = new System.Drawing.Size(328, 24);
			this.trackProxyBuffer.SmallChange = 512;
			this.trackProxyBuffer.TabIndex = 0;
			this.trackProxyBuffer.TickFrequency = 512;
			this.trackProxyBuffer.ValueChanged += new System.EventHandler(this.trackProxyBuffer_ValueChanged);
			// 
			// txPBufferTimeout
			// 
			this.txPBufferTimeout.Location = new System.Drawing.Point(440, 64);
			this.txPBufferTimeout.Name = "txPBufferTimeout";
			this.txPBufferTimeout.ReadOnly = true;
			this.txPBufferTimeout.Size = new System.Drawing.Size(88, 20);
			this.txPBufferTimeout.TabIndex = 3;
			this.txPBufferTimeout.Text = "";
			this.txPBufferTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txPBufferValue
			// 
			this.txPBufferValue.Location = new System.Drawing.Point(440, 20);
			this.txPBufferValue.Name = "txPBufferValue";
			this.txPBufferValue.ReadOnly = true;
			this.txPBufferValue.Size = new System.Drawing.Size(88, 20);
			this.txPBufferValue.TabIndex = 2;
			this.txPBufferValue.Text = "";
			this.txPBufferValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(8, 48);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(96, 40);
			this.label14.TabIndex = 1;
			this.label14.Text = "Proxy Buffer TimeOut (milliseconds) :";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(8, 24);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(104, 16);
			this.label13.TabIndex = 0;
			this.label13.Text = "Proxy Buffer Size :";
			// 
			// groupContentLength
			// 
			this.groupContentLength.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupContentLength.Controls.Add(this.txValueContent);
			this.groupContentLength.Controls.Add(this.trackContent);
			this.groupContentLength.Location = new System.Drawing.Point(12, 288);
			this.groupContentLength.Name = "groupContentLength";
			this.groupContentLength.Size = new System.Drawing.Size(536, 64);
			this.groupContentLength.TabIndex = 3;
			this.groupContentLength.TabStop = false;
			this.groupContentLength.Text = "Content Length :";
			// 
			// txValueContent
			// 
			this.txValueContent.Location = new System.Drawing.Point(280, 10);
			this.txValueContent.Name = "txValueContent";
			this.txValueContent.ReadOnly = true;
			this.txValueContent.Size = new System.Drawing.Size(240, 20);
			this.txValueContent.TabIndex = 1;
			this.txValueContent.Text = "";
			this.txValueContent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// trackContent
			// 
			this.trackContent.AutoSize = false;
			this.trackContent.LargeChange = 100000;
			this.trackContent.Location = new System.Drawing.Point(8, 32);
			this.trackContent.Maximum = 1024000;
			this.trackContent.Minimum = 10240;
			this.trackContent.Name = "trackContent";
			this.trackContent.Size = new System.Drawing.Size(520, 24);
			this.trackContent.SmallChange = 10000;
			this.trackContent.TabIndex = 0;
			this.trackContent.TickFrequency = 10000;
			this.trackContent.Value = 102400;
			this.trackContent.ValueChanged += new System.EventHandler(this.trackContent_ValueChanged);
			// 
			// groupOptions
			// 
			this.groupOptions.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupOptions.Controls.Add(this.chkEncription);
			this.groupOptions.Controls.Add(this.chkStealthMode);
			this.groupOptions.Controls.Add(this.chkStrictProxy);
			this.groupOptions.Controls.Add(this.radioSoftThread);
			this.groupOptions.Controls.Add(this.radioHeavyThread);
			this.groupOptions.Controls.Add(this.chkStats);
			this.groupOptions.Controls.Add(this.cmbProtocol);
			this.groupOptions.Controls.Add(this.label12);
			this.groupOptions.Controls.Add(this.chkCompress);
			this.groupOptions.Controls.Add(this.chkCompatible);
			this.groupOptions.Location = new System.Drawing.Point(12, 8);
			this.groupOptions.Name = "groupOptions";
			this.groupOptions.Size = new System.Drawing.Size(536, 128);
			this.groupOptions.TabIndex = 0;
			this.groupOptions.TabStop = false;
			this.groupOptions.Text = "Options :";
			// 
			// chkEncription
			// 
			this.chkEncription.Location = new System.Drawing.Point(400, 96);
			this.chkEncription.Name = "chkEncription";
			this.chkEncription.Size = new System.Drawing.Size(128, 16);
			this.chkEncription.TabIndex = 18;
			this.chkEncription.Text = "Use Data Encryption";
			// 
			// chkStealthMode
			// 
			this.chkStealthMode.Location = new System.Drawing.Point(256, 96);
			this.chkStealthMode.Name = "chkStealthMode";
			this.chkStealthMode.Size = new System.Drawing.Size(160, 16);
			this.chkStealthMode.TabIndex = 17;
			this.chkStealthMode.Text = "Enable Stealth Mode";
			this.chkStealthMode.CheckedChanged += new System.EventHandler(this.chkStealthMode_CheckedChanged);
			// 
			// chkStrictProxy
			// 
			this.chkStrictProxy.Checked = true;
			this.chkStrictProxy.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkStrictProxy.Location = new System.Drawing.Point(256, 72);
			this.chkStrictProxy.Name = "chkStrictProxy";
			this.chkStrictProxy.Size = new System.Drawing.Size(176, 16);
			this.chkStrictProxy.TabIndex = 16;
			this.chkStrictProxy.Text = "Use Strict Content Length";
			// 
			// radioSoftThread
			// 
			this.radioSoftThread.Checked = true;
			this.radioSoftThread.Location = new System.Drawing.Point(136, 80);
			this.radioSoftThread.Name = "radioSoftThread";
			this.radioSoftThread.Size = new System.Drawing.Size(144, 16);
			this.radioSoftThread.TabIndex = 15;
			this.radioSoftThread.TabStop = true;
			this.radioSoftThread.Text = "SoftThread mode";
			// 
			// radioHeavyThread
			// 
			this.radioHeavyThread.Location = new System.Drawing.Point(8, 80);
			this.radioHeavyThread.Name = "radioHeavyThread";
			this.radioHeavyThread.Size = new System.Drawing.Size(152, 16);
			this.radioHeavyThread.TabIndex = 14;
			this.radioHeavyThread.Text = "HeavyThread mode";
			// 
			// chkStats
			// 
			this.chkStats.Checked = true;
			this.chkStats.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkStats.Location = new System.Drawing.Point(256, 48);
			this.chkStats.Name = "chkStats";
			this.chkStats.Size = new System.Drawing.Size(168, 16);
			this.chkStats.TabIndex = 13;
			this.chkStats.Text = "Use Connections Statistics";
			// 
			// cmbProtocol
			// 
			this.cmbProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbProtocol.Items.AddRange(new object[] {
															 "TCP/IP",
															 "UDP/IP"});
			this.cmbProtocol.Location = new System.Drawing.Point(104, 48);
			this.cmbProtocol.Name = "cmbProtocol";
			this.cmbProtocol.Size = new System.Drawing.Size(136, 21);
			this.cmbProtocol.TabIndex = 12;
			this.cmbProtocol.SelectedIndexChanged += new System.EventHandler(this.cmbProtocol_SelectedIndexChanged);
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(8, 52);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(120, 16);
			this.label12.TabIndex = 11;
			this.label12.Text = "Tunnel Protocol :";
			// 
			// chkCompress
			// 
			this.chkCompress.Location = new System.Drawing.Point(256, 24);
			this.chkCompress.Name = "chkCompress";
			this.chkCompress.Size = new System.Drawing.Size(176, 16);
			this.chkCompress.TabIndex = 10;
			this.chkCompress.Text = "Compress Data Inside Tunnel";
			// 
			// chkCompatible
			// 
			this.chkCompatible.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkCompatible.Location = new System.Drawing.Point(8, 24);
			this.chkCompatible.Name = "chkCompatible";
			this.chkCompatible.Size = new System.Drawing.Size(176, 16);
			this.chkCompatible.TabIndex = 9;
			this.chkCompatible.Text = "Run in HTS Compatible Mode";
			this.chkCompatible.CheckedChanged += new System.EventHandler(this.chkCompatible_CheckedChanged);
			// 
			// groupLogger
			// 
			this.groupLogger.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupLogger.Controls.Add(this.cmbLoggerLevel);
			this.groupLogger.Controls.Add(this.label5);
			this.groupLogger.Controls.Add(this.chkLogger);
			this.groupLogger.Location = new System.Drawing.Point(12, 352);
			this.groupLogger.Name = "groupLogger";
			this.groupLogger.Size = new System.Drawing.Size(536, 56);
			this.groupLogger.TabIndex = 4;
			this.groupLogger.TabStop = false;
			this.groupLogger.Text = "Logger :";
			// 
			// cmbLoggerLevel
			// 
			this.cmbLoggerLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbLoggerLevel.Enabled = false;
			this.cmbLoggerLevel.Items.AddRange(new object[] {
																"0 - Log Only Errors",
																"1 - Log Every Actions "});
			this.cmbLoggerLevel.Location = new System.Drawing.Point(208, 24);
			this.cmbLoggerLevel.Name = "cmbLoggerLevel";
			this.cmbLoggerLevel.Size = new System.Drawing.Size(320, 21);
			this.cmbLoggerLevel.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.Enabled = false;
			this.label5.Location = new System.Drawing.Point(120, 29);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 24);
			this.label5.TabIndex = 1;
			this.label5.Text = "Logger Level :";
			// 
			// chkLogger
			// 
			this.chkLogger.Location = new System.Drawing.Point(8, 24);
			this.chkLogger.Name = "chkLogger";
			this.chkLogger.Size = new System.Drawing.Size(112, 24);
			this.chkLogger.TabIndex = 0;
			this.chkLogger.Text = "Enable Logger";
			this.chkLogger.CheckedChanged += new System.EventHandler(this.chkLogger_CheckedChanged);
			// 
			// groupAgent
			// 
			this.groupAgent.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupAgent.Controls.Add(this.txUserAgent);
			this.groupAgent.Location = new System.Drawing.Point(12, 232);
			this.groupAgent.Name = "groupAgent";
			this.groupAgent.Size = new System.Drawing.Size(536, 56);
			this.groupAgent.TabIndex = 2;
			this.groupAgent.TabStop = false;
			this.groupAgent.Text = "User Agent :";
			// 
			// txUserAgent
			// 
			this.txUserAgent.Location = new System.Drawing.Point(8, 24);
			this.txUserAgent.Name = "txUserAgent";
			this.txUserAgent.Size = new System.Drawing.Size(520, 20);
			this.txUserAgent.TabIndex = 0;
			this.txUserAgent.Text = "";
			// 
			// tabStatistics
			// 
			this.tabStatistics.Controls.Add(this.groupStat);
			this.tabStatistics.Controls.Add(this.label11);
			this.tabStatistics.Location = new System.Drawing.Point(4, 22);
			this.tabStatistics.Name = "tabStatistics";
			this.tabStatistics.Size = new System.Drawing.Size(552, 438);
			this.tabStatistics.TabIndex = 2;
			this.tabStatistics.Text = "Statistics";
			// 
			// groupStat
			// 
			this.groupStat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupStat.Controls.Add(this.btClearStats);
			this.groupStat.Controls.Add(this.btRefresh);
			this.groupStat.Controls.Add(this.groupBox1);
			this.groupStat.Controls.Add(this.lvConn);
			this.groupStat.Location = new System.Drawing.Point(0, 0);
			this.groupStat.Name = "groupStat";
			this.groupStat.Size = new System.Drawing.Size(552, 440);
			this.groupStat.TabIndex = 6;
			this.groupStat.TabStop = false;
			// 
			// btClearStats
			// 
			this.btClearStats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btClearStats.Location = new System.Drawing.Point(240, 208);
			this.btClearStats.Name = "btClearStats";
			this.btClearStats.Size = new System.Drawing.Size(144, 24);
			this.btClearStats.TabIndex = 6;
			this.btClearStats.Text = "Clear Statistics List";
			this.btClearStats.Click += new System.EventHandler(this.btClearStats_Click);
			// 
			// btRefresh
			// 
			this.btRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btRefresh.Location = new System.Drawing.Point(400, 208);
			this.btRefresh.Name = "btRefresh";
			this.btRefresh.Size = new System.Drawing.Size(144, 24);
			this.btRefresh.TabIndex = 4;
			this.btRefresh.Text = "Refresh";
			this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.lvStat);
			this.groupBox1.Location = new System.Drawing.Point(8, 232);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(536, 200);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Statistics :";
			// 
			// lvStat
			// 
			this.lvStat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvStat.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					 this.clmName1,
																					 this.clmValue1,
																					 this.clmName2,
																					 this.clmValue2});
			this.lvStat.GridLines = true;
			this.lvStat.Location = new System.Drawing.Point(8, 16);
			this.lvStat.MultiSelect = false;
			this.lvStat.Name = "lvStat";
			this.lvStat.Size = new System.Drawing.Size(520, 176);
			this.lvStat.TabIndex = 0;
			this.lvStat.View = System.Windows.Forms.View.Details;
			// 
			// clmName1
			// 
			this.clmName1.Text = "Name";
			this.clmName1.Width = 126;
			// 
			// clmValue1
			// 
			this.clmValue1.Text = "Value";
			this.clmValue1.Width = 112;
			// 
			// clmName2
			// 
			this.clmName2.Text = "Name";
			this.clmName2.Width = 126;
			// 
			// clmValue2
			// 
			this.clmValue2.Text = "Value";
			this.clmValue2.Width = 112;
			// 
			// lvConn
			// 
			this.lvConn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvConn.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					 this.clmID,
																					 this.clmSrvHost,
																					 this.clmSrvPort,
																					 this.clmDstHost,
																					 this.clmDstPort,
																					 this.clmDate});
			this.lvConn.FullRowSelect = true;
			this.lvConn.GridLines = true;
			this.lvConn.Location = new System.Drawing.Point(8, 16);
			this.lvConn.MultiSelect = false;
			this.lvConn.Name = "lvConn";
			this.lvConn.Size = new System.Drawing.Size(536, 184);
			this.lvConn.TabIndex = 3;
			this.lvConn.View = System.Windows.Forms.View.Details;
			this.lvConn.Click += new System.EventHandler(this.lvConn_Click);
			this.lvConn.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvConn_ColumnClick);
			this.lvConn.SelectedIndexChanged += new System.EventHandler(this.lvConn_SelectedIndexChanged);
			// 
			// clmID
			// 
			this.clmID.Text = "Conn ID";
			this.clmID.Width = 69;
			// 
			// clmSrvHost
			// 
			this.clmSrvHost.Text = "Server Host";
			this.clmSrvHost.Width = 98;
			// 
			// clmSrvPort
			// 
			this.clmSrvPort.Text = "Server Port";
			this.clmSrvPort.Width = 71;
			// 
			// clmDstHost
			// 
			this.clmDstHost.Text = "Dest. Host";
			this.clmDstHost.Width = 95;
			// 
			// clmDstPort
			// 
			this.clmDstPort.Text = "Dest. Port";
			this.clmDstPort.Width = 73;
			// 
			// clmDate
			// 
			this.clmDate.Text = "Start-Time";
			this.clmDate.Width = 89;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(8, -21);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(136, 21);
			this.label11.TabIndex = 5;
			this.label11.Text = "Logged Connections :";
			// 
			// nIcon
			// 
			this.nIcon.Text = "";
			this.nIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.nIcon_MouseMove);
			this.nIcon.DoubleClick += new System.EventHandler(this.nIcon_DoubleClick);
			// 
			// WarpTunnelClient
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(562, 480);
			this.Controls.Add(this.tabControl1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "WarpTunnelClient";
			this.Text = " Warp Tunnel Client v 1.4.7.0";
			this.Resize += new System.EventHandler(this.WarpTunnelClient_Resize);
			this.Load += new System.EventHandler(this.WarpTunnelClient_Load);
			this.Closed += new System.EventHandler(this.WarpTunnelClient_Closed);
			this.Activated += new System.EventHandler(this.WarpTunnelClient_Activated);
			this.tabControl1.ResumeLayout(false);
			this.tabClient.ResumeLayout(false);
			this.gruopTunnelAuth.ResumeLayout(false);
			this.groupProxyEmu.ResumeLayout(false);
			this.groupTunnelDest.ResumeLayout(false);
			this.groupSettings.ResumeLayout(false);
			this.groupProxy.ResumeLayout(false);
			this.groupAuth.ResumeLayout(false);
			this.groupENDPoints.ResumeLayout(false);
			this.tabOptions.ResumeLayout(false);
			this.groupProxyAdvance.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackProxyTimeout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackProxyBuffer)).EndInit();
			this.groupContentLength.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackContent)).EndInit();
			this.groupOptions.ResumeLayout(false);
			this.groupLogger.ResumeLayout(false);
			this.groupAgent.ResumeLayout(false);
			this.tabStatistics.ResumeLayout(false);
			this.groupStat.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) 
		{

			WarpTunnelClient startApp = new WarpTunnelClient();
			
			if(args != null && args.Length == 1)
			{
				if(args[0].ToLower() == "-s")
					startApp.ifStartAtStartup = true;
			}
			
			Application.Run(startApp);
		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
			Htc.stopClient();
		
		}

		private void WarpTunnelClient_Closed(object sender, System.EventArgs e)
		{
			this.saveSettings();
			this.nIcon.Dispose();

			Process.GetCurrentProcess().Close();
			Process.GetCurrentProcess().Kill();

		}

		private void chkLogger_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkLogger.Checked)
			{
				this.label5.Enabled = true;
				this.cmbLoggerLevel.Enabled = true;
			}
			else 
			{
				this.label5.Enabled = false;
				this.cmbLoggerLevel.Enabled = false;
			}
		
		}

		private void chkProxy_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkProxy.Checked)
			{
				this.label6.Enabled = true;
				this.label9.Enabled = true;
				this.txProxyAddress.Enabled = true;
				this.txProxyPort.Enabled = true;
				this.groupAuth.Enabled = true;
			}
			else
			{
				this.label6.Enabled = false;
				this.label9.Enabled = false;
				this.txProxyAddress.Enabled = false;
				this.txProxyPort.Enabled = false;
				this.groupAuth.Enabled = false;
			}
		}

		private void chkProxyAuth_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkProxyAuth.Checked)
			{
				this.label7.Enabled = true;
				this.label8.Enabled = true;
				this.txProxyUser.Enabled = true;
				this.txProxyPass.Enabled = true;
			}
			else
			{
				this.label7.Enabled = false;
				this.label8.Enabled = false;
				this.txProxyUser.Enabled = false;
				this.txProxyPass.Enabled = false;
			}
		}

		private void saveSettings()
		{

			string xmlFile = Application.StartupPath + @"\" + "ClientSettings.xml";
			ClientSettingTable SettingTable = new ClientSettingTable();

			try 
			{
				for(int i = 0;i < 1; i++)
				{
					ClientSettingTable.ClientSettingsRow row = SettingTable.ClientSettings.NewClientSettingsRow();

					if(this.txProxyPort.Text.Trim() == "")
						this.txProxyPort.Text = "0";
					if(this.txLocPort.Text.Trim() == "")
						this.txLocPort.Text = "0";
					if(this.txRemPort.Text.Trim() == "")
						this.txRemPort.Text = "0";

					row.clientName = "htc";
					row.serverAddress = this.txRemAddress.Text;
					row.serverPort = Convert.ToInt32(this.txRemPort.Text);
					row.localAddress = this.txLocAddress.Text;
					row.localPort = Convert.ToInt32(this.txLocPort.Text);
					row.proxyCheck = this.chkProxy.Checked;
					row.proxyAddress = this.txProxyAddress.Text;

					row.proxyPort = Convert.ToInt32(this.txProxyPort.Text);
					row.proxyAuthCheck = this.chkProxyAuth.Checked;
					row.proxyUser = this.txProxyUser.Text;
					row.userAgent = this.txUserAgent.Text;
					row.loggerCheck = this.chkLogger.Checked;
					row.loggerLevel = this.cmbLoggerLevel.SelectedIndex;
					row.ifCompressData = this.chkCompress.Checked;
					row.ifCompatible = this.chkCompatible.Checked;
					row.ProxyAuthenticationMode = this.cmbProxyAuth.SelectedIndex;
					row.ContentLength = this.trackContent.Value;
					row.ProtocolType = this.cmbProtocol.SelectedIndex;
					row.ProxyBufferSize = this.trackProxyBuffer.Value;
					row.ProxyBufferTimeout = this.trackProxyTimeout.Value;
					row.IsAuthTunnelRequired = this.chkTunnelAuth.Checked;
					row.TunnelAuthUsername = this.txTunnelUsername.Text;
					row.TunnelDestHost = this.txTunnelDestHost.Text;
					row.TunnelDestPort = Convert.ToInt32(this.txTunnelDestPort.Text);
					row.IfUseStatistics = this.chkStats.Checked;
					row.clientLikeA = this.cmbAcceptConnLike.SelectedIndex;
					row.IfUseFixedDestAddress = this.chkFixedDestAddress.Checked;
					row.IfSoftThreadingMode = this.radioSoftThread.Checked;
					row.StrictContentLength = this.chkStrictProxy.Checked;
					row.StealthModeEnable = this.chkStealthMode.Checked;
					row.AutoProxyAuthentication = this.chkProxyAutoConf.Checked;
					row.ifUseAsimmetricDataEncription = this.chkEncription.Checked;
					
					byte[] passArr = new byte[this.txTunnelPassword.Text.Length];
					Encoding.ASCII.GetBytes(this.txTunnelPassword.Text,0,this.txTunnelPassword.Text.Length,passArr,0);
					row.TunnelAuthPassword = Convert.ToBase64String(passArr,0,passArr.Length);

					SettingTable.ClientSettings.AddClientSettingsRow(row);
					SettingTable.ClientSettings.AcceptChanges();
				}

				if(File.Exists(xmlFile))
					File.Delete(xmlFile);

				SettingTable.WriteXml(xmlFile);
			} 
			catch
			{
			} 
			finally
			{
				SettingTable = null;
			}

		}

		private void loadSettings()
		{
			string xmlFile = Application.StartupPath + @"\" + "ClientSettings.xml";

			if(File.Exists(xmlFile))
			{
				ClientSettingTable SettingTable = new ClientSettingTable();
				
				try
				{
					SettingTable.ReadXml(xmlFile);

					for(int i = 0; i < SettingTable.ClientSettings.Rows.Count; i++)
					{
						ClientSettingTable.ClientSettingsRow curr_row = (ClientSettingTable.ClientSettingsRow)SettingTable.ClientSettings.Rows[i];
						this.txRemAddress.Text = curr_row.serverAddress;
						this.txRemPort.Text = Convert.ToString(curr_row.serverPort);
						this.txLocAddress.Text = curr_row.localAddress;
						this.txLocPort.Text = Convert.ToString(curr_row.localPort);
						this.chkProxy.Checked = curr_row.proxyCheck;
						this.txProxyAddress.Text = curr_row.proxyAddress;
						this.txProxyPort.Text = Convert.ToString(curr_row.proxyPort);
						this.chkProxyAuth.Checked = curr_row.proxyAuthCheck;
						this.txProxyUser.Text = curr_row.proxyUser;
						this.txUserAgent.Text = curr_row.userAgent;
						this.chkLogger.Checked = curr_row.loggerCheck;
						this.cmbLoggerLevel.SelectedIndex = curr_row.loggerLevel;
						this.chkCompress.Checked = curr_row.ifCompressData;
						this.chkCompatible.Checked = curr_row.ifCompatible;
						this.cmbProxyAuth.SelectedIndex = curr_row.ProxyAuthenticationMode;
						this.trackContent.Value = curr_row.ContentLength;
						this.cmbProtocol.SelectedIndex = curr_row.ProtocolType;
						this.trackProxyBuffer.Value = curr_row.ProxyBufferSize;
						this.trackProxyTimeout.Value = curr_row.ProxyBufferTimeout;
						this.chkTunnelAuth.Checked = curr_row.IsAuthTunnelRequired;
						this.txTunnelUsername.Text = curr_row.TunnelAuthUsername;
						this.txTunnelDestHost.Text = curr_row.TunnelDestHost;
						this.txTunnelDestPort.Text = Convert.ToString(curr_row.TunnelDestPort);
						this.chkStats.Checked = curr_row.IfUseStatistics;
						this.cmbAcceptConnLike.SelectedIndex = curr_row.clientLikeA;
						this.chkFixedDestAddress.Checked = curr_row.IfUseFixedDestAddress;
						this.radioSoftThread.Checked = curr_row.IfSoftThreadingMode;
						this.radioHeavyThread.Checked = !curr_row.IfSoftThreadingMode;
						this.chkStrictProxy.Checked = curr_row.StrictContentLength;
						this.chkStealthMode.Checked = curr_row.StealthModeEnable;
						this.chkProxyAutoConf.Checked = curr_row.AutoProxyAuthentication;
						this.chkEncription.Checked = curr_row.ifUseAsimmetricDataEncription;

						byte[] passArr = Convert.FromBase64String(curr_row.TunnelAuthPassword);
						this.txTunnelPassword.Text = Encoding.ASCII.GetString(passArr,0,passArr.Length);
					}
				} 
				catch
				{
					File.Delete(xmlFile);
				} 
				finally
				{
					SettingTable = null;
				}
			}
		}

		private void btSaveSettings_Click(object sender, System.EventArgs e)
		{
			saveSettings();
		}

		private void btloadsettings_Click(object sender, System.EventArgs e)
		{
			loadSettings();
		}

		private static HTTPStructures.ProxyAuthMethod getAuthMethod (int cmbSelectedIndex)
		{
			switch (cmbSelectedIndex)
			{
				case 0 :
					return HTTPStructures.ProxyAuthMethod.BASIC_AUTENTICATION;
				case 1 :
					return HTTPStructures.ProxyAuthMethod.DIGEST_AUTHENTICATION;
				case 2 :
					return HTTPStructures.ProxyAuthMethod.SOCK4_AUTHENTICATION;
				case 3 :
					return HTTPStructures.ProxyAuthMethod.SOCK5_AUTHENTICATION;
			}

			return HTTPStructures.ProxyAuthMethod.BASIC_AUTENTICATION;
		}

		private void btStop_Click(object sender, System.EventArgs e)
		{
			this.timerControll.Enabled = false;
			this.Htc.stopClient();

			if(this.chkLogger.Checked)
			{
				Logger.setLoggerEnabled(false);
				Logger.stopLoggerStream();
			}

			this.gruopTunnelAuth.Enabled = true;
			this.groupProxyAdvance.Enabled = true;
			this.groupAgent.Enabled = true;
			this.groupProxy.Enabled = true;
			this.groupENDPoints.Enabled = true;
			this.groupLogger.Enabled = true;
			this.groupSettings.Enabled = true;
			this.groupOptions.Enabled = true;
			this.groupContentLength.Enabled = true;
			this.cmbProxyAuth.Enabled = true;
			this.cmbProtocol.Enabled = true;

			this.btStart.Enabled = true;
			this.btStop.Enabled = false;

			this.Htc = null;
		}

		private void startClient()
		{
			try 
			{
				this.Htc = new ClientTunnel();
				this.Htc.ifCompressData = this.chkCompress.Checked;
				this.Htc.ifUseAsimmetricDataEncription = this.chkEncription.Checked;
				this.Htc.stealthModeEnable = this.chkStealthMode.Checked;
				this.Htc.ifCompatible = this.chkCompatible.Checked;
				this.Htc.host_name = this.txRemAddress.Text;
				this.Htc.host_port = Convert.ToInt32(this.txRemPort.Text);
				this.Htc.forward_host = this.txLocAddress.Text;
				this.Htc.forward_port = Convert.ToInt32(this.txLocPort.Text);
				this.Htc.content_length = this.trackContent.Value;
				this.Htc.strict_content_length = this.chkStrictProxy.Checked;

				if(((string)this.cmbProtocol.SelectedItem) == "TCP/IP")
					this.Htc.ClientProtocol = ClientTunnel.TunnelProtocol.TCP;
				else if(((string)this.cmbProtocol.SelectedItem) == "UDP/IP")
					this.Htc.ClientProtocol = ClientTunnel.TunnelProtocol.UDP;

				if(this.chkProxy.Checked)
				{
					this.Htc.proxy_name = this.txProxyAddress.Text;
					this.Htc.proxy_port = Convert.ToInt32(this.txProxyPort.Text);
					this.Htc.autoProxyConfiguration = this.chkProxyAutoConf.Checked;

					if(this.chkProxyAuth.Checked)
					{
						this.Htc.proxy_authorization = this.txProxyUser.Text +
							":" + this.txProxyPass.Text;
						this.Htc.proxy_auth = getAuthMethod(this.cmbProxyAuth.SelectedIndex);
					}

					if(this.trackProxyBuffer.Value != 0)
					{
						this.Htc.proxy_buffer_size = this.trackProxyBuffer.Value;
						this.Htc.proxy_buffer_timeout = this.trackProxyTimeout.Value;
					}
				}

				if(this.chkTunnelAuth.Checked)
				{
					Htc.IsTunnelAuthRequired = true;
					Htc.tunnelAuthUsername = this.txTunnelUsername.Text;
					Htc.tunnelAuthPasword = this.txTunnelPassword.Text;

					if(this.chkFixedDestAddress.Checked)
					{
						Htc.tunnelDestHost = this.txTunnelDestHost.Text;
						Htc.tunnelDestPort = Convert.ToInt32(this.txTunnelDestPort.Text);
						Htc.EmulatorKind = ClientTunnel.Client_Emulate.NONE;

					} 
					else if(this.chkLetProgDecideAddress.Checked)
					{
						switch(this.cmbAcceptConnLike.SelectedIndex)       
						{         
							case 0:   
								Htc.EmulatorKind = ClientTunnel.Client_Emulate.CONNECT;
								break; 
							case 1:            
								Htc.EmulatorKind = ClientTunnel.Client_Emulate.SOCKS4;
								break;                  
							case 2:            
								Htc.EmulatorKind = ClientTunnel.Client_Emulate.SOCKS5;
								break;                  
						}
					}
				}

				if(this.radioSoftThread.Checked)
				{
					Htc.clientThreadMode = Tunnel.Thread_Mode.SoftThreading;
				} 
				else if (this.radioHeavyThread.Checked)
				{
					Htc.clientThreadMode = Tunnel.Thread_Mode.HeavyThreading;
				}

				if(this.txUserAgent.Text.Trim() != "")
					this.Htc.user_agent = this.txUserAgent.Text;

				Htc.ifUseStats = this.chkStats.Checked;

				this.saveSettings();

				this.gruopTunnelAuth.Enabled = false;
				this.groupContentLength.Enabled = false;
				this.groupOptions.Enabled = false;
				this.groupAgent.Enabled = false;
				this.groupProxy.Enabled = false;
				this.groupENDPoints.Enabled = false;
				this.groupLogger.Enabled = false;
				this.groupSettings.Enabled = false;
				this.groupProxyAdvance.Enabled = false;
				this.btStop.Enabled = true;
				this.btStart.Enabled = false;
				this.cmbProxyAuth.Enabled = false;
				this.cmbProtocol.Enabled = false;

				if(this.chkLogger.Checked)
				{
					if(this.cmbLoggerLevel.SelectedIndex == 0)
						Logger.setLogErrorOnly(true);
					else
						Logger.setLogErrorOnly(false);

					Logger.setLoggerEnabled(true);
					Logger.startLoggerStream("");
				}

				this.Htc.startClient();
				this.timerControll.Enabled = true;
			} 
			catch 
			{
				MessageBox.Show(this,"Wrong Inserted Values !! Client Stopped !",this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
				btStop_Click(this,null);
			}
		}

		private void btStart_Click(object sender, System.EventArgs e)
		{
			this.startClient();
		}

		private void timerControll_Tick(object sender, System.EventArgs e)
		{
			if(Htc.ClientStop)
			{
				this.btStop_Click(this,null);
			}
		}

		private void loadEntriesData()
		{
			this.lvConn.Items.Clear();
			this.lvStat.Items.Clear();

			SortedList connTable = ConnStatRequests.getConnectionsLogEntries();

			ListViewItem new_item = null;
	
			for( int i = 0 ; i < connTable.Count; i++)
			{
				ConnectionStatistics.ConnectionDetails curr_row = (ConnectionStatistics.ConnectionDetails)connTable.GetByIndex(i);
				new_item = this.lvConn.Items.Add(Convert.ToString(curr_row.IDconn));
				new_item.SubItems.Add(curr_row.ServerHost);
				new_item.SubItems.Add(curr_row.ServerPort);
				new_item.SubItems.Add(curr_row.DestinationHost);
				new_item.SubItems.Add(curr_row.DestinationPort);
				new_item.SubItems.Add(curr_row.connTIME.ToShortDateString() + " " + curr_row.connTIME.ToShortTimeString());
			}


			this.lvConn.Sort();
			new_item = null;
			GC.Collect();
			
		}

		private void view_stats_data()
		{
			if(this.lvConn.SelectedItems.Count > 0)
			{
				this.lvStat.Items.Clear();

				int connID = Convert.ToInt32(this.lvConn.SelectedItems[0].Text);
				ConnectionStatistics.Statistics statData =  ConnStatRequests.getConnectionStatsFromConnectionID(connID);
				ListViewItem new_item = null;

				System.Reflection.MemberInfo[] stat_members = statData.GetType().GetMembers(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly | System.Reflection.BindingFlags.InvokeMethod);
				
				int Num_fields = 0;
				for(int i = 0; i < (stat_members.Length); i++)
				{
					if(stat_members[i].MemberType == System.Reflection.MemberTypes.Field)
						Num_fields++;
				}
				
				System.Reflection.MemberInfo[] new_stat_members = new System.Reflection.MemberInfo[Num_fields];

				int l = 0;

				for(int i = 0; i < (stat_members.Length); i++)
				{
					if(stat_members[i].MemberType == System.Reflection.MemberTypes.Field)
					{
						new_stat_members[l] = stat_members[i];
						l++;
					}
				}

				stat_members = new_stat_members;

				for(int i = 0; i < (stat_members.Length); i++)
				{
					new_item = this.lvStat.Items.Add(stat_members[i].Name.Replace("_"," "));
					new_item.SubItems.Add(Convert.ToString(statData.GetType().GetField(stat_members[i].Name).GetValue(statData)));

					if(i == stat_members.Length - 1)
						break;

					new_item.SubItems.Add(stat_members[i+1].Name.Replace("_"," "));
					new_item.SubItems.Add(Convert.ToString(statData.GetType().GetField(stat_members[i+1].Name).GetValue(statData)));
					i++;
				}


				new_item = null;
				GC.Collect();

			}
		}

		private void lvConn_Click(object sender, System.EventArgs e)
		{
			view_stats_data();
		}

		private void lvConn_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			view_stats_data();
		}

		private void btRefresh_Click(object sender, System.EventArgs e)
		{
			loadEntriesData();
		}

		private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.tabControl1.SelectedIndex == 2)
				loadEntriesData();
		}

		private void trackContent_ValueChanged(object sender, System.EventArgs e)
		{
			this.txValueContent.Text =  Convert.ToString(this.trackContent.Value);
		}

		private void chkCompatible_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkCompatible.Checked)
			{
				this.chkStealthMode.Checked = false;
				this.chkStealthMode.Enabled = false;
				this.chkCompress.Enabled = false;
				this.cmbProtocol.SelectedIndex = 0;
				this.cmbProtocol.Enabled = false;
				this.gruopTunnelAuth.Enabled = false;
				this.chkTunnelAuth.Checked = false;
				this.chkEncription.Checked = false;
				this.chkEncription.Enabled = false;

			} 
			else 
			{
				this.chkStealthMode.Enabled = true;
				this.chkCompress.Enabled = true;
				this.cmbProtocol.Enabled = true;
				this.gruopTunnelAuth.Enabled = true;
				this.chkEncription.Enabled = true;
			}
		}

		private void trackProxyBuffer_ValueChanged(object sender, System.EventArgs e)
		{
			this.txPBufferValue.Text = Convert.ToString(this.trackProxyBuffer.Value);
		}

		private void trackProxyTimeout_ValueChanged(object sender, System.EventArgs e)
		{
			this.txPBufferTimeout.Text = Convert.ToString(this.trackProxyTimeout.Value);
		}

		private void chkTunnelAuth_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkTunnelAuth.Checked)
			{
				this.txTunnelPassword.Enabled = true;
				this.txTunnelUsername.Enabled = true;
				this.groupTunnelDest.Enabled = true;
				if(this.cmbProtocol.SelectedIndex == 0)
					this.groupProxyEmu.Enabled = true;
			} 
			else
			{
				this.txTunnelPassword.Enabled = false;
				this.txTunnelUsername.Enabled = false;
				this.groupTunnelDest.Enabled = false;
				this.groupProxyEmu.Enabled = false;
			}
		}

		private void txRemPort_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int temp = Convert.ToInt32(this.txRemPort.Text);
			} 
			catch
			{
				this.txRemPort.Text = "0";
			}


			if(this.txRemPort.Text  == "")
				this.txRemPort.Text = "0";
		}

		private void txLocPort_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int temp = Convert.ToInt32(this.txLocPort.Text);
			} 
			catch
			{
				this.txLocPort.Text = "0";
			}


			if(this.txLocPort.Text  == "")
				this.txLocPort.Text = "0";
		}

		private void txProxyPort_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int temp = Convert.ToInt32(this.txProxyPort.Text);
			} 
			catch
			{
				this.txProxyPort.Text = "0";
			}


			if(this.txProxyPort.Text  == "")
				this.txProxyPort.Text = "0";
		}

		private void txTunnelDestPort_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int temp = Convert.ToInt32(this.txTunnelDestPort.Text);
			} 
			catch
			{
				this.txTunnelDestPort.Text = "0";
			}


			if(this.txTunnelDestPort.Text  == "")
				this.txTunnelDestPort.Text = "0";
		}

		private void WarpTunnelClient_Load(object sender, System.EventArgs e)
		{
//			if(Process.GetProcessesByName("WarpTunnelClientGUI").Length > 1)
//			{
//				MessageBox.Show(this,"Another instance of WarpTunnelClientGUI is already running on this machine. You cannot run TWO instances at a time.",this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
//				this.Close();
//			}
//
//			GC.Collect();

			if(this.ifStartAtStartup)
			{
				this.WindowState = FormWindowState.Minimized;
				this.startClient();
			}
		}

		private void chkFixedDestAddress_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkFixedDestAddress.Checked)
			{
				this.chkLetProgDecideAddress.Checked = false;

				this.txTunnelDestHost.Enabled = true;
				this.txTunnelDestPort.Enabled = true;
			} 
			else
			{
				if(this.groupProxyEmu.Enabled)
				{
					this.chkLetProgDecideAddress.Checked = true;

					this.txTunnelDestHost.Enabled = false;
					this.txTunnelDestPort.Enabled = false;
				} else
					this.chkFixedDestAddress.Checked = true;
			}
		}

		private void chkLetProgDecideAddress_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkLetProgDecideAddress.Checked)
			{
				this.chkFixedDestAddress.Checked = false;

				this.cmbAcceptConnLike.Enabled = true;
			} 
			else
			{
				this.chkFixedDestAddress.Checked = true;

				this.cmbAcceptConnLike.Enabled = false;
			}
		}

		private void cmbProtocol_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.cmbProtocol.SelectedIndex != 0)
			{
				if(!this.chkFixedDestAddress.Checked)
					this.chkFixedDestAddress.Checked = true;
				this.groupProxyEmu.Enabled = false;
			} 
			else
			{
				if(this.chkTunnelAuth.Checked)
					this.groupProxyEmu.Enabled = true;
			}
		}

		private void lvConn_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			if ( e.Column == this.lvSorter.SortColumn )
			{
				// Reverse the current sort direction for this column.
				if (this.lvSorter.Order == SortOrder.Ascending)
					this.lvSorter.Order = SortOrder.Descending;
				else
					this.lvSorter.Order = SortOrder.Ascending;
			}
			else
			{
				// Set the column number that is to be sorted; default to ascending.
				this.lvSorter.SortColumn = e.Column;
				this.lvSorter.Order = SortOrder.Ascending;
			}

			// Perform the sort with these new sort options.
			this.lvConn.Sort();		
		}

		private void btClearStats_Click(object sender, System.EventArgs e)
		{
			this.lvStat.Items.Clear();
			this.lvConn.Items.Clear();
			ConnStatRequests.clearConnectionsLogEntries();
			loadEntriesData();
		}

		private void chkStealthMode_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkStealthMode.Checked)
			{
				if(this.chkCompress.Checked)
					this.chkCompress.Checked = false;

				this.chkCompress.Enabled = false;
			} 
			else
			{
				this.chkCompress.Enabled = true;
			}
		}

		private void chkProxyAutoConf_CheckedChanged(object sender, System.EventArgs e)
		{
			this.txProxyAddress.ReadOnly = this.chkProxyAutoConf.Checked;
			this.txProxyPort.ReadOnly = this.chkProxyAutoConf.Checked;
			this.txProxyUser.ReadOnly = this.chkProxyAutoConf.Checked;
			this.txProxyPass.ReadOnly = this.chkProxyAutoConf.Checked;
		}

		private void nIcon_DoubleClick(object sender, System.EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				if(!this.Visible)
					this.Visible = !this.Visible;

				this.WindowState = FormWindowState.Normal;
			} 
			else
			{
				if(this.Visible)
					this.Visible = !this.Visible;

				this.WindowState = FormWindowState.Minimized;
			}
		}

		private void WarpTunnelClient_Resize(object sender, System.EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				if(this.Visible)
					this.Visible = false;

				if(!this.nIcon.Visible)
					this.nIcon.Visible = true;
			}
			else if(this.WindowState == FormWindowState.Maximized || this.WindowState == FormWindowState.Normal)
			{
				if(!this.Visible)
					this.Visible = true;

				if(this.nIcon.Visible)
					this.nIcon.Visible = false;
			}
		}

		private void nIcon_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(this.Htc == null || this.Htc.ClientStop)
			{
				if(this.nIcon.Text != this.Text) this.nIcon.Text = this.Text;
			}
			else
			{
				if(this.nIcon.Text != ("Listening on : " + this.txLocAddress.Text + ":" + this.txLocPort.Text )) this.nIcon.Text = (("Listening on : " + this.txLocAddress.Text + ":" + this.txLocPort.Text ));
			}
		}

		private void WarpTunnelClient_Activated(object sender, System.EventArgs e)
		{
			if(this.ifStartAtStartup)
			{
				this.Visible = false;
				this.ifStartAtStartup = false;
			}
		}

	}
}
