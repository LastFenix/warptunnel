using System;
using System.Threading;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using WarpTunnel;

namespace WarpTunnel
{
	public class UDPOutputBridge
	{
		private Socket TCPin = null;
		private Socket UDPout = null;
		public int TcpInputPort = 0;
		private Thread TCPinThread = null;
		private Thread UDPoutThread = null;
		private EndPoint remoteEP = null;
		private const int sizeByte = sizeof(byte);
		private const int sizeShort = sizeof(ushort);
		private int content_lenght = 0;
		private Socket connected_sck = null;
	
		public UDPOutputBridge(IPEndPoint remoteHostEP,string localinterface,int server_content_lenght)
		{
			this.content_lenght = server_content_lenght;
			this.remoteEP = (EndPoint)remoteHostEP;

			this.TCPin = find_TCP_accept_port(localinterface,remoteHostEP.Port,ref this.TcpInputPort);
			this.UDPout = find_UDP_accept_port(localinterface,this.TcpInputPort);

			this.TCPin.Blocking = true;
			this.UDPout.Blocking = true;

			this.TCPinThread = new Thread(new ThreadStart(this.TCPinThread_routine));
			this.UDPoutThread = new Thread(new ThreadStart(this.UDPoutThread_routine));

			this.TCPinThread.Start();
			this.UDPoutThread.Start();

		}

		private void TCPinThread_routine()
		{
			bool ifClosed = false;
			byte[] dataRead = null;
			int n = 0;

			try
			{
				this.connected_sck = this.TCPin.Accept();

				this.connected_sck.Blocking = false;

				this.connected_sck.Poll(-1,SelectMode.SelectRead);
				this.connected_sck.Receive(new byte[1],0,1,SocketFlags.None);
				this.connected_sck.Send(new byte[]{255},0,1,SocketFlags.None);

				this.connected_sck.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.Linger,new LingerOption (true, 0));
				this.connected_sck.SetSocketOption(SocketOptionLevel.Tcp,SocketOptionName.NoDelay,1);
				this.connected_sck.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.ReceiveBuffer,Tunnel.SOCKET_BUFFER_SIZE);
				this.connected_sck.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.SendBuffer,Tunnel.SOCKET_BUFFER_SIZE);
				this.connected_sck.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.KeepAlive,1);
			} 
			catch
			{
				return;
			}

			while(!ifClosed)
			{
				try
				{
					if(!this.connected_sck.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
					{
						continue;
						//this.reset_bridge();
						//ifClosed = true;
						//break;
					}

					if(this.connected_sck.Available == 0)
					{
						this.reset_bridge();
						ifClosed = true;
						break;
					}

					dataRead = new byte[sizeByte];
 
					this.connected_sck.Receive(dataRead,0,sizeByte,SocketFlags.None);

					if(dataRead[0] == UDPInputBridge.UDPPacketStartByte)
					{
					
						dataRead = new byte[sizeShort];

						n = 0;
						while(n < sizeShort)
						{
							if(!this.connected_sck.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
							{
								this.reset_bridge();
								ifClosed = true;
								break;
							}

							if(this.connected_sck.Available == 0)
							{
								this.reset_bridge();
								ifClosed = true;
								break;
							}

							n += this.connected_sck.Receive(dataRead,n,(sizeShort - n),SocketFlags.None );
						}

						ushort dataLen = BitConverter.ToUInt16(dataRead,0);
						dataRead = new byte[dataLen];
						n = 0;

						while(n < dataLen)
						{
							if(!this.connected_sck.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
							{
								this.reset_bridge();
								ifClosed = true;
								break;
							}

							if(this.connected_sck.Available == 0)
							{
								this.reset_bridge();
								ifClosed = true;
								break;
							}

							n += this.connected_sck.Receive(dataRead,n,(dataLen - n),SocketFlags.None );
						}

						this.connected_sck.Send(new byte[]{255},0,1,SocketFlags.None);

						this.UDPout.SendTo(dataRead,0,dataLen,SocketFlags.None,this.remoteEP);
					}

				}
				catch 
				{
					this.reset_bridge();
					ifClosed = true;
					break;
				}
			}
		}

		private void UDPoutThread_routine()
		{
			byte[] udp_data = new byte[this.content_lenght];
			int n = 0;
			bool ifClosed = false;
			byte[] tunnel_message = new byte[ sizeByte + sizeShort + this.content_lenght ];


			while(!ifClosed)
			{
				try
				{
					EndPoint tempEP = (EndPoint)(new IPEndPoint(IPAddress.Any, 0));
					n = this.UDPout.ReceiveFrom(udp_data,ref tempEP);

					//UPD Packet :
					//1 byte (start UDPPacket)
					//2 bytes (length of data)
					//n bytes (UDP Data)

					if(n > 0 && this.connected_sck != null)
					{
						if(tunnel_message.Length < (sizeByte + sizeShort + n))
							tunnel_message = new byte[(sizeByte + sizeShort + n)];

						tunnel_message[0] = UDPInputBridge.UDPPacketStartByte;
						Array.Copy(BitConverter.GetBytes(((ushort)n)),0,tunnel_message,1,sizeShort);
						Array.Copy(udp_data,0,tunnel_message,sizeByte + sizeShort,n);

						n = (sizeByte + sizeShort + n);
						int sended = 0;
						while(sended < n)
							sended += this.connected_sck.Send(tunnel_message,sended,(n - sended),SocketFlags.None);
					}

				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
					this.reset_bridge();
					ifClosed = true;
					break;
				}
			}
		}

		public void stop_bridge()
		{
			this.reset_bridge();

			if(this.TCPinThread != null)
			{
				this.TCPinThread.Abort();
				this.TCPinThread.Join();
				this.TCPinThread = null;
			}

			if(this.UDPoutThread != null)
			{
				this.UDPoutThread.Abort();
				this.UDPoutThread.Join();
				this.UDPoutThread = null;
			}
		}

		private void reset_bridge()
		{
			if(this.TCPin != null)
			{
				if(this.TCPin.Connected)
					this.TCPin.Shutdown(SocketShutdown.Both);

				try
				{
					this.TCPin.Close();
				} 
				catch
				{
				}
			}

			if(this.connected_sck != null)
			{
				if(this.connected_sck.Connected)
					this.connected_sck.Shutdown(SocketShutdown.Both);

				try
				{
					this.connected_sck.Close();
				} 
				catch
				{
				}
			}

			if(this.UDPout != null)
			{
				try
				{
					this.UDPout.Shutdown(SocketShutdown.Both);
				} 
				catch
				{
				}

				try 
				{
					this.UDPout.Close();
				} 
				catch
				{
				}
			}
		}

		private static Socket find_TCP_accept_port(string localinterface,int port,ref int finded_free_port)
		{
			lock(typeof(UDPOutputBridge))
			{
				int new_port = port;
				bool ifPortFind = false;
				Socket new_binding_socket = null;
				IPEndPoint new_binding_address = null;

				while(!ifPortFind)
				{
					try
					{
						new_port++;
                        new_binding_address = new IPEndPoint(Dns.GetHostEntry(localinterface).AddressList[0], new_port);
						new_binding_socket = new Socket(new_binding_address.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
						new_binding_socket.Bind(new_binding_address);
						new_binding_socket.Listen(1);
						finded_free_port = new_port;
						ifPortFind = true;
						return new_binding_socket;
					} 
					catch(Exception ex)
					{
						Console.WriteLine(ex.Message);

						if(new_binding_socket != null)
						{
							try
							{
								new_binding_socket.Close();
								new_binding_socket = null;
							} 
							catch
							{
							}
						}
					} 
				}

				return null;
			}
		}


		private static Socket find_UDP_accept_port(string localinterface,int port)
		{
			lock(typeof(UDPOutputBridge))
			{
				int new_port = port;
				bool ifPortFind = false;
				Socket new_binding_socket = null;
				IPEndPoint new_binding_address = null;

				while(!ifPortFind)
				{
					try
					{
						new_port++;
                        new_binding_address = new IPEndPoint(Dns.GetHostEntry(localinterface).AddressList[0], new_port);
						new_binding_socket = new Socket(new_binding_address.Address.AddressFamily,SocketType.Dgram,ProtocolType.Udp);
						new_binding_socket.Bind(new_binding_address);
						ifPortFind = true;
						return new_binding_socket;
					} 
					catch(Exception ex)
					{
						Console.WriteLine(ex.Message);

						if(new_binding_socket != null)
						{
							try
							{
								new_binding_socket.Close();
								new_binding_socket = null;
							} 
							catch
							{
							}
						}
					} 
				}

				return null;
			}

		}

	}
}
