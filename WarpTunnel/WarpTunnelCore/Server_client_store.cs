using System;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Collections;

namespace WarpTunnel
{
	public class Server_client_store
	{

		private const int SLEEP_TIMEOUT_MILLIS = 100;

		private static Hashtable client_store_put = Hashtable.Synchronized( new Hashtable(10));
		private static Hashtable client_store_get = Hashtable.Synchronized( new Hashtable(10));
		private static Hashtable client_store_post = Hashtable.Synchronized( new Hashtable(10));

		private static Hashtable clients_store = Hashtable.Synchronized( new Hashtable(10));

		private static AutoResetEvent autoEvent = new AutoResetEvent(true);
		private static TimerCallback timerDelegate = new TimerCallback(Server_client_store.check_timeout_clients_on_server);
		private static Timer stateTimer = new Timer(timerDelegate, autoEvent, Tunnel.MAX_TIMEOUT_CLIENT_ON_SERVER, Timeout.Infinite);

		private static void check_timeout_clients_on_server(Object stateInfo)
		{
			Server_client_store.remove_timeout_clients();
		}

		public static void get_client_giud_and_http_method(Socket sk,ref HTTPStructures.Http_method method,ref Guid client_id)
		{
			sk.Blocking = true;

			if(!sk.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
			{
				sk.Blocking = false;
				return;
			}

			byte[] arr = new byte[sk.Available];
			string sk_data = null;
			string guid = null,http_met = null;
			int calc_temp = 0;

			sk.Receive(arr,0,arr.Length,SocketFlags.Peek);

			sk_data = Encoding.ASCII.GetString(arr,0,arr.Length);


			http_met = sk_data.Substring(0,sk_data.IndexOf(" ",0));

			method = HTTPStructures.http_string_to_method(http_met,http_met.Length);

			if((calc_temp = sk_data.IndexOf("&ID=",0)) == -1)
			{
				client_id = Guid.Empty;
			} 
			else
			{
				calc_temp += "&ID=".Length;
				guid = sk_data.Substring(calc_temp,sk_data.IndexOf(" ",calc_temp) - calc_temp);

				client_id = new Guid(guid);
			}

			sk.Blocking = false;
		}

		private static Hashtable get_hashtable_from_method(HTTPStructures.Http_method method)
		{
			Hashtable ret;

			switch (method)
			{
				case HTTPStructures.Http_method.HTTP_GET :
					ret = client_store_get;
					break;

				case HTTPStructures.Http_method.HTTP_POST :
					ret = client_store_post;
					break;

				case HTTPStructures.Http_method.HTTP_PUT :
					ret = client_store_put;
					break;

				default:
					ret = null;
					break;
			}

			return ret;
		}
											 
		//return true if is a new client
		public static bool add_client_to_store(Socket sk, Guid client_id,HTTPStructures.Http_method method)
		{
			Hashtable client_store = get_hashtable_from_method(method);

			if(client_store == null)
				return false;

			bool ret = false;

			if(!clients_store.ContainsKey(client_id))
			{
				clients_store.Add(client_id,DateTime.Now);
				ret = true;
			}
			else
			{
				DateTime client_time = (DateTime)clients_store[client_id];
				TimeSpan elapsed = DateTime.Now.Subtract(client_time);

				if(elapsed.TotalMilliseconds >= Tunnel.MAX_TIMEOUT_CLIENT_ON_SERVER)
				{
					remove_timeout_clients();
					clients_store.Add(client_id,DateTime.Now);
					ret = true;
				}
				else
				{
					ret = false;
					clients_store[client_id] = DateTime.Now;

				}
			}

			try 
			{
				client_store.Add(client_id,sk);
			} 
			catch
			{
			}

			return ret;

		}

		private static void remove_timeout_clients()
		{
			IDictionaryEnumerator dict_enum = clients_store.GetEnumerator();

			while(dict_enum.MoveNext())
			{
				DateTime client_time = (DateTime)dict_enum.Value;
				TimeSpan elapsed = DateTime.Now.Subtract(client_time);

				if(elapsed.TotalMilliseconds >= Tunnel.MAX_TIMEOUT_CLIENT_ON_SERVER)
					remove_client_from_store( (Guid)dict_enum.Key );

			}

		}

		public static void remove_client_from_store(Guid client_id)
		{

			if(clients_store.ContainsKey(client_id))
			{
				clients_store.Remove(client_id);
			}


			if(client_store_get.ContainsKey(client_id))
			{
				try_close_sk((Socket)client_store_get[client_id]);
				client_store_get.Remove(client_id);
			}


			if(client_store_put.ContainsKey(client_id))
			{
				try_close_sk((Socket)client_store_get[client_id]);
				client_store_put.Remove(client_id);
			}

			if(client_store_post.ContainsKey(client_id))
			{
				try_close_sk((Socket)client_store_get[client_id]);
				client_store_post.Remove(client_id);
			}


		}

		private static void try_close_sk(Socket sk)
		{
			if(sk != null)
			{
				try
				{
					sk.Shutdown(SocketShutdown.Both);
				} 
				catch
				{
				}

				try
				{
					sk.Close();
				} 
				catch
				{
				}
			}
		}

		public static bool is_socket_available(Guid client_id,HTTPStructures.Http_method method)
		{
			Hashtable client_store = get_hashtable_from_method(method);

			if(client_store == null)
				return false;


			return client_store.ContainsKey(client_id);
		}

		public static Socket wait_and_get_socket(Guid client_id,HTTPStructures.Http_method method)
		{
			int timeout_millis = 0;

			if(clients_store.ContainsKey(client_id))
			{
				Hashtable client_store = get_hashtable_from_method(method);

				if(client_store == null)
					return null;

				while(!client_store.ContainsKey(client_id))
				{ 
					Thread.Sleep( SLEEP_TIMEOUT_MILLIS );

					timeout_millis += SLEEP_TIMEOUT_MILLIS;

					if(timeout_millis >= Tunnel.MAX_TIMEOUT_READ_WAIT)
					{
						remove_client_from_store(client_id);
						return null;
					}
				}

				Socket ret = (Socket)client_store[client_id];
				client_store.Remove(client_id);

				return ret;

			} else
				return null;

		}


	}
}
