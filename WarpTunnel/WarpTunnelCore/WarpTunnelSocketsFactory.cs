using System;
using System.Net;
using System.Net.Sockets;

namespace WarpTunnel
{
	/// <summary>
	/// Summary description for WarpTunnelClientSocketFactory.
	/// </summary>
	public class WarpTunnelSocketsFactory
	{

		public static Socket getServerSocket(ServerTunnel tunnel,bool if_UDP_Socket,int backlog)
		{
			IPEndPoint tunnel_EP = null;

			try 
			{
                tunnel_EP = new IPEndPoint(Dns.GetHostEntry(tunnel.forward_host).AddressList[0], tunnel.forward_port);
			} 
			catch
			{
				return null;
			}

			Socket tunnel_sck = null;

			if(!if_UDP_Socket)
			{
				try 
				{
					tunnel_sck = new Socket(tunnel_EP.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
					tunnel_sck.Bind(tunnel_EP);
					tunnel_sck.Listen(backlog);
				} 
				catch
				{
					return null;
				}
			} 
			else 
			{
				try 
				{
					tunnel_sck = new Socket(tunnel_EP.AddressFamily,SocketType.Dgram,ProtocolType.Udp);
					tunnel_sck.Bind(tunnel_EP);
				} 
				catch
				{
					return null;
				}
			}

			return tunnel_sck;

		}

		public static Socket getClientSocket(ClientTunnel tunnel,out EndPoint remoteEP)
		{
			IPEndPoint tunnel_EP = null;
			remoteEP = null;

			try 
			{
                tunnel_EP = new IPEndPoint(Dns.GetHostEntry(tunnel.forward_host).AddressList[0], tunnel.forward_port);
			} 
			catch
			{
				return null;
			}

			remoteEP = tunnel_EP;
			Socket tunnel_sck = null;

			if(tunnel.ClientProtocol == ClientTunnel.TunnelProtocol.TCP)
			{
				try 
				{
					tunnel_sck = new Socket(tunnel_EP.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
					tunnel_sck.Connect(tunnel_EP);
				} 
				catch
				{
					return null;
				}
			} 
			else if(tunnel.ClientProtocol == ClientTunnel.TunnelProtocol.UDP)
			{
				try 
				{
					tunnel_sck = new Socket(tunnel_EP.AddressFamily,SocketType.Dgram,ProtocolType.Udp);
				} 
				catch
				{
					return null;
				}
			}

			return tunnel_sck;
		}
	}
}
