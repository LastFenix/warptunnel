using System;
using System.IO;

namespace WarpTunnel
{

	public class DecompressBuffer
	{
		private MemoryStream bufferStream;
		private int writeBytes = 0;
		private int totCompressBytes = -1;

		public DecompressBuffer()
		{
			this.bufferStream = new MemoryStream(Tunnel.MAX_COPRESS_DATA);
		}

		public bool isAllBytesIntoBuffer()
		{
			if(this.writeBytes == this.totCompressBytes)
				return true;
			else
				return false;
		}

		public void decompressBytes(ref byte[] buf,ref int len)
		{
			if(isAllBytesIntoBuffer())
			{
				this.bufferStream.Seek(0,SeekOrigin.Begin);
				len = this.bufferStream.Read(buf,0,this.totCompressBytes);

				CompressData.decompressDatabuf(ref buf,ref len);

				this.totCompressBytes = -1;
				this.writeBytes = 0;
			}
		}

		public int getWriteBytes()
		{
			return (int)this.writeBytes;
		}

		public void write(byte[] buff,int offset,int len)
		{
			if(writeBytes == 0)
			{
				this.totCompressBytes = BitConverter.ToUInt16(buff,offset);
				this.bufferStream.Seek(0,SeekOrigin.Begin);
				this.bufferStream.Write(buff,(offset + Tunnel.sizeShort),(len -  Tunnel.sizeShort));
				writeBytes = writeBytes + (len -  Tunnel.sizeShort);
			} 
			else
			{
				this.bufferStream.Write(buff,offset,len);
				writeBytes = writeBytes + len;
			}
		}
	}
}
