using System;
using System.IO;
using System.Security.Cryptography;

namespace WarpTunnel
{
	public class SymmetricCryptDecrypt
	{

		public RSACryptoServiceProvider RSAEncrDecr = null;
		public RijndaelManaged cryptRijndael = null;
		public ICryptoTransform cryptTrasform = null;
		public ICryptoTransform decryptTrasform = null;

		private static void crypt_decrypt_bytes(bool ifcrypt,SymmetricCryptDecrypt CryptDecryptOptions,ref byte[] data,ref int datalen)
		{
			try 
			{
				MemoryStream memoryStream = new MemoryStream();
				memoryStream.SetLength(0);
				ICryptoTransform used_transform = ifcrypt?CryptDecryptOptions.cryptTrasform:CryptDecryptOptions.decryptTrasform;
				CryptoStream cryptoStream = new CryptoStream(memoryStream, used_transform, CryptoStreamMode.Write);

				cryptoStream.Write(data, 0, datalen);
				cryptoStream.Flush();

				try 
				{
					cryptoStream.FlushFinalBlock();
				} 
				catch
				{
				}

				datalen = (int)memoryStream.Length;
				memoryStream.Position = 0;
				memoryStream.Read(data,0,datalen);

				memoryStream.Close();
				cryptoStream.Close();
				cryptoStream.Clear();

				cryptoStream = null;
				memoryStream = null;
			} 
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				datalen = -1;
			}
		}

		public static void SymmetricCrypt(SymmetricCryptDecrypt CryptDecryptOptions,ref byte[] data,ref int datalen)
		{
			crypt_decrypt_bytes(true, CryptDecryptOptions,ref data,ref datalen);
		}

		public static void SymmetricDecrypt(SymmetricCryptDecrypt CryptDecryptOptions,ref byte[] data,ref int datalen)
		{
			crypt_decrypt_bytes(false, CryptDecryptOptions,ref data,ref datalen);

		}
	}
}
