using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using WarpTunnelUtils;
using WarpTunnelAuth;

namespace WarpTunnel
{
	delegate void AsyncServerReader(ref byte[] RBuf,Socket RSocket,Tunnel RTunnel, ref bool IfClose, DecompressBuffer decBuf);
	delegate void AsyncServerWriter(ref byte[] WBuf,Socket RSocket,Tunnel RTunnel, ref bool IfClose);
	/// <summary>
	/// Summary description for ServerTunnel.
	/// </summary>
	[Serializable]
	public class ServerTunnel
	{

		private Thread ServerThread;
		private Tunnel AcceptTunnel;

		public bool ifCompatible;
		public bool ifCompressData;
		public bool stealthModeEnable;
		public bool ifUDPTraffic;
		public string me;
		public string host;
		public int port;
		public string forward_host;
		public int forward_port;
		public int content_length;
		public bool strict_content_length;
		public int keep_alive;
		public int max_connection_age;
		public Tunnel_Auth_Dest returned_auth_destination = null;
		public Tunnel.Thread_Mode serverThreadingMode;



		public bool disconnect = false;

		public WarpTunnelAuthenticator tunnelAuth;

		public ServerTunnel()
		{
			this.me = "";
			this.ifCompatible = false;
			this.ifCompressData = false;
			this.stealthModeEnable = false;
			this.ifUDPTraffic = false;
			this.host = null;
			this.port = Tunnel.DEFAULT_HOST_PORT;
			this.forward_host = "localhost";
			this.forward_port = -1;
			this.content_length = Tunnel.DEFAULT_BUFFER_LENGTH;
			this.strict_content_length = true;
			this.keep_alive = Tunnel.DEFAULT_KEEP_ALIVE;
			this.max_connection_age = Tunnel.DEFAULT_CONNECTION_MAX_TIME;
			this.serverThreadingMode = Tunnel.Thread_Mode.SoftThreading;
			this.tunnelAuth = null;
		}

		~ServerTunnel()
		{
			this.stopServer();
		}

		public void startServer()
		{
			GC.Collect();
			ServerThread = new Thread(new ThreadStart(this.serverAcceptCycle));
			ServerThread.Start();
		}

		public void stopServer()
		{
			if(this.ServerThread != null)
			{
				this.disconnect = true;
				try 
				{
					this.AcceptTunnel.tunnel_destroy();

					this.AcceptTunnel.destroy_server_socket();
				} 
				catch{}

				try 
				{
					this.AcceptTunnel.tunnel_in_disconnect();
					this.AcceptTunnel.tunnel_out_disconnect();
				} 
				catch {}

				this.ServerThread.Abort();
				try 
				{
					this.ServerThread.Join();
				} 
				catch{}

				this.ServerThread = null;
				GC.Collect();
			}
		}

		private void serverAcceptCycle()
		{
			Logger.writeToLog ("creating a new tunnel",false,true,this.me);

			Guid client_id = Guid.Empty;
			Socket sk;
			HTTPStructures.Http_method method = HTTPStructures.Http_method.HTTP_UNKNOW;
			Socket used_server_socket = null;

			while(!disconnect)
			{
				if(used_server_socket == null)
					used_server_socket = Tunnel.create_new_serverSocket(this.host,this.port);

				client_id = Guid.Empty;
				sk = null;
				method = HTTPStructures.Http_method.HTTP_UNKNOW;

				if(!used_server_socket.Blocking)
					used_server_socket.Blocking = true;

				try
				{
					sk = used_server_socket.Accept();
				} 
				catch
				{
					continue;
				}

				Server_client_store.get_client_giud_and_http_method(sk,ref method, ref client_id);

				if(method == HTTPStructures.Http_method.HTTP_UNKNOW)
				{
					try
					{
						sk.Close();
					} 
					catch
					{
					}

					continue;

				}

				if(Server_client_store.add_client_to_store(sk,client_id,method))
				{
					AcceptTunnel = create_new_tunnel(used_server_socket,client_id);

					if(AcceptTunnel != null && used_server_socket != null)
					{
						Logger.writeToLog ("creating tunnel handler thread",false,true,this.me);

						new ClientThreadInstance(AcceptTunnel,this);
					}
				}
			}


			AcceptTunnel = null;

			if (used_server_socket != null )
			{
				used_server_socket.Close();
				used_server_socket = null;
			}
		}

		private Tunnel create_new_tunnel(Socket used_server_socket,Guid client_id)
		{
			try 
			{
				Tunnel tunnel = Tunnel.tunnel_new_server (used_server_socket,this.host, this.port, this.content_length,this.me,this.ifCompatible,this.tunnelAuth,client_id);
				if (tunnel == null)
				{
					Logger.writeToLog ("couldn't create tunnel",true,true,this.me);
					return null;
				}

				if (tunnel.tunnel_setopt ("strict_content_length",this.strict_content_length) == -1)
					Logger.writeToLog ("tunnel_setopt strict_content_length error !",false,true,this.me);

				if (tunnel.tunnel_setopt ("keep_alive",this.keep_alive) == -1)
					Logger.writeToLog ("tunnel_setopt keep_alive error !", false,true,this.me);

				if (tunnel.tunnel_setopt ("max_connection_age",this.max_connection_age) == -1)
					Logger.writeToLog ("tunnel_setopt max_connection_age error !", false,true,this.me);

				return tunnel;
			} 
			catch
			{
				return null;
			}
		}

	}

	internal class ClientThreadInstance
	{

		private string me;
		private bool ifCompressData;
		private bool stealthModeEnable;
		private bool ifCompatible;
		private bool ifUDPTraffic;
		private string host;
		private int port;
		private string forward_host;
		private int forward_port;
		private int content_length;
		private bool strict_content_length;
		private int keep_alive;
		private int max_connection_age;
		private Tunnel_Auth_Dest returned_auth_destination;
		private int max_readable_data = 0;
		private Tunnel.Thread_Mode serverThreadingMode;

		private bool closed = false;
		private ServerTunnel parentServer;
		
		private Tunnel ClientTunnel = null;


		public ClientThreadInstance(Tunnel tunnel,ServerTunnel parent)
		{
			this.me = parent.me;
			this.ifCompatible = parent.ifCompatible;
			this.ifCompressData = parent.ifCompressData;
			this.stealthModeEnable = parent.stealthModeEnable;
			this.ifUDPTraffic = parent.ifUDPTraffic;
			this.host = parent.host;
			this.port = parent.port;
			this.returned_auth_destination = parent.returned_auth_destination;

			this.forward_host = parent.forward_host;
			this.forward_port = parent.forward_port;

			this.content_length = parent.content_length;
			this.strict_content_length = parent.strict_content_length;
			this.keep_alive = parent.keep_alive;
			this.max_connection_age = parent.max_connection_age;
			this.serverThreadingMode = parent.serverThreadingMode;

			parentServer = parent;

			ClientTunnel = tunnel;
			Thread ClientThread = new Thread(new ThreadStart(this.run_connection));
			ClientThread.Start();
		}

		private void run_connection()
		{
			Socket fd = null;
			DateTime last_tunnel_write;
			UDPOutputBridge OutUDPBridge = null;
			byte[] WBuf = null;
			byte[] RBuf = null;
			DecompressBuffer decBuf = new DecompressBuffer();

			Logger.writeToLog ("waiting for tunnel connection",false,true,this.me);
			if (ClientTunnel.tunnel_accept(false,ref this.ifCompressData,ref this.stealthModeEnable,ref this.ifUDPTraffic,ref this.returned_auth_destination) == -1)
			{
				Logger.writeToLog ("couldn't accept connection !!", true,true,this.me);
				return;
			}

			if(this.returned_auth_destination != null)
			{
				this.forward_host = this.returned_auth_destination.host;
				this.forward_port = this.returned_auth_destination.port;
			}

			if (this.forward_port != -1)
			{
				if(!this.ifUDPTraffic)
				{
                    IPHostEntry lipa = Dns.GetHostEntry(this.forward_host);
					IPEndPoint lep = new IPEndPoint(lipa.AddressList[0], this.forward_port);
					fd = new Socket(lep.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);

					try
					{
						fd.Connect(lep);
						this.ClientTunnel.tunnel_setsockopts(fd);
						Logger.writeToLog ("Connect  (\"" + this.forward_host 
							+ ":" + this.forward_port + "\")",false,true,this.me);
					} 
					catch (Exception ex)
					{
						Logger.writeToLog ("couldn't connect to " 
							+ this.forward_host + ":" 
							+ this.forward_port + ": " + ex.Message + "\n",true,true,this.me);

						return;
					}
				} 
				else
				{
                    IPHostEntry lipa = Dns.GetHostEntry(this.forward_host);
					IPEndPoint lep = new IPEndPoint(lipa.AddressList[0], this.forward_port);

					OutUDPBridge = new UDPOutputBridge(lep,ClientTunnel.server_address.Address.ToString(),this.ClientTunnel.content_length);

                    lipa = Dns.GetHostEntry(ClientTunnel.server_address.Address.ToString());
					lep = new IPEndPoint(lipa.AddressList[0], OutUDPBridge.TcpInputPort);

					fd = new Socket(lep.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);

					try
					{
						fd.Connect(lep);
						this.ClientTunnel.tunnel_setsockopts(fd);
						Logger.writeToLog ("Connect  (\"" + this.forward_host 
							+ ":" + this.forward_port + "\")",false,true,this.me);
					} 
					catch (Exception ex)
					{
						Logger.writeToLog ("couldn't connect to " 
							+ this.forward_host + ":" 
							+ this.forward_port + ": " + ex.Message + "\n",true,true,this.me);

						return;
					}
				}

			} 
			else 
			{
				return;
			}

			AsyncClientReader Reader = null;
			AsyncClientWriter Writer = null;

			if(this.serverThreadingMode == Tunnel.Thread_Mode.HeavyThreading)
			{
				Reader = new AsyncClientReader(this.TunnelReader);
				Writer = new AsyncClientWriter(this.TunnelWriter);
			}

			IAsyncResult RRes = null;
			IAsyncResult WRes = null;
			WBuf = new byte[Tunnel.DIM_READWRITE_BUFFER_SIZE + (Tunnel.sizeInt + Tunnel.MAX_COMPRESSION_OVERHEAD_SIZE)];
			RBuf = new byte[Tunnel.DIM_READWRITE_BUFFER_SIZE + (Tunnel.sizeInt + Tunnel.MAX_COMPRESSION_OVERHEAD_SIZE)];

			if(!this.ifCompressData && !this.stealthModeEnable)
				max_readable_data = Tunnel.DIM_READWRITE_BUFFER_SIZE;
			else
				max_readable_data = Tunnel.DIM_READWRITE_BUFFER_SIZE < Tunnel.MAX_COPRESS_DATA?Tunnel.DIM_READWRITE_BUFFER_SIZE:Tunnel.MAX_COPRESS_DATA;

			if(ClientTunnel.ifUseAsimmetricDataEncription)
				max_readable_data = max_readable_data - 2*ClientTunnel.crypt_decrypt.cryptRijndael.BlockSize;

			closed = false;
			last_tunnel_write = DateTime.Now.ToUniversalTime();
			ArrayList sockList = new ArrayList(2);


			try 
			{
				while ((!closed && !parentServer.disconnect))
				{
					int timeout;
					DateTime t;
  
					sockList.Clear();
					sockList.Add(fd);

					if(ClientTunnel.tunnel_pollin_fd() == null)
					{
						closed = true;
						continue;
					}

					sockList.Add(ClientTunnel.tunnel_pollin_fd());

					t = DateTime.Now.ToUniversalTime();
					timeout = 1000 * (this.keep_alive - ((TimeSpan)t.Subtract(last_tunnel_write)).Seconds);
					if (timeout < 0)
						timeout = 0;

					Socket.Select(sockList,null,null,(timeout * 1000));

					if (sockList.Count == 0)
					{
						Logger.writeToLog ("poll() timed out",false,true,this.me);

						if(ClientTunnel.tunnel_padding (1) == -1)
						{
							closed = true;
							continue;
						}

						last_tunnel_write = DateTime.Now.ToUniversalTime();
						continue;
					}

					for (int i = 0; i < sockList.Count ; i++)
					{
						if(((Socket)sockList[i]) == ClientTunnel.tunnel_pollin_fd())
						{
							if(this.serverThreadingMode == Tunnel.Thread_Mode.HeavyThreading)
							{
								RRes = Reader.BeginInvoke(ref RBuf,fd,this.ClientTunnel,ref closed,decBuf,null,null);
							} 
							else if(this.serverThreadingMode == Tunnel.Thread_Mode.SoftThreading)
							{
								this.TunnelReader(ref RBuf,fd,this.ClientTunnel,ref closed,decBuf);
							}
						} 
						else 
						{
							if(this.serverThreadingMode == Tunnel.Thread_Mode.HeavyThreading)
							{
								WRes = Writer.BeginInvoke(ref WBuf,fd,this.ClientTunnel,ref closed,null,null);
							}
							else if(this.serverThreadingMode == Tunnel.Thread_Mode.SoftThreading)
							{
								this.TunnelWriter(ref WBuf,fd,this.ClientTunnel,ref closed);
								last_tunnel_write = DateTime.Now.ToUniversalTime();
							}

						}
					}

					if(this.serverThreadingMode == Tunnel.Thread_Mode.HeavyThreading)
					{

						if(RRes != null)
						{

							RRes.AsyncWaitHandle.WaitOne();

							if(RRes.IsCompleted)
								Reader.EndInvoke(ref RBuf,ref closed,RRes);

							RRes.AsyncWaitHandle.Close();
							RRes = null;

						}

						if(WRes != null)
						{

							WRes.AsyncWaitHandle.WaitOne();

							if(WRes.IsCompleted)
								Writer.EndInvoke(ref WBuf,ref closed,WRes);

							WRes.AsyncWaitHandle.Close();
							WRes = null;

							last_tunnel_write = DateTime.Now.ToUniversalTime();
						}
					}
				}
			} 
			catch(Exception ex) 
			{
				Console.WriteLine(ex.Message);
				Logger.writeToLog("Error : " + ex.Message,true,true,this.me);
			}


			Logger.writeToLog ("closing tunnel",false,true,this.me);
			
			if (fd != null || fd.Connected)
			{
				fd.Shutdown(SocketShutdown.Both);
				fd.Close();
				fd = null;
			}
 
			Logger.writeToLog ("destroying tunnel",false,true,this.me);
			this.ClientTunnel.tunnel_destroy();
			this.ClientTunnel = null;

			if(this.ifUDPTraffic)
			{
				OutUDPBridge.stop_bridge();
				OutUDPBridge = null;
			}
 
			Logger.writeToLog ("thread exiting",false,true,this.me);
		}

		private void TunnelWriter(ref byte[] WBuf,Socket WSocket,Tunnel WTunnel, ref bool IfClose)
		{
			int len = 0;

			try  
			{

				if(WSocket.Available == 0)
				{
					IfClose = true;
					return;
				}

				WSocket.Blocking = false;
				len = WSocket.Receive(WBuf,0,max_readable_data,SocketFlags.None);

				if (len == -1 || len == 0 )//|| !WSocket.Connected)
				{
					IfClose = true;
					return;
				} 
								
				if(len > 0)
				{

					if(!this.ifCompatible)
					{
						if(this.ifCompressData || this.stealthModeEnable)
							CompressData.compressDatabuf(ref WBuf,ref len,this.stealthModeEnable);
					}

					if(!this.ifCompatible && WTunnel.ifUseAsimmetricDataEncription)
					{
						if(WTunnel.tunnel_write_and_crypt (WBuf, len) == -1)
						{
							IfClose = true;
							return;
						}
					} 
					else
					{
						if(WTunnel.tunnel_write (WBuf, len) == -1)
						{
							IfClose = true;
							return;
						}
					}

				}

			}
			catch 
			{
				IfClose = true;
			}
		}

		private void TunnelReader(ref byte[] RBuf,Socket RSocket,Tunnel RTunnel, ref bool IfClose, DecompressBuffer decBuf)
		{

			int len = 0;
				
			try 
			{

				if(!this.ifCompatible && RTunnel.ifUseAsimmetricDataEncription)
					len = RTunnel.tunnel_read_and_decrypt(ref RBuf, RBuf.Length);
				else
					len = RTunnel.tunnel_read (ref RBuf, RBuf.Length);

				if(len < 0)
				{
					IfClose = true;
					return;
				}
														
				if (len > 0)
				{

					RSocket.Blocking = false;

					int n = 0;

					if(!this.ifCompatible)
					{
						if(this.ifCompressData || this.stealthModeEnable)
						{
							decBuf.write(RBuf,0,len);
							if(decBuf.isAllBytesIntoBuffer())
								decBuf.decompressBytes(ref RBuf,ref len);
							else
								return;
						}
					}

					while(n < len)
					{
						if(RSocket.Poll(Tunnel.MAX_TUNNEL_WRITE_TIMEOUT,SelectMode.SelectWrite))
							n += RSocket.Send(RBuf,n,(len - n),SocketFlags.None);
						else
						{
							IfClose = true;
							return;
						}
					}

				}

			}
			catch( Exception ex)
			{
				Console.WriteLine(ex.Message);
				IfClose = true;
			}

		}

	}

}
