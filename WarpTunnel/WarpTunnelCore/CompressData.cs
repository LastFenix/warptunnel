using System;
using ICSharpCode.SharpZipLib.Zip.Compression;

namespace WarpTunnel
{
	public class CompressData
	{
		private static Deflater compressAgent = null;
		private static byte[] out_compress_buf = null;

		private static Inflater decompressAgent = null;
		private static byte[] out_dec_compress_buf = null;

		public static void compressDatabuf(ref byte[] buf,ref int len,bool steathMode)
		{
			lock(typeof(CompressData))
			{
				if(out_compress_buf == null || out_compress_buf.Length < buf.Length)
					out_compress_buf = new byte[buf.Length];

				if(compressAgent == null || compressAgent.GetLevel() != (steathMode?Deflater.NO_COMPRESSION:Deflater.BEST_SPEED))
					compressAgent = new Deflater(steathMode?Deflater.NO_COMPRESSION:Deflater.BEST_SPEED,true);

				compressAgent.SetInput(buf,0,len);
				compressAgent.Flush();
				compressAgent.Finish();
				len = compressAgent.Deflate(out_compress_buf,Tunnel.sizeShort,out_compress_buf.Length);
				Array.Copy(BitConverter.GetBytes((ushort)len),0,out_compress_buf,0,Tunnel.sizeShort);
				len = len + Tunnel.sizeShort;
				buf = out_compress_buf;

				compressAgent.Reset();
			}
			//compressAgent = null;
		}

		public static void decompressDatabuf(ref byte[] buf,ref int len)
		{
			lock(typeof(CompressData))
			{
				if(decompressAgent == null)
					decompressAgent = new Inflater(true);

				if(out_dec_compress_buf == null || out_dec_compress_buf.Length < buf.Length)
					out_dec_compress_buf = new byte[buf.Length];


				try
				{
					decompressAgent.SetInput(buf,0,len);
					len = decompressAgent.Inflate(out_dec_compress_buf,0,out_dec_compress_buf.Length);
					buf = out_dec_compress_buf;
				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				decompressAgent.Reset();
			}
			//decompressAgent = null;
		}
	}
}
