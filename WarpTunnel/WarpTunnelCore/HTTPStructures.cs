using System;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;
using ProxyAuth;
using WarpTunnelUtils;

namespace WarpTunnel
{
	/// <summary>
	/// Summary description for HTTPStructures.
	/// </summary>
	public class HTTPStructures
	{

		public enum Http_method
		{
			HTTP_GET,
			HTTP_PUT,
			HTTP_POST,
			HTTP_OPTIONS,
			HTTP_HEAD,
			HTTP_DELETE,
			HTTP_TRACE,
			HTTP_UNKNOW
		}

		public enum ProxyAuthMethod
		{
			BASIC_AUTENTICATION = 0,
			DIGEST_AUTHENTICATION = 1,
			NTLM_AUTHENTICATION = 2,
			SOCK5_AUTHENTICATION = 3,
			SOCK4_AUTHENTICATION = 4
		}

		private static int
			http_method (ref Socket fd, Http_destination dest,
			Http_method method, int length,Guid clientID)
		{
			string str = ""; 
			string UriWarpServer = "";
			Http_request request;
			int n;

			if (fd  == null)
			{
				Logger.writeToLog ("http_method: fd == -1",true,false,"HTTPStructure");
				return -1;
			}

			n = 0;
			if (dest.proxy_name != null && (dest.AuthMeth != HTTPStructures.ProxyAuthMethod.SOCK5_AUTHENTICATION && dest.AuthMeth != HTTPStructures.ProxyAuthMethod.SOCK4_AUTHENTICATION))
			{
				str = @"http://" + dest.host_name + ':' + dest.host_port;
				n = str.Length;
			} 
			else if (dest.AuthMeth == HTTPStructures.ProxyAuthMethod.SOCK5_AUTHENTICATION || 
				dest.AuthMeth == HTTPStructures.ProxyAuthMethod.SOCK4_AUTHENTICATION)
			{
				if(dest.AuthMeth == HTTPStructures.ProxyAuthMethod.SOCK5_AUTHENTICATION)
				{
					if(!SocksProxy.ConnectToSocks5Proxy(fd,dest.host_name,(ushort)dest.host_port,dest.username,dest.pass))
						return -1;
				}
				else if(dest.AuthMeth == HTTPStructures.ProxyAuthMethod.SOCK4_AUTHENTICATION)
				{
					if(!Socks4Proxy.ConnectToSocks4Proxy(fd,dest.host_name,(ushort)dest.host_port,dest.username,dest.pass))
						return -1;
				}
			}

			str = str + "/index.html?crap=" + Convert.ToString((DateTime.Now.ToUniversalTime().Ticks / 10000)) + "&ID=" + clientID.ToString();

			UriWarpServer = str;

			request = http_create_request (ref method, str, 1, 1);
			if (request == null)
				return -1;

			str = dest.host_name + ':' + dest.host_port;
			http_add_header (ref request.header, "Host", str);

			if (length >= 0)
			{
				str = Convert.ToString(length);
				http_add_header (ref request.header, "Content-Length", str);
			}


			if(dest.username != null && dest.pass != null)
			{
				if (dest.AuthMeth == HTTPStructures.ProxyAuthMethod.BASIC_AUTENTICATION)
				{
					http_add_header (ref request.header,
						"Proxy-Authorization",
						dest.basic_proxy_authorization);
				} 
				else if (dest.AuthMeth == HTTPStructures.ProxyAuthMethod.DIGEST_AUTHENTICATION)
				{

					fd = DigestAuth.Get_Authentication_Header(length,fd,dest.username,dest.pass,UriWarpServer,http_method_to_string(method));
					if(fd != null)
					{
						string[] ProxyAuthHeader = DigestAuth.calculate_digest_response(fd,http_method_to_string(method));

						if(ProxyAuthHeader == null)
							return -1;

						http_add_header (ref request.header,
							ProxyAuthHeader[0],
							ProxyAuthHeader[1]);
					}
					else 
						return -1;

				}
				else if (dest.AuthMeth != HTTPStructures.ProxyAuthMethod.SOCK5_AUTHENTICATION &&
					dest.AuthMeth != HTTPStructures.ProxyAuthMethod.SOCK4_AUTHENTICATION)
					return -1;
			}


			if (dest.user_agent != null)
			{
				http_add_header (ref request.header,
					"User-Agent",
					dest.user_agent);
			}

			http_add_header (ref request.header, "Connection", "close");

			n = http_write_request (fd, ref request);
			http_destroy_request (ref request);
			return n;
		}

		public static int
			http_get (ref Socket fd, ref Http_destination dest,Guid clientID)
		{
			return http_method (ref fd, dest, Http_method.HTTP_GET, -1, clientID);
		}

		public static int
			http_put (ref Socket fd, ref Http_destination dest, int length,Guid clientID)
		{
			return http_method (ref fd, dest, Http_method.HTTP_PUT, length, clientID);
		}

		public static int
			http_post (ref Socket fd, ref Http_destination dest, int length,Guid clientID)
		{
			return http_method (ref fd, dest, Http_method.HTTP_POST, length, clientID);
		}

		public static int
			http_error_to_errno (int err)
		{
			/* Error codes taken from RFC2068. */
			switch (err)
			{
				case -1: /* system error */
					return -1;
				case -200: /* OK */
				case -201: /* Created */
				case -202: /* Accepted */
				case -203: /* Non-Authoritative Information */
				case -204: /* No Content */
				case -205: /* Reset Content */
				case -206: /* Partial Content */
					return 0;
				case -400: /* Bad Request */
					Logger.writeToLog ("http_error_to_errno: 400 bad request",true,false,"HTTPStructure");
					return 1; //EIO
				case -401: /* Unauthorized */
					Logger.writeToLog ("http_error_to_errno: 401 unauthorized",true,false,"HTTPStructure");
					return 2; // EACCES
				case -403: /* Forbidden */
					Logger.writeToLog ("http_error_to_errno: 403 forbidden",true,false,"HTTPStructure");
					return 2;
				case -404: /* Not Found */
					Logger.writeToLog ("http_error_to_errno: 404 not found",true,false,"HTTPStructure");
					return 3; //ENOENT
				case -411: /* Length Required */
					Logger.writeToLog ("http_error_to_errno: 411 length required",true,false,"HTTPStructure");
					return 1;
				case -413: /* Request Entity Too Large */
					Logger.writeToLog ("http_error_to_errno: 413 request entity too large",true,false,"HTTPStructure");
					return 1;
				case -505: /* HTTP Version Not Supported       */
					Logger.writeToLog ("http_error_to_errno: 413 HTTP version not supported",true,false,"HTTPStructure");
					return 1;
				case -100: /* Continue */
				case -101: /* Switching Protocols */
				case -300: /* Multiple Choices */
				case -301: /* Moved Permanently */
				case -302: /* Moved Temporarily */
				case -303: /* See Other */
				case -304: /* Not Modified */
				case -305: /* Use Proxy */ 
				case -402: /* Payment Required */
				case -405: /* Method Not Allowed */
				case -406: /* Not Acceptable */
				case -407: /* Proxy Autentication Required */
				case -408: /* Request Timeout */
				case -409: /* Conflict */
				case -410: /* Gone */
				case -412: /* Precondition Failed */
				case -414: /* Request-URI Too Long */
				case -415: /* Unsupported Media Type */
				case -500: /* Internal Server Error */
				case -501: /* Not Implemented */
				case -502: /* Bad Gateway */
				case -503: /* Service Unavailable */
				case -504: /* Gateway Timeout */
					Logger.writeToLog ("http_error_to_errno: HTTP error " + err,true,false,"HTTPStructure");
					return 1;
				default:
					Logger.writeToLog ("http_error_to_errno: unknown error " + err,true,false,"HTTPStructure");
					return 1;
			}
		}

		public static Http_method
			http_string_to_method (string method, int n)
		{
			if (method.Substring(0,n) == "GET")
				return Http_method.HTTP_GET;
			if (method.Substring(0,n) == "PUT")
				return Http_method.HTTP_PUT;
			if (method.Substring(0,n) == "POST")
				return Http_method.HTTP_POST;
			if (method.Substring(0,n) == "OPTIONS")
				return Http_method.HTTP_OPTIONS;
			if (method.Substring(0,n) == "HEAD")
				return Http_method.HTTP_HEAD;
			if (method.Substring(0,n) == "DELETE")
				return Http_method.HTTP_DELETE;
			if (method.Substring(0,n) == "TRACE")
				return Http_method.HTTP_TRACE;

			return Http_method.HTTP_UNKNOW;
		}

		public static string
			http_method_to_string (Http_method method)
		{
			switch (method)
			{
				case Http_method.HTTP_GET: return "GET";
				case Http_method.HTTP_PUT: return "PUT";
				case Http_method.HTTP_POST: return "POST";
				case Http_method.HTTP_OPTIONS: return "OPTIONS";
				case Http_method.HTTP_HEAD: return "HEAD";
				case Http_method.HTTP_DELETE: return "DELETE";
				case Http_method.HTTP_TRACE: return "TRACE";
			}
			return "(uknown)";
		}

		public static Http_header http_alloc_header (string name, string value)
		{
			Http_header header = new Http_header();

			header.name = header.value = null;
			header.name = name;
			header.value = value;
			
			return header;
		}


		public static Http_header http_add_header (ref Http_header header, string name, string value)
		{
			if(header == null)
			{
				header = http_alloc_header (name, value);
				return header;
			}

			Http_header new_header = null;

			new_header = http_alloc_header (name, value);

			new_header.next = null;
			
			Http_header tempHeader = header;
			while (tempHeader.next != null)
				tempHeader = tempHeader.next;

			tempHeader.next = new_header;

			return new_header;
		}

		public static int read_until (Socket fd, int ch, ref byte[] data)
		{
			byte[] buf, buf2;
			int n;
			int len, buf_size;

			data = null;

			buf_size = 100;
			buf = new byte[buf_size];

			if(fd.Blocking)
				fd.Blocking = false;

			len = 0;
			n = 1;

			while (n == 1)
			{
				if(fd.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
				{
					if(fd.Available == 0)
					{
						n = 0;
						continue;
					}

					n = fd.Receive(buf,len, Tunnel.sizeByte,SocketFlags.None);

					if (buf[len] == ch)
						break;

					if (len + 1 == buf_size)
					{
						buf_size *= 2;
						buf2 = new byte[buf_size];
						buf.CopyTo(buf2,0);
						buf = buf2;
						buf2 = null;
					}

					len++;
				} 
				else
				{
					n = 0;
					continue;
				}
			}

			if (n == 0)
			{
				buf = null;
				Logger.writeToLog ("read_until: closed",true,false,"HTTPStructure");
				return n;
			}

			/* Shrink to minimum size + 1 in case someone wants to add a NUL. */
			buf2 = new byte[len + 1];

			for(int i = 0; i < (len + 1);i++)
				buf2[i] = buf[i];

			buf = buf2;
			buf2 = null;

			data = buf;
			buf = null;
			return data.Length;
		}

		public static int
			parse_header (Socket fd,ref Http_header header)
		{
			byte[] buf = new byte[2];
			byte[] data = null;
			Http_header h;
			int len;
			int n = 0;

			header = null;

			fd.Blocking = false;

			while(n < 2)
			{
				if(fd.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
				{
					if( fd.Available == 0)
					{
						n = 0;
						break;
					}
					n = fd.Receive(buf, n, (2-n),SocketFlags.None);
				} 
				else
				{
					n = 0;
					break;
				}
			}

			if (n == 0)
				return n;

			if ( (char)buf[0] == '\r' && (char)buf[1] == '\n')
				return n;

			h = new Http_header();

			header = h;
			h.name = null;
			h.value = null;

			n = read_until (fd, ':',ref data);

			if (n == 0)
				return n;

			byte[] buftemp = new byte[n + 2];
			data.CopyTo(buftemp, 2);
			buf.CopyTo(buftemp,0);
			n += 2;
			data = buftemp;
//			data[n - 1] = (byte)0;
			h.name = Encoding.ASCII.GetString(data);
			len = n;

			n = read_until (fd, '\r', ref data);
			if (n == 0)
				return n;

			data[n - 1] = 0;
			h.value = Encoding.ASCII.GetString(data);
			len += n;

			n = read_until (fd, '\n', ref data);
			if (n == 0)
				return n;
			
			data = null;
			if (n != 1)
			{
				Logger.writeToLog ("parse_header: invalid line ending",true,false,"HTTPStructure");
				return -1;
			}
			len += n;

			Logger.writeToLog ("parse_header: " + h.name + ":" + h.value,false,false,"HTTPStructure");

			n = parse_header (fd, ref h.next);
			if (n == 0)
				return n;
			len += n;

			return len;
		}

		public static int
			http_write_header (Socket fd, ref Http_header header)
		{
			int n = 0, m;

			if (header == null)
				return fd.Send(Encoding.ASCII.GetBytes("\r\n"));

			try 
			{
				m = fd.Send(Encoding.ASCII.GetBytes(header.name));
			} 
			catch
			{
				return -1;
			}
			n += m;

			try 
			{
			m = fd.Send(Encoding.ASCII.GetBytes(": "));
			} 
			catch
			{
				return -1;
			}
			n += m;

			try 
			{
			m = fd.Send(Encoding.ASCII.GetBytes(header.value));
			} 
			catch
			{
				return -1;
			}
			n += m;

			try 
			{
			m = fd.Send(Encoding.ASCII.GetBytes("\r\n"));
			} 
			catch
			{
				return -1;
			}
			n += m;

			try 
			{
			m = http_write_header (fd, ref header.next);
			} 
			catch
			{
				return -1;
			}
			n += m;

			return n;
		}

		public static void
			http_destroy_header (ref Http_header header)
		{
			if (header == null)
				return;

			http_destroy_header (ref header.next);

			if (header.name != null)
				header.name = null;
			if (header.value != null)
				header.value = null;

			header = null;
		}

		public static Http_response 
			http_allocate_response (string status_message)
		{
			Http_response response;

			response = new Http_response();

			response.status_message = status_message;

			return response;
		}

		public static Http_response 
			http_create_response (int major_version,int minor_version,int status_code,string status_message)
		{
			Http_response response;

			response = http_allocate_response (status_message);

			response.major_version = major_version;
			response.minor_version = minor_version;
			response.status_code = status_code;
			response.header = null;

			return response;
		}

		public static int
			http_parse_response (Socket fd, ref Http_response response_)
		{
			Http_response response;
			byte[] data = null;
			int len;
			int n;

			response_ = null;

			response = new Http_response();

			response.major_version = -1;
			response.minor_version = -1;
			response.status_code = -1;
			response.status_message = null;
			response.header = null;

			n = read_until (fd, '/',ref data);
			if (n == 0)
			{
				response = null;
				return n;
			}
			else if (n != 5 || !(Encoding.ASCII.GetString(data).Substring(0,4) == "HTTP"))
			{
				Logger.writeToLog("http_parse_response: expected HTTP",true,false,"HTTPStructure");
				data = null;
				response = null;
				return -1;
			}
			data = null;
			len = n;

			n = read_until (fd, '.', ref data);
			if (n == 0)
			{
				response = null;
				return n;
			}

			data[n - 1] = 0;
			response.major_version = Convert.ToInt32(Encoding.ASCII.GetString(data));
			Logger.writeToLog ("http_parse_response: major version = " + response.major_version,false,false,"HTTPStructure");
			data = null;
			len += n;

			n = read_until (fd, ' ', ref data);
			if (n == 0)
			{
				response = null;
				return n;
			}

			data[n - 1] = 0;
			response.minor_version = Convert.ToInt32(Encoding.ASCII.GetString(data));
			Logger.writeToLog ("http_parse_response: minor version = " + response.minor_version,false,false,"HTTPStructure");
			data = null;
			len += n;

			n = read_until (fd, ' ', ref data);
			if (n == 0)
			{
				response = null;
				return n;
			}

			data[n - 1] = 0;
			response.status_code = Convert.ToInt32(Encoding.ASCII.GetString(data));
			Logger.writeToLog ("http_parse_response: status code = " + response.status_code,false,false,"HTTPStructure");
			data = null;
			len += n;

			n = read_until (fd, '\r', ref data);
			if (n <= 0)
			{
				response = null;
				return n;
			}

			data[n - 1] = 0;
			response.status_message = Encoding.ASCII.GetString(data);
			Logger.writeToLog ("http_parse_response: status message = " + response.status_message,false,false,"HTTPStructure");
			len += n;

			n = read_until (fd, '\n', ref data);
			if (n == 0)
			{
				http_destroy_response (ref response);
				return n;
			}

			data = null;
			if (n != 1)
			{
				Logger.writeToLog ("http_parse_request: invalid line ending",true,false,"HTTPStructure");
				http_destroy_response (ref response);
				return -1;
			}
			len += n;

			n = parse_header (fd, ref response.header);
			if (n <= 0)
			{
				http_destroy_response (ref response);
				return n;
			}
			len += n;

			response_ = response;
			return len;
		}

		public static void http_destroy_response (ref Http_response response)
		{
			if (response.status_message != null)
				response.status_message = null;

			http_destroy_header (ref response.header);
			response = null;
		}

		public static Http_request 
			http_allocate_request (string uri)
		{
			Http_request request;

			request = new Http_request();

			request.uri = uri;

			return request;
		}

		public static Http_request 
			http_create_request (ref Http_method method,string uri,int major_version,int minor_version)
		{
			Http_request request;

			request = http_allocate_request (uri);
			if (request == null)
				return null;

			request.method = method;
			request.major_version = major_version;
			request.minor_version = minor_version;
			request.header = null;

			return request;
		}

		public static int
			http_write_request (Socket fd, ref Http_request request)
		{
			string str = "";
			int n = 0;
			int m;
  
			str = http_method_to_string (request.method) + " " + 
				request.uri + " " + "HTTP/"+ request.major_version + "." +
				request.minor_version + "\r\n";

			m = str.Length;

			try
			{
				m = fd.Send(Encoding.ASCII.GetBytes(str));
				Logger.writeToLog ("http_write_request: " + str,false,false,"HTTPStructure");
			} 
			catch 
			{
				return -1;
			}

			n += m;

			m = http_write_header (fd, ref request.header);
			if (m == -1)
			{
				return -1;
			}
			n += m;

			return n;
		}

		public static void
			http_destroy_request (ref Http_request request)
		{
			if (request.uri != null)
				request.uri = null;
			http_destroy_header (ref request.header);
			request = null;
		}

		public static Http_header 
			http_header_find (ref Http_header header, string name)
		{
			if (header == null)
				return null;

			if (header.name == name)
				return header;

			return http_header_find (ref header.next, name);
		}

			static string
			http_header_get (ref Http_header header, string name)
		{
			Http_header h;

			h = http_header_find (ref header, name);
			if (h == null)
				return null;

			return h.value;
		}

		public static void http_header_set (ref Http_header header, string name, string value)
		{
			Http_header h;
			string v;

			v = value;
			h = http_header_find (ref header, name);
			if (h == null)
			{
				Http_header h2;

				h2 = new Http_header();
				
				h2.name = name;

				h2.value = v;

				h2.next = header;

				header = h2;

				return;
			}
			else
			{
				h.value = null;
				h.value = v;
			}
		}


		public static int
			http_parse_request (Socket fd, ref Http_request request_)
		{
			Http_request request;
			byte[] data = null;
			int len;
			int n;

			request_ = null;

			request = new Http_request();
			request.method = Http_method.HTTP_UNKNOW;
			request.uri = null;
			request.major_version = -1;
			request.minor_version = -1;
			request.header = null;

			n = read_until (fd, ' ', ref data);
			if (n == 0)
			{
				request = null;
				return n;
			}

			string currMethod = Encoding.ASCII.GetString(data).TrimEnd();
			request.method = http_string_to_method (currMethod, currMethod.Length);
			
			if (request.method == Http_method.HTTP_UNKNOW)
			{
				Logger.writeToLog ("http_parse_request: expected an HTTP method",true,false,"HTTPStructure");
				data = null;
				request = null;;
				return -1;
			}

			data[n - 1] = 0;
			Logger.writeToLog ("http_parse_request: method = " 
				+ Encoding.ASCII.GetString(data),false,false,"HTTPStructure");
			data = null;;
			len = n;

			n = read_until (fd, ' ', ref data);
			if (n == 0)
			{
				request = null;
				return n;
			}

			data[n - 1] = 0;
			request.uri = Encoding.ASCII.GetString(data);
			len += n;
			Logger.writeToLog("http_parse_request: uri = " +  request.uri,false,false,"HTTPStructure");

			n = read_until (fd, '/', ref data);
			if (n == 0)
			{
				http_destroy_request (ref request);
				return n;
			}
			else if (n != 5 || !(Encoding.ASCII.GetString(data).Substring(0,4) == "HTTP"))
			{
				Logger.writeToLog ("http_parse_request: expected HTTP",true,false,"HTTPStructure");
				data = null;
				http_destroy_request (ref request);
				return -1;
			}
			data = null;
			len = n;

			n = read_until (fd, '.', ref data);
			if (n == 0)
			{
				http_destroy_request (ref request);
				return n;
			}

			data[n - 1] = 0;
			request.major_version = Convert.ToInt32(Encoding.ASCII.GetString(data)) ;
			Logger.writeToLog ("http_parse_request: major version = " +
				request.major_version,false,false,"HTTPStructure");
			data = null;
			len += n;

			n = read_until (fd, '\r', ref data);
			if (n <= 0)
			{
				http_destroy_request (ref request);
				return n;
			}

			data[n - 1] = 0;
			request.minor_version = Convert.ToInt32(Encoding.ASCII.GetString(data));
			Logger.writeToLog ("http_parse_request: minor version = " +
				request.minor_version,false,false,"HTTPStructure");
			data = null;
			len += n;

			n = read_until (fd, '\n', ref data);
			if (n <= 0)
			{
				http_destroy_request (ref request);
				return n;
			}
			data = null;

			if (n != 1)
			{
				Logger.writeToLog ("http_parse_request: invalid line ending",true,false,"HTTPStructure");
				http_destroy_request (ref request);
				return -1;
			}
			len += n;

			n = parse_header (fd, ref request.header);
			if (n <= 0)
			{
				http_destroy_request (ref request);
				return n;
			}
			len += n;

			request_ = request;
			return len;
		}
	}

	public class Http_header
	{
		public string name = "";
		public string value = "";
		public Http_header next; 
	}

	public class Http_request
	{
		public HTTPStructures.Http_method method;
		public string uri = "";
		public int major_version;
		public int minor_version;
		public Http_header header = new Http_header();
	} 

	public class Http_response
	{
		public int major_version;
		public int minor_version;
		public int status_code;
		public string status_message = "";
		public Http_header header = new Http_header();
	} 

	public class Http_destination
	{
		public string host_name = "";
		public int host_port;
		public string proxy_name = "";
		public int proxy_port;
		public string basic_proxy_authorization = "";
		public string username = "";
		public string pass = "";
		public string user_agent = "";
		public HTTPStructures.ProxyAuthMethod AuthMeth = HTTPStructures.ProxyAuthMethod.BASIC_AUTENTICATION ; 
	} 

}
