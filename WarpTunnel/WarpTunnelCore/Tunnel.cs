using System;
using System.Xml;
using System.Data;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using ConnectionStatistics;
using WarpTunnelUtils;
using WarpTunnelAuth;

namespace WarpTunnel
{
	/// <summary>
	/// Summary description for Tunnel.
	/// </summary>
	/// 

	[Serializable]
	public class Tunnel_Auth_Dest
	{
		public string host = null;
		public int port = 0;
	}

	[Serializable]
	public class Tunnel_Client_AuthData
	{
		public string username;
		public string base64digPassword;
		public string destHost;
		public int destPort = 0;
	}

	public class Tunnel
	{
		public enum Thread_Mode
		{
			HeavyThreading,
			SoftThreading,
		}

		private const int TUNNEL_CLOSE_REQ_READ = -2;
		private const int MAX_TIMEOUT_CLOSE_WAIT = 2 * DEFAULT_KEEP_ALIVE; //SECONDS
		private const int MAX_SEND_BYTES = 65024; //BYTES
		private static byte[] padding = new byte[MAX_SEND_BYTES];
		private const byte TUNNEL_AUTH_NO_REQUIRED = 0x42;
		private const byte TUNNEL_AUTH_REQUIRED = 0x43;
		private const byte TUNNEL_SIMPLE = 0x40;
		private const byte TUNNEL_OPEN = 0x01;
		private const byte TUNNEL_DATA = 0x02;
		private const byte TUNNEL_PADDING = 0x03;
		private const byte TUNNEL_ERROR = 0x04;
		private const byte TUNNEL_PAD1 = TUNNEL_SIMPLE | 0x05;
		private const byte TUNNEL_CLOSE = TUNNEL_SIMPLE | 0x06;
		private const byte TUNNEL_DISCONNECT = TUNNEL_SIMPLE | 0x07;
		private const byte TUNNEL_READY_DISCONNECTION = TUNNEL_SIMPLE | 0x08; 

		private const int MAX_FAILS_CONECTION = 10;

		public const int DEFAULT_CONNECTION_MAX_TIME = 300;
		public const int READ_TRAIL_TIMEOUT = 1000; /* milliseconds */
		public const int ACCEPT_TIMEOUT = 10; /* seconds */

		public const int DEFAULT_HOST_PORT = 8888;
		public const int DEFAULT_BUFFER_LENGTH = (100 * 1024); /* bytes */
		public const int DEFAULT_KEEP_ALIVE = 1; /* seconds */
		public const int DEFAULT_MAX_CONNECTION_AGE = 600; /* seconds */
		public const int MAX_COMPRESSION_OVERHEAD_SIZE = 5; // bytes
		public const int MAX_COPRESS_DATA = 65000; //bytes
		public const int TUNNEL_IN = 1;
		public const int TUNNEL_OUT = 2;
		public const int SOCKET_BUFFER_SIZE = (8 * 1024); //BYTES
		public const int DIM_READWRITE_BUFFER_SIZE = (5 * 1024); //bytes
		public const int MAX_TUNNEL_WRITE_TIMEOUT = 60000000; // 60 SECONDS
		public const int MAX_TIMEOUT_READ_WAIT = 30000000; //30 seconds

		public const int MAX_TIMEOUT_CLIENT_ON_SERVER = 86400000; //24 HOURS

		public const int RSA_KEY_SIZE = 1024;
		public const int RJ_KEY_SIZE = 128;
		public const int CRYPT_OVERHEAD = 16;

		public const int sizeShort = sizeof(ushort);
		public const int sizeByte = sizeof(byte);
		public const int sizeInt = sizeof(int);

		//private bool ifServerSocketChanged = false;
		private bool ifTunnelCloseRequestReceived = false;
		private Guid Guid_client_ID;

		public bool isAuthenticationRequired;
		public string myName;
		public Socket in_fd = null, out_fd = null;
		public Http_destination dest;
		public Socket server_socket;
		public IPEndPoint address;
		public IPEndPoint server_address;
		//public IPEndPoint reconnect_address;
		public int bytes;
		public int content_length;
		public bool padding_only;
		public int in_total_raw;
		public int in_total_data;
		public int out_total_raw;
		public int out_total_data;
		public DateTime out_connect_time;
		public bool strict_content_length;
		public int keep_alive;
		public int max_connection_age;
		public int errno;
		public int listenPort;
		public bool compatible_mode;
		public const int sizeof_header = sizeof (byte) + sizeof (ushort);

		public bool ifUseAsimmetricDataEncription = false;
		public SymmetricCryptDecrypt crypt_decrypt = new SymmetricCryptDecrypt();

		private bool isServer = false;
		private WarpTunnelAuthenticator curr_Auth_Info;
		private string server_sourceIP = null;
		public ConnectionDetails ConnStatRow = null;

		protected Tunnel()
		{
		}

		private static string REQ_TO_STRING (byte request)
		{
			switch (request)
			{
				case TUNNEL_OPEN:		return "TUNNEL_OPEN";
				case TUNNEL_DATA:		return "TUNNEL_DATA";
				case TUNNEL_PADDING:	return "TUNNEL_PADDING";
				case TUNNEL_ERROR:		return "TUNNEL_ERROR";
				case TUNNEL_PAD1:		return "TUNNEL_PAD1";
				case TUNNEL_CLOSE:		return "TUNNEL_CLOSE";
				case TUNNEL_DISCONNECT:	return "TUNNEL_DISCONNECT";
				default:								return "(unknown)";
			}
		}

		private static int min(int a , int b)
		{ 
			return ((a) < (b) ? (a) : (b));
		}


		public bool tunnel_is_disconnected ()
					  {
						  if(this.out_fd == null)
							  return true;

						  return !this.out_fd.Connected;
					  }

		public bool tunnel_is_connected ()
					  {
						  return !this.tunnel_is_disconnected();
					  }

		public bool tunnel_is_server ()
					{
						if(this.isServer)
							return true;
						else
							return false;
					}

		public bool tunnel_is_client ()
					  {
						  return !this.tunnel_is_server ();
					  }

		public void
			tunnel_out_disconnect ()
		{
			if (this.tunnel_is_disconnected())
				return;

			if (this.tunnel_is_client() &&
				this.bytes != this.content_length + 1)
			{
				Logger.writeToLog("tunnel_out_disconnect: warning: bytes=" 
					+ this.bytes + "!= content_length=" 
					+ (this.content_length + 1),true,this.tunnel_is_server(),this.myName);
			}

			if(this.out_fd != null)
			{
//				if(this.out_fd.Available > 0)
//				{
//					this.out_fd.Blocking = true;
//					byte[] tempBuf = new byte[this.out_fd.Available];
//					this.out_fd.Receive(tempBuf,0,tempBuf.Length,SocketFlags.None);
//				}

				this.out_fd.Shutdown(SocketShutdown.Both);
				this.out_fd.Close();
			}
			this.out_fd = null;
			this.bytes = 0;

			Logger.writeToLog ("tunnel_out_disconnect: output disconnected",false,this.tunnel_is_server(),this.myName);
		}

		public void tunnel_in_disconnect ()
		{
			if (this.in_fd == null)
				return;

//			if(this.in_fd.Available > 0)
//			{
//				byte[] tempBuf = new byte[this.in_fd.Available];
//				this.in_fd.Blocking = true;
//				this.in_fd.Receive(tempBuf,0,tempBuf.Length,SocketFlags.None);
//			}

			this.in_fd.Shutdown(SocketShutdown.Both);
			this.in_fd.Close();
			this.in_fd = null;

			Logger.writeToLog ("tunnel_in_disconnect: input disconnected",false,this.tunnel_is_server(),this.myName);
		}

		public int tunnel_out_connect ()
		{
			int n = 0;
			int fails;

			for(fails = 0; fails < MAX_FAILS_CONECTION ; fails++)
			{
				if(this.tunnel_is_client())
					ConnStatRequests.AddConnectionData(this.ConnStatRow,0,0,0,0,true);

				if (this.tunnel_is_connected () && fails == 0)
				{
					Logger.writeToLog ("tunnel_out_connect: already connected",false,this.tunnel_is_server(),this.myName);
					this.tunnel_out_disconnect();
				}
				else if(fails != 0)
				{
					if(this.out_fd != null)
					{
						this.out_fd.Close();
						this.out_fd = null;
					}
				}

				try 
				{
					this.out_fd = new Socket(this.address.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);

					if(!this.out_fd.Blocking) 
						this.out_fd.Blocking = !this.out_fd.Blocking;

					this.out_fd.Connect(this.address);

					if (this.out_fd == null || !this.out_fd.Connected)
					{
						if(fails == (MAX_FAILS_CONECTION - 1))
						{
							Logger.writeToLog ("tunnel_out_connect: connect error !",true,this.tunnel_is_server(),this.myName);
							return -1;
						} 
						else
						{
							continue;
						}
					}
				} 
				catch 
				{
					if(fails == (MAX_FAILS_CONECTION - 1))
					{
						return -1;
					} 
					else
					{
						continue;
					}					
				}

				this.tunnel_out_setsockopts (this.out_fd);

				/* + 1 to allow for TUNNEL_DISCONNECT */
				n = HTTPStructures.http_post(ref this.out_fd,
					ref this.dest,
					(this.content_length + 1),this.Guid_client_ID);
				if (n == -1 || !this.out_fd.Connected)
				{
					if(fails == (MAX_FAILS_CONECTION - 1))
						return -1;
				} 
				else
				{
					break;
				}
			}

			this.out_total_raw += n;
			Logger.writeToLog ("tunnel_out_connect: out_total_raw = " 
				+ this.out_total_raw,false,this.tunnel_is_server(),this.myName);


			this.bytes = 0;
			this.padding_only = true;
			this.out_connect_time = DateTime.Now.ToUniversalTime();

			Logger.writeToLog ("tunnel_out_connect: output connected",false,this.tunnel_is_server(),this.myName);

			return 0;
		}

		public int tunnel_in_connect ()
		{
			Http_response response = null;
			int n;
			int fails;

			for(fails = 0; fails < MAX_FAILS_CONECTION ; fails++)
			{
				if(this.tunnel_is_client())
					ConnStatRequests.AddConnectionData(this.ConnStatRow,0,0,0,0,true);

				Logger.writeToLog ("tunnel_in_connect()",false,this.tunnel_is_server(),this.myName);

				if (this.in_fd != null && this.in_fd.Connected && fails == 0)
				{
					Logger.writeToLog ("tunnel_in_connect: already connected",true,this.tunnel_is_server(),this.myName);
					return -1;
				} 
				else if(fails != 0)
				{
					if(this.in_fd != null)
					{
						this.in_fd.Close();
						this.in_fd = null;
					}
				}

				this.in_fd = new Socket(this.address.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);

				if(!this.in_fd.Blocking) 
					this.in_fd.Blocking = !this.in_fd.Blocking;

				this.in_fd.Connect(this.address);

				if (this.in_fd == null || !this.in_fd.Connected)
				{
					if(fails == (MAX_FAILS_CONECTION - 1))
					{
						Logger.writeToLog ("tunnel_in_connect: do_connect_retry() error !",true,this.tunnel_is_server(),this.myName);
						return -1;
					} 
					else
					{
						continue;
					}	
				}

				this.tunnel_in_setsockopts (this.in_fd);

				if (HTTPStructures.http_get (ref this.in_fd, ref this.dest,this.Guid_client_ID) == -1)
				{
					if(fails == (MAX_FAILS_CONECTION - 1))
					{
						return -1;
					} 
					else
					{
						continue;
					}	
				}

				n = HTTPStructures.http_parse_response (this.in_fd, ref response);
			
				if (n <= 0)
				{
					if (n == 0)
						Logger.writeToLog ("tunnel_in_connect: no response; peer closed connection",true,this.tunnel_is_server(),this.myName);
					else
						Logger.writeToLog ("tunnel_in_connect: no response; error !",true,this.tunnel_is_server(),this.myName);
				}
				else if (response.major_version != 1 ||
					(response.minor_version != 1 &&
					response.minor_version != 0))
				{
					Logger.writeToLog ("tunnel_in_connect: unknown HTTP version: " + response.major_version + "." + response.minor_version,true,this.tunnel_is_server(),this.myName);
					n = -1;
				}
				else if (response.status_code != 200)
				{
					Logger.writeToLog ("tunnel_in_connect: HTTP error " + response.status_code,true,this.tunnel_is_server(),this.myName);
					errno = HTTPStructures.http_error_to_errno (-response.status_code);
					n = -1;
				}

				if (response != null)
					HTTPStructures.http_destroy_response (ref response);
			

				if (n > 0)
				{

					this.in_total_raw += n;
					Logger.writeToLog ("tunnel_in_connect: in_total_raw = " + this.in_total_raw,false,this.tunnel_is_server(),this.myName);
					break;
				}
				else
				{
					if(fails == (MAX_FAILS_CONECTION - 1))
						return n;
				}
			}

			Logger.writeToLog ("tunnel_in_connect: input connected",false,this.tunnel_is_server(),this.myName);

			return 1;
		}	

		public int  tunnel_write_data ( byte[] data, int length)
		{
			return this.tunnel_write_data(data,0,length);
		}

		public int  tunnel_write_data ( byte[] data,int offset, int length)
		{

			try
			{
				int startOffset = offset;
				int n = 0;
				this.out_fd.Blocking = false;
				while (n < length)
				{
					if(!out_fd.Poll(Tunnel.MAX_TUNNEL_WRITE_TIMEOUT,SelectMode.SelectWrite))
						return -1;

					n += this.out_fd.Send(data,startOffset,length-n,SocketFlags.None);
					startOffset += n;

					if(!out_fd.Connected)
						return -1;
				}
			} 
			catch (Exception ex)
			{
				Logger.writeToLog ("tunnel_write_data: write error: " + ex.Message,true,this.tunnel_is_server(),this.myName);
				return -1;
			}

			this.out_fd.Blocking = false;

			this.bytes += length;
			return length;
		}

		public int tunnel_write_request (byte request,
			byte[] data, ushort length)
		{
			return this.tunnel_write_request(request,data,0,length);
		}

		public int tunnel_write_request (byte request,
			byte[] data,int offset, ushort length)
		{
			if ( this.bytes + sizeByte +
				(data != null ? sizeShort + (int)length : 0) > (this.content_length - 1))
			{
				tunnel_padding ((this.content_length - this.bytes) - 1 );

				Logger.writeToLog ("tunnel_write_request:  this.bytes + sizeByte + (data != null ? sizeShort + (int)length : 0) > this.content_length",false,this.tunnel_is_server(),this.myName);
			
				byte c = TUNNEL_DISCONNECT;
				tunnel_write_data (new byte[] {c}, sizeByte);

				this.tunnel_out_disconnect();

				if (this.tunnel_is_client())
				{
					if (this.tunnel_out_connect() == -1)
						return -1;
				}
				else
				{
					if (this.tunnel_accept(true) == -1)
						return -1;
				}
			}


			DateTime t;

			t = DateTime.Now.ToUniversalTime();

			if (tunnel_is_client() &&
				tunnel_is_connected() &&
				((TimeSpan)t.Subtract(this.out_connect_time)).Seconds > this.max_connection_age)
			{
				byte c = TUNNEL_DISCONNECT;

				Logger.writeToLog ("tunnel_write_request: connection > " + this.max_connection_age + " seconds old",false,this.tunnel_is_server(),this.myName);

				if (this.strict_content_length)
				{
					int l = this.content_length - this.bytes - 1;

					Logger.writeToLog ("tunnel_write_request: write padding (" + (this.content_length - this.bytes - 1) + " bytes)",false,this.tunnel_is_server(),this.myName);

					if (l > 3)
					{
						ushort s;
						int i;

						c = TUNNEL_PADDING;
						this.tunnel_write_data (new byte[] {c}, sizeByte);

						s = (ushort)(l-2); 
						this.tunnel_write_data (htons(BitConverter.GetBytes(s)), sizeShort);

						l -= 2;
						c = 0;
						for (i=0; i<l; i++)
							this.tunnel_write_data (new byte[] {c}, sizeByte);
					}
					else
					{
						c = TUNNEL_PAD1;
						int i;
		
						for (i=0; i<l; i++)
							this.tunnel_write_data (new byte[] {c}, sizeByte);
					}
				}

				Logger.writeToLog ("tunnel_write_request: closing old connection",false,this.tunnel_is_server(),this.myName);
				
				if(c != TUNNEL_DISCONNECT)
					c = TUNNEL_DISCONNECT;
				
				if (this.tunnel_write_data ( new byte[] {c}, sizeByte) <= 0)
					return -1;

				this.tunnel_out_disconnect();

				if (this.tunnel_is_client())
				{
					if (this.tunnel_out_connect() == -1)
						return -1;
				}
				else
				{
					if (this.tunnel_accept(true) == -1)
						return -1;
				}
			}

			if (this.tunnel_is_disconnected())
			{
				if (this.tunnel_is_client())
				{
					if (this.tunnel_out_connect() == -1)
						return -1;
				}
				else
				{
					if (this.tunnel_accept(true) == -1)
						return -1;
				}
			}

			if (request !=  TUNNEL_PADDING && request != TUNNEL_PAD1)
				this.padding_only = false;

			if (this.tunnel_write_data (new byte[] {request}, sizeByte) == -1)
			{
				this.tunnel_out_disconnect();
				if (this.tunnel_is_client())
					this.tunnel_out_connect();
				else
				{
					Logger.writeToLog ("tunnel_write_request: couldn't write request: output is disconnected",true,this.tunnel_is_server(),this.myName);
					errno = 1;
					return -1;
				}
				/* return tunnel_write_request (tunnel, request, data, length); */
				if (this.tunnel_write_data (new byte[] {request}, sizeByte) == -1)
					return -1;
			}

			if (data != null)
			{

				ushort network_length = length;
				if (this.tunnel_write_data (htons(BitConverter.GetBytes(length)),sizeShort) == -1)
					return -1;

				if (this.tunnel_write_data (data,offset, length) == -1)
					return -1;

				this.out_total_raw += 3 + length;

				if (request == TUNNEL_DATA)
					Logger.writeToLog ("tunnel_write_request: " + REQ_TO_STRING(request) + " (" + length + ")",false,this.tunnel_is_server(),this.myName);
				else
					Logger.writeToLog ("tunnel_write_request: " + REQ_TO_STRING(request) + " (" + length + ")",false,this.tunnel_is_server(),this.myName);
			}
			else
			{
				this.out_total_raw += 1;
				Logger.writeToLog ("tunnel_write_request: " +  REQ_TO_STRING(request),false,this.tunnel_is_server(),this.myName);
			}

			Logger.writeToLog("tunnel_write_data: out_total_raw = " + this.out_total_raw,false,this.tunnel_is_server(),this.myName);

			return 0;
		}

		private static byte[] ntons(byte[] toConvert)
		{
			byte[] newByte = new byte[toConvert.Length];
			int maxIndex = toConvert.Length - 1;

			for(int i = 0;i < toConvert.Length ;i++)
				newByte[maxIndex - i] = toConvert[i];

			return newByte;
		}

		private static byte[] htons(byte[] toConvert)
		{
			byte[] newByte = new byte[toConvert.Length];
			int maxIndex = toConvert.Length - 1;

			for(int i = 0;i < toConvert.Length ;i++)
				newByte[i] = toConvert[maxIndex - i];

			return newByte;
		}

		public int
			tunnel_connect (bool ifTunnelCompressData,bool stealthModeEnable,bool ifUDPTraffic,Tunnel_Client_AuthData clientAuth)
		{
			byte auth_data =  TUNNEL_AUTH_NO_REQUIRED ; /* SET IF WARPTUNNEL AUTHENTICATION IS REQUIRED OR NOT */
			
			if(this.isAuthenticationRequired  && !this.compatible_mode)
			{
				auth_data = TUNNEL_AUTH_REQUIRED;
				this.ConnStatRow = ConnStatRequests.AddConnectionEntry(this.dest.host_name,this.dest.host_port,clientAuth.destHost,clientAuth.destPort);
			}
			else
				this.ConnStatRow = ConnStatRequests.AddConnectionEntry(this.dest.host_name,this.dest.host_port,"set at server side",0);



			Logger.writeToLog ("tunnel_connect()",false,this.tunnel_is_server(),this.myName);

			if (this.tunnel_is_connected())
			{
				Logger.writeToLog ("tunnel_connect: already connected",true,this.tunnel_is_server(),this.myName);
				return -1;
			}

			if (this.tunnel_write_request (TUNNEL_OPEN,
				new byte[] {auth_data}, sizeByte) == -1)
				return -1;

			if (this.tunnel_in_connect() <= 0)
				return -1;

			if(!this.compatible_mode)
			{
				if(this.Tunnel_connect_Init(ifTunnelCompressData,stealthModeEnable,ifUDPTraffic) == -1)
					return -1;
			}

			if(this.isAuthenticationRequired && !this.compatible_mode)
			{
				if(Authenticate_To_Server(clientAuth) == -1)
					return -1;
			}


			if(!this.compatible_mode)
			{
				if(this.Negotiate_RSA_Key_ClientSide(this.ifUseAsimmetricDataEncription) == -1)
					return -1;
			}

			return 0;
		}

		private int Authenticate_To_Server(Tunnel_Client_AuthData clientAuth)
		{
			int readVar = 0;

			try
			{
				//handshake len
				byte[] handshakeLen = new byte[Tunnel.sizeInt];
				while(readVar  <= 0)
				{
					readVar = this.tunnel_read(ref handshakeLen,Tunnel.sizeInt);

					if(readVar == -1)
						return -1;
				}

				//get handshakestring
				byte[] handshake = new byte[BitConverter.ToInt32(handshakeLen,0)];
				readVar = 0;
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref handshake,handshake.Length);

					if(readVar == -1)
						return -1;
				}

				string auth = WarpTunnelAuthenticator.getAuthString( clientAuth.username ,clientAuth.base64digPassword,Encoding.ASCII.GetString(handshake),clientAuth.destHost ,clientAuth.destPort);

				if(this.tunnel_write(BitConverter.GetBytes(auth.Length),Tunnel.sizeInt) == -1)
					return -1;

				if(this.tunnel_write(Encoding.ASCII.GetBytes(auth),auth.Length) == -1)
					return -1;

				return 1;
			} 
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				return -1;
			}
		}

		private int Tunnel_connect_Init(bool ifTunnelCompressData,bool stealthModeEnable,bool ifUDPTraffic)
		{
			//trasmit new port server data
			int readVar = 0;

			byte[] new_port_byte = new byte[Tunnel.sizeInt];
			while(readVar <= 0)
			{
				readVar = this.tunnel_read(ref new_port_byte,Tunnel.sizeInt);

				if(readVar == -1)
					return -1;
			}

			//this.dest.host_port = BitConverter.ToInt32(new_port_byte,0); // not more used sice version 1.4.7.0

			//trasmit if data into tunnel will be compressed
			if(ifTunnelCompressData && !stealthModeEnable)
			{
				if(this.tunnel_write(new byte[] {(byte)1},Tunnel.sizeByte) == -1)
					return -1;
			}
			else if(!ifTunnelCompressData && stealthModeEnable)
			{
				if(this.tunnel_write(new byte[] {(byte)2},Tunnel.sizeByte) == -1)
					return -1;
			}
			else
			{
				if(this.tunnel_write(new byte[] {(byte)0},Tunnel.sizeByte) == -1)
					return -1;
			}

			if(ifUDPTraffic)
			{
				if(this.tunnel_write(new byte[] {(byte)1},Tunnel.sizeByte) == -1)
					return -1;
			}
			else
			{
				if(this.tunnel_write(new byte[] {(byte)0},Tunnel.sizeByte) == -1)
					return -1;
			}

			create_address(this);
			return 0;

		}


		public int tunnel_write_or_padding (byte request, byte[] data,int length)
		{
			int n = 0, remaining;
			int nextOffset = 0;

			
			for (remaining = length; remaining > 0; remaining -= n)
			{
				if (this.bytes + remaining > this.content_length - sizeof_header &&
					this.content_length - this.bytes > sizeof_header)
					n = this.content_length - sizeof_header - this.bytes;
				else if (remaining > this.content_length - sizeof_header)
					n = this.content_length - sizeof_header;
				else
					n = remaining;

				if (n > MAX_SEND_BYTES)
					n = MAX_SEND_BYTES;

				if (request == TUNNEL_PADDING)
				{
					if (n + sizeof_header > remaining)
						n = remaining - sizeof_header;

					if (this.tunnel_write_request (request, padding, (ushort)n) == -1)
						return -1;

					n += sizeof_header;
				}
				else
				{
					if(this.ifUseAsimmetricDataEncription)
						n = length;

					if (this.tunnel_write_request (request, data,nextOffset, (ushort)n) == -1)
						return -1;

					nextOffset += n;
				}
			}

			return length - remaining;
		}

		public int tunnel_write_and_crypt ( byte[] data, int length)
		{
			SymmetricCryptDecrypt.SymmetricCrypt(this.crypt_decrypt,ref data, ref length);

			if(length < 0)
			{
				return - 1;
			}

			return tunnel_write ( data,  length);
		}

		public int tunnel_write ( byte[] data, int length)
		{
			int n;


			n = tunnel_write_or_padding (TUNNEL_DATA, data, length);
			this.out_total_data += length;
			Logger.writeToLog ("tunnel_write: out_total_data = " + this.out_total_data,false,this.tunnel_is_server(),this.myName);
			return n;
		}

		public int tunnel_padding (int length)
		{
			if (length < sizeof_header + 1)
			{
				int i;

				for (i = 0; i < length; i++)
				{
					if(tunnel_write_request (TUNNEL_PAD1, null, 0) == -1)
						return -1;
				}
				return length;
			}

			return tunnel_write_or_padding (TUNNEL_PADDING, null, length);
		}

		public void tunnel_close ()
		{
			
			int n = 0;

			if (this.strict_content_length)
			{
				Logger.writeToLog ("tunnel_close: write padding (" + (this.content_length - this.bytes - 1) + " bytes)",false,this.tunnel_is_server(),this.myName);
				this.tunnel_padding (this.content_length - this.bytes - 1);
			}

			Logger.writeToLog ("tunnel_close: write TUNNEL_CLOSE request",false,this.tunnel_is_server(),this.myName);

			if(!this.tunnel_is_disconnected())
			{
				//Wait for close response (let all buffered data to go away)
				if(this.tunnel_write_request (TUNNEL_CLOSE, null, 0) != -1 && !this.ifTunnelCloseRequestReceived)
				{
					DateTime start_wait = DateTime.Now;

					while (n != Tunnel.TUNNEL_CLOSE_REQ_READ)
					{
						n = this.tunnel_read(ref Tunnel.padding,Tunnel.MAX_SEND_BYTES);

						if(n >= 0)
							start_wait = DateTime.Now;

						if(((TimeSpan)DateTime.Now.Subtract(start_wait)).Seconds >= Tunnel.MAX_TIMEOUT_CLOSE_WAIT)
							break;
					}
				}
			}

			this.tunnel_out_disconnect();

			Logger.writeToLog ("tunnel_close: reading trailing data from input ...",false,this.tunnel_is_server(),this.myName);

			try 
			{
				while (this.in_fd.Poll(READ_TRAIL_TIMEOUT,SelectMode.SelectRead))
				{
					byte[] buf = new byte[this.in_fd.Available];
					this.in_fd.Blocking = true;
					n = this.in_fd.Receive(buf,0,buf.Length,SocketFlags.None);

					if (n > 0)
					{
						Logger.writeToLog ("read (" + n + " Bytes)",false,this.tunnel_is_server(),this.myName);
						continue;
					}
					else if (n == -1)
						Logger.writeToLog  ("tunnel_close: ... error !",true,this.tunnel_is_server(),this.myName);
					else 
					{
						Logger.writeToLog  ("tunnel_close: ... done (tunnel closed)",false,this.tunnel_is_server(),this.myName);
						break;
					}
				}
			} 
			catch{}

			this.tunnel_in_disconnect();

			if(!this.isServer)
				this.destroy_server_socket();

			this.in_total_raw = 0;
			this.in_total_data = 0;
			this.out_total_raw = 0;
			this.out_total_data = 0;

			return;
		}

		public void tunnel_setsockopts(Socket fd)
		{
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.Linger,new LingerOption (true, 0));
			fd.SetSocketOption(SocketOptionLevel.Tcp,SocketOptionName.NoDelay,1);
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.ReceiveBuffer,Tunnel.SOCKET_BUFFER_SIZE);
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.SendBuffer,Tunnel.SOCKET_BUFFER_SIZE);
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.KeepAlive,1);
		}

		private void tunnel_in_setsockopts (Socket fd)
		{
			tunnel_setsockopts(fd);
		}

		private void tunnel_out_setsockopts (Socket fd)
		{
			tunnel_setsockopts(fd);
		}

		public int tunnel_read_request (ref byte request,byte[] buf, int length)
		{
			byte req;
			ushort len;
			int n;
			byte[] tempReq = new byte[1];

			this.in_fd.Blocking = false;
			if(!in_fd.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
				return -1;

			if(in_fd.Available == 0)
				return -1;

			n = this.in_fd.Receive(tempReq,0,1 ,SocketFlags.None);
			
		
			req = tempReq[0];

			if (n == -1)
			{
				return n;
			}
			else if (n == 0)
			{
				Logger.writeToLog ("tunnel_read_request: connection closed by peer",false,this.tunnel_is_server(),this.myName);
				this.tunnel_in_disconnect ();

				if (this.tunnel_is_client ()
				&& this.tunnel_in_connect () == -1)
				return -1;
			}

			request = req;

			this.in_total_raw += n;

			if (Convert.ToBoolean(req & TUNNEL_SIMPLE))
			{
				Logger.writeToLog ("tunnel_read_request: " + REQ_TO_STRING (req),false,this.tunnel_is_server(),this.myName);
				length = 0;
				return 1;
			}

			byte[] templen = new byte[2];
			n = 0;
			this.in_fd.Blocking = false;

			while(n < 2)
			{
				if(!in_fd.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
					return -1;

				if(in_fd.Available == 0)
					return -1;
				n += this.in_fd.Receive(templen,n,(2 - n),SocketFlags.None );
			}
			
		
			len = (ushort)BitConverter.ToInt16(ntons(templen),0);

			if (n <= 0)
			{
				Logger.writeToLog ("tunnel_read_request: error reading request length",true,this.tunnel_is_server(),this.myName);
				return -1;
			}

			length = len;
			this.in_total_raw += n;

			if (len > 0)
			{

				n = -1;
				this.in_fd.Blocking = true;

				while(n != len)
				{
					if(!in_fd.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
						return -1;

					if(in_fd.Available == 0)
						return -1;

					if(n == -1)
						n = in_fd.Receive(buf,0,len,SocketFlags.None);
					else
						n += in_fd.Receive(buf,n,len-n,SocketFlags.None );

				}

				if (n <= 0)
				{
					Logger.writeToLog ("tunnel_read_request: error reading request data",true,this.tunnel_is_server(),this.myName);
					return -1;
				}

				this.in_total_raw += n;

			}

			if (req == TUNNEL_DATA)
				Logger.writeToLog ("tunnel_read_request: " + REQ_TO_STRING (req) + " (" + len + ")",false,this.tunnel_is_server(),this.myName);
			else
				Logger.writeToLog ("tunnel_read_request: " + REQ_TO_STRING (req) + " (" + len + ")",false,this.tunnel_is_server(),this.myName);

			return len;
		}

		public Socket tunnel_pollin_fd ()
		{
			if (this.tunnel_is_server () &&
				((this.in_fd == null || this.out_fd == null ) || (!this.in_fd.Connected || !this.out_fd.Connected)))
				{
					if (this.in_fd == null || !this.in_fd.Connected)
						Logger.writeToLog ("tunnel_pollin_fd: in_fd Close; returning server_socket", false,this.tunnel_is_server(),this.myName);
					else
						Logger.writeToLog ("tunnel_pollin_fd: out_fd Close; returning server_socket",false,this.tunnel_is_server(),this.myName);

					if (this.tunnel_accept (true) == -1)
						return null;
					
					return this.in_fd;
					//return this.server_socket;
				}
			else if (this.in_fd.Connected)
				return this.in_fd;
			else
			{
				Logger.writeToLog ("tunnel_pollin_fd: returning -1",true,this.tunnel_is_server(),this.myName);
				return null;
			}
		}

		public int
			tunnel_maybe_pad (int length)
		{
			int padding;

			if (this.tunnel_is_disconnected () ||
				this.bytes % length == 0 ||
				this.padding_only)
				return 0;

			padding = length - (this.bytes % length);
			if (padding > this.content_length - this.bytes)
				padding = this.content_length - this.bytes;

			return this.tunnel_padding (padding);
		}

		public int tunnel_read_and_decrypt(ref byte[] data, int length)
		{
			int len = tunnel_read (ref data, length);

			if(len > 0)
			{
				SymmetricCryptDecrypt.SymmetricDecrypt(this.crypt_decrypt,ref data,ref len);

				if(len < 0)
				{
					return - 1;
				}
			}

			return len;
		}

		public int tunnel_read (ref byte[] data, int length)
		{
			byte req = 0;
			int len = 0;

			if (this.in_fd == null || (!this.in_fd.Connected))
			{
				if (this.tunnel_is_client ())
				{
					if (this.tunnel_in_connect () == -1)
						return -1;
				}
				else
				{
					if (this.tunnel_accept (true) == -1)
						return -1;

				}
			}

			if ((this.out_fd == null || !this.out_fd.Connected) && this.tunnel_is_server())
			{
				if(this.tunnel_accept(true) == -1)
					return -1;
			}

			len = tunnel_read_request (ref req, data,4);
			if (len <= 0)
				return -1;

			switch (req)
			{
				case TUNNEL_OPEN:

					if(data[0] == TUNNEL_AUTH_REQUIRED)
						this.isAuthenticationRequired = true;
					else 
						this.isAuthenticationRequired = false;

					break;

				case TUNNEL_DATA:
					this.in_total_data += len;
					Logger.writeToLog ("tunnel_read: len_data = " + len,false,this.tunnel_is_server(),this.myName);
					Logger.writeToLog ("tunnel_read: in_total_data = " + this.in_total_data,false,this.tunnel_is_server(),this.myName);				
					return len;

				case TUNNEL_PADDING:
					/* discard data */
					break;

				case TUNNEL_PAD1:
					/* do nothing */
					break;

				case TUNNEL_ERROR:
					data[len] = 0;
					Logger.writeToLog ("tunnel_read: received error: " + Encoding.ASCII.GetString(data),true,this.tunnel_is_server(),this.myName);
					return -1;

				case TUNNEL_CLOSE:
					this.ifTunnelCloseRequestReceived = true;
					return Tunnel.TUNNEL_CLOSE_REQ_READ;

				case TUNNEL_DISCONNECT:
					this.tunnel_in_disconnect ();

					if (this.tunnel_is_client () && this.tunnel_in_connect () == -1)
						return -1;

					return 0;

				default:
					Logger.writeToLog ("tunnel_read: protocol error: unknown request " +  req,true,this.tunnel_is_server(),this.myName);
					return -1;
			}

			return 0;
		}

		private static int get_Content_Length_from_headers(Http_request req)
		{
			Http_header req_cp = req.header;

			if(req_cp.name.ToLower().IndexOf("content-length") != -1)
				return Convert.ToInt32(req_cp.value);

			while(req_cp.next != null)
			{
				req_cp = req_cp.next;

				if(req_cp.name.ToLower().IndexOf("content-length") != -1)
					return Convert.ToInt32(req_cp.value.Substring(1,(req_cp.value.Length - 2)));

			}

			return Tunnel.DEFAULT_BUFFER_LENGTH;
		}

		public int tunnel_accept (bool ifReconnect)
		{
			bool temp1 = false,temp2 = false,temp4 = false;
			Tunnel_Auth_Dest temp3 = new Tunnel_Auth_Dest();
			return this.tunnel_accept(ifReconnect,ref temp1,ref temp4,ref temp2,ref temp3);
		}

		public int tunnel_accept (bool ifReconnect,ref bool ifCompressionEnable,ref bool stealthModeEnable,ref bool ifUDPTraffic,ref Tunnel_Auth_Dest ret_newDest)
		{
			bool ifPostArrived = false; // connection structure : fisrt post than get

			if ((this.in_fd != null && this.out_fd != null) && (this.in_fd.Connected && this.out_fd.Connected))
			{
				Logger.writeToLog ("tunnel_accept: tunnel already established",false,this.tunnel_is_server(),this.myName);
				return 0;
			}

			if(this.in_fd != null && !this.in_fd.Connected)
				this.in_fd = null;

			if(this.out_fd != null && !this.out_fd.Connected)
				this.out_fd = null;

			while (this.in_fd == null || this.out_fd == null)
			{
				Http_request request = null;
				int m;
				Socket s = null;

				try 
				{

					if(this.in_fd == null || !this.in_fd.Connected)
					{
						if((s = Server_client_store.wait_and_get_socket(this.Guid_client_ID,HTTPStructures.Http_method.HTTP_POST)) == null)
							return -1;
					} 
					else if(this.out_fd == null || !this.out_fd.Connected)
					{
						if((s = Server_client_store.wait_and_get_socket(this.Guid_client_ID,HTTPStructures.Http_method.HTTP_GET)) == null)
							return -1;
					}

					if(s == null)
						return -1;


					if(ifReconnect)
						if(server_sourceIP != ((IPEndPoint)s.RemoteEndPoint).Address.ToString())
							return -1;

//					if(ifReconnect && !this.compatible_mode)
//					{
//						if(this.server_socket.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
//							s = server_socket.Accept();
//						else
//							return -1;
//
//						if(server_sourceIP != ((IPEndPoint)s.RemoteEndPoint).Address.ToString())
//							return -1;
//
//					} 
//					else 
//					{
//						server_socket.Blocking = true;
//						s = server_socket.Accept();
//
//						if(ifReconnect)
//							if(server_sourceIP != ((IPEndPoint)s.RemoteEndPoint).Address.ToString())
//								return -1;
//
//					}
				} 
				catch (Exception ex)
				{
					Logger.writeToLog ("tunnel_accept: accept error: " + ex.Message,true,this.tunnel_is_server(),this.myName);
					return -1;
				}

				Logger.writeToLog ("connection from Accepted",false,this.tunnel_is_server(),this.myName);

				m = HTTPStructures.http_parse_request (s, ref request);
				if (m <= 0)
					return m;


				if (request.method == HTTPStructures.Http_method.HTTP_UNKNOW)
				{
					Logger.writeToLog ("tunnel_accept: error parsing header !",true,this.tunnel_is_server(),this.myName);
					s.Shutdown(SocketShutdown.Both);
					s.Close();
				}
				else if (request.method == HTTPStructures.Http_method.HTTP_POST ||
					request.method == HTTPStructures.Http_method.HTTP_PUT)
				{
					if (this.in_fd != null)
					{
						try
						{
							if(this.in_fd.Connected)
							{
								this.in_fd.Shutdown(SocketShutdown.Both);
								this.in_fd.Close();
							}
						} 
						catch
						{
						}

						this.in_fd = null;
					}

					this.content_length = get_Content_Length_from_headers(request);

					this.in_fd = s;

					this.in_total_raw += m; /* from parse_header() */

					this.tunnel_in_setsockopts (this.in_fd);

					ifPostArrived = true;

					Logger.writeToLog ("tunnel_accept: input connected",false,this.tunnel_is_server(),this.myName);

				}
				else if (request.method == HTTPStructures.Http_method.HTTP_GET && (ifReconnect?true:ifPostArrived))
				{
					if (this.out_fd == null || !this.out_fd.Connected)
					{
						string str = "";

						this.out_fd = s;
						tunnel_out_setsockopts (this.out_fd);

						str = "HTTP/1.1 200 OK\r\n" +
							"Content-Length: " + (this.content_length + 1) + "\r\n" +
							"Connection: close\r\n" +
							"Pragma: no-cache\r\n" +
							"Cache-Control: no-cache, no-store, must-revalidate\r\n" +
							"Expires: 0\r\n" +
							"Content-Type: text/html\r\n" +
							"\r\n";


						this.out_fd.Blocking = true;
						if (this.out_fd.Send(Encoding.ASCII.GetBytes(str), 0, str.Length,SocketFlags.None ) <= 0)
						{
							Logger.writeToLog ("tunnel_accept: couldn't write GET header",true,this.tunnel_is_server(),this.myName);
							this.out_fd.Shutdown(SocketShutdown.Both);
							this.out_fd.Close();
							this.out_fd = null;
						}
						else
						{
							this.bytes = 0;

							this.out_total_raw += str.Length;

							Logger.writeToLog ("tunnel_accept: output connected",false,this.tunnel_is_server(),this.myName);
						}
					}
					else
					{
						Logger.writeToLog ("tunnel_accept: rejected tunnel_out: already got a connection",false,this.tunnel_is_server(),this.myName);
						s.Shutdown(SocketShutdown.Both);
						s.Close();
					}
				}
				else
				{
					Logger.writeToLog ("tunnel_accept: unknown header type",true,this.tunnel_is_server(),this.myName);
					Logger.writeToLog ("tunnel_accept: closing connection",false,this.tunnel_is_server(),this.myName);
					s.Shutdown(SocketShutdown.Both);
					s.Close();
				}

				HTTPStructures.http_destroy_request(ref request);
			}

		if ((this.in_fd == null || this.out_fd == null) || (!this.in_fd.Connected || !this.out_fd.Connected))
			{
			Logger.writeToLog ("tunnel_accept: in_fd = null or out_fd = null",true,this.tunnel_is_server(),this.myName);

			if (this.in_fd != null)
			{
				this.in_fd.Shutdown(SocketShutdown.Both);
				this.in_fd.Close();
				this.in_fd = null;
			}

			Logger.writeToLog ("tunnel_accept: input disconnected",false,this.tunnel_is_server(),this.myName);

			this.tunnel_out_disconnect ();

			return -1;
			}

			if(!ifReconnect && !this.compatible_mode)
			{
				if(this.Tunnel_accept_Init(ref ifCompressionEnable,ref stealthModeEnable,ref ifUDPTraffic) == -1)
				{
					try
					{
						this.tunnel_close();
					} 
					catch
					{
					}

					return -1;
				}
			}

			if(server_sourceIP == null)
				server_sourceIP = ((IPEndPoint)this.in_fd.RemoteEndPoint).Address.ToString();

			if(this.isAuthenticationRequired && !ifReconnect && !this.compatible_mode)
			{
				if(Authenticate_Client(ref ret_newDest) == -1)
				{
					try
					{
						this.tunnel_close();
					} 
					catch
					{
					}

					return -1;
				}
			}

			if(!ifReconnect && !this.compatible_mode)
			{
				if(Negotiate_RSA_Key_ServerSide() == -1)
				{
					try
					{
						this.tunnel_close();
					} 
					catch
					{
					}

					return -1;
				}
			}

		return 0;
		}

		private int Negotiate_RSA_Key_ServerSide()
		{
			string RSAExp;
			int readVar = 0;
			int response = 0;
			byte[] responseArr = new byte[Tunnel.sizeInt];

			while(readVar <= 0)
			{
				readVar = this.tunnel_read(ref responseArr,Tunnel.sizeInt);

				if(readVar == -1)
					return -1;
			}

			response = BitConverter.ToInt32(responseArr,0);

			if(response == 1)
			{
				try 
				{
					this.ifUseAsimmetricDataEncription = true;

					readVar = 0;
					while(readVar <= 0)
					{
						readVar = this.tunnel_read(ref responseArr,Tunnel.sizeInt);

						if(readVar == -1)
							return -1;
					}

					response = BitConverter.ToInt32(responseArr,0);
					responseArr = new byte[response];

					readVar = 0;
					while(readVar <= 0)
					{
						readVar = this.tunnel_read(ref responseArr,response);

						if(readVar == -1)
							return -1;
					}

					RSAExp = Encoding.ASCII.GetString(responseArr,0,response);

					this.crypt_decrypt.RSAEncrDecr = new RSACryptoServiceProvider();

					this.crypt_decrypt.RSAEncrDecr.FromXmlString(RSAExp);

					this.crypt_decrypt.cryptRijndael = new RijndaelManaged();
					this.crypt_decrypt.cryptRijndael.KeySize = RJ_KEY_SIZE;
					this.crypt_decrypt.cryptRijndael.Padding = PaddingMode.PKCS7;
					this.crypt_decrypt.cryptRijndael.GenerateKey();
					this.crypt_decrypt.cryptRijndael.GenerateIV();

					this.crypt_decrypt.cryptTrasform = this.crypt_decrypt.cryptRijndael.CreateEncryptor();
					this.crypt_decrypt.decryptTrasform  = this.crypt_decrypt.cryptRijndael.CreateDecryptor();

					byte[] decrKey = this.crypt_decrypt.RSAEncrDecr.Encrypt(this.crypt_decrypt.cryptRijndael.Key,false);
					if(this.tunnel_write(BitConverter.GetBytes(decrKey.Length),Tunnel.sizeInt) == -1)
						return -1;
					if(this.tunnel_write(decrKey,decrKey.Length) == -1)
						return -1;

					byte[] decrIV = this.crypt_decrypt.RSAEncrDecr.Encrypt(this.crypt_decrypt.cryptRijndael.IV,false);
					if(this.tunnel_write(BitConverter.GetBytes(decrIV.Length),Tunnel.sizeInt) == -1)
						return -1;
					if(this.tunnel_write(decrIV,decrIV.Length) == -1)
						return -1;
				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
					return -1;
				}
			}
			else
			{
				this.ifUseAsimmetricDataEncription = false;
				this.crypt_decrypt.RSAEncrDecr = null;
			}

			return 1;

		}

		private int Negotiate_RSA_Key_ClientSide(bool Negotiate_RSA_Key)
		{
			int req;
			string RSAExp;
			int readVar = 0;
			byte[] responseArr = new byte[Tunnel.sizeInt];

			if(Negotiate_RSA_Key)
			{
				req = 1;

				if(this.tunnel_write(BitConverter.GetBytes(req),Tunnel.sizeInt) == -1)
					return -1;

				this.crypt_decrypt.RSAEncrDecr = new RSACryptoServiceProvider(Tunnel.RSA_KEY_SIZE);

				RSAExp = this.crypt_decrypt.RSAEncrDecr.ToXmlString(false);

				if(this.tunnel_write(BitConverter.GetBytes(RSAExp.Length),Tunnel.sizeInt) == -1)
					return -1;

				if(this.tunnel_write(Encoding.ASCII.GetBytes(RSAExp),RSAExp.Length) == -1)
					return -1;

				//server return crypto keys
				this.crypt_decrypt.cryptRijndael = new RijndaelManaged();
				this.crypt_decrypt.cryptRijndael.KeySize = RJ_KEY_SIZE;
				this.crypt_decrypt.cryptRijndael.Padding = PaddingMode.PKCS7;

				//key
				readVar = 0;
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref responseArr,Tunnel.sizeInt);

					if(readVar == -1)
						return -1;
				}

				byte[] rjKey = new byte[BitConverter.ToInt32(responseArr,0)];

				readVar = 0;
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref rjKey,rjKey.Length);

					if(readVar == -1)
						return -1;
				}

				//IV
				readVar = 0;
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref responseArr,Tunnel.sizeInt);

					if(readVar == -1)
						return -1;
				}

				byte[] rjIV = new byte[BitConverter.ToInt32(responseArr,0)];

				readVar = 0;
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref rjIV,rjIV.Length);

					if(readVar == -1)
						return -1;
				}

				try
				{
					this.crypt_decrypt.cryptRijndael.Key = this.crypt_decrypt.RSAEncrDecr.Decrypt(rjKey,false);
					this.crypt_decrypt.cryptRijndael.IV = this.crypt_decrypt.RSAEncrDecr.Decrypt(rjIV,false);

					this.crypt_decrypt.cryptTrasform = this.crypt_decrypt.cryptRijndael.CreateEncryptor();
					this.crypt_decrypt.decryptTrasform  = this.crypt_decrypt.cryptRijndael.CreateDecryptor();
				} 
				catch
				{
					return -1;
				}


			} 
			else
			{
				req = 0;

				if(this.tunnel_write(BitConverter.GetBytes(req),Tunnel.sizeInt) == -1)
					return -1;
			}

			return 1;
		}

		private int Authenticate_Client(ref Tunnel_Auth_Dest ret_newDest)
		{
			int readVar = 0;

			try
			{
				string handShake = WarpTunnelAuthenticator.getUnivoqueHandShakeString();
				byte[] handshakebytes = Encoding.ASCII.GetBytes(handShake);

				if(this.tunnel_write(BitConverter.GetBytes(handshakebytes.Length),Tunnel.sizeInt) == -1)
					return -1;

				if(this.tunnel_write(handshakebytes,handshakebytes.Length) == -1)
					return -1;

				//receive length of respose
				byte[] responseLen = new byte[Tunnel.sizeInt];
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref responseLen,Tunnel.sizeInt);

					if(readVar == -1)
						return -1;
				}

				byte[] response = new byte[BitConverter.ToInt32(responseLen,0)];
				readVar = 0;
				while(readVar <= 0)
				{
					readVar = this.tunnel_read(ref response,response.Length);

					if(readVar == -1)
						return -1;
				}

				string[] strResponseArr = Encoding.ASCII.GetString(response,0,response.Length).Split( new char[] {':'});

				if(strResponseArr.Length != 5)
					return -1;

				if(this.curr_Auth_Info.isResponseCorrect(strResponseArr[0],strResponseArr[1],strResponseArr[2],strResponseArr[3],strResponseArr[4]))
				{
					ret_newDest = new Tunnel_Auth_Dest();
					ret_newDest.host = strResponseArr[3];
					ret_newDest.port = Convert.ToInt32(strResponseArr[4]);
					return 1;
				}
				else
				{
					ret_newDest = null;
					return -1;
				}
			} 
			catch
			{
				return -1;
			}
		}

		private int Tunnel_accept_Init(ref bool ifCompressionEnable,ref bool stealthModeEnable, ref bool ifUDPTraffic)
		{
			int readVar = 0;

			//Socket rec_sck = find_server_reconnect_port(this,this.dest.host_name);
			//send new server port 'fixed with new version
			byte[] port_byte = BitConverter.GetBytes(this.server_address.Port);
			
			if(this.tunnel_write(port_byte,Tunnel.sizeInt) == -1)
				return -1;

			//receive from client if compression into tunnel is enabled
			byte[] ifcompress_byte = new byte[Tunnel.sizeByte];
			while(readVar <= 0)
			{
				readVar = this.tunnel_read(ref ifcompress_byte,ifcompress_byte.Length);

				if(readVar == -1)
					return -1;
			}

			if(ifcompress_byte[0] == 2)
			{
				ifCompressionEnable = false;
				stealthModeEnable = true;
			}
			else if(ifcompress_byte[0] == 1)
			{
				ifCompressionEnable = true;
				stealthModeEnable = false;
			}
			else
			{
				ifCompressionEnable = false;
				stealthModeEnable = false;
			}

			byte[] ifudp_byte = new byte[Tunnel.sizeByte];
			readVar = 0;
			while(readVar <= 0)
			{
				readVar = this.tunnel_read(ref ifudp_byte,ifcompress_byte.Length);

				if(readVar == -1)
					return -1;
			}

			if(ifudp_byte[0] == 1)
				ifUDPTraffic = true;
			else
				ifUDPTraffic = false;

			//this.server_socket = rec_sck;


			//ifServerSocketChanged = true;
			//this.server_socket.Listen(10);
			//rec_sck = null;
			return 0;
		}

		public static Tunnel tunnel_new_server (Socket server_socket,string host, int port, int content_length,string serverName,bool ifCompatible,WarpTunnelAuthenticator tunnelAuth,Guid client_id)
		{
			Tunnel tunnel;

			if (host == null || host.Trim().Length == 0)
				host ="localhost";

			tunnel = new Tunnel();

			/* If content_length is 0, a value must be determined automatically. */
			/* For now, a default value will do. */
			if (content_length == 0)
				content_length = DEFAULT_BUFFER_LENGTH;

			tunnel.myName = serverName;
			tunnel.in_fd = null;
			tunnel.out_fd = null;
			tunnel.dest = new Http_destination();
			tunnel.dest.host_name = host;
			tunnel.dest.host_port = port;
			/* -1 to allow for TUNNEL_DISCONNECT */
			tunnel.content_length = content_length - 1;
			tunnel.in_total_raw = 0;
			tunnel.in_total_data = 0;
			tunnel.out_total_raw = 0;
			tunnel.out_total_data = 0;
			tunnel.strict_content_length = false;
			tunnel.bytes = 0;
			tunnel.compatible_mode = ifCompatible;
			tunnel.isServer = true;

			tunnel.Guid_client_ID = client_id;

			tunnel.curr_Auth_Info = tunnelAuth;
			tunnel.server_address = (IPEndPoint)server_socket.LocalEndPoint;
			tunnel.server_socket = server_socket;
			
			return tunnel;
		}

		public static Socket create_new_serverSocket(string host, int port)
		{
			try
			{
				Socket new_Server_Socket = null;
                IPEndPoint new_server_address = new IPEndPoint(Dns.GetHostEntry(host).AddressList[0], port);

				new_Server_Socket = new Socket(new_server_address.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
				new_Server_Socket.Bind(new_server_address);
				new_Server_Socket.Listen(100);
				return new_Server_Socket;
			} 
			catch
			{
				return null;
			}
		}

//		private static Socket find_server_reconnect_port(Tunnel tunnel,string host)
//		{
//			lock(typeof(Tunnel))
//			{
//				int new_port = tunnel.server_address.Port;
//				bool ifPortFind = false;
//				Socket reconnect_socket = null;
//
//				while(!ifPortFind)
//				{
//					try
//					{
//						new_port++;
//						tunnel.reconnect_address = new IPEndPoint(Dns.Resolve(host).AddressList[0], new_port);
//						reconnect_socket = new Socket(tunnel.reconnect_address.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
//						reconnect_socket.Bind(tunnel.reconnect_address);
//						ifPortFind = true;
//						return reconnect_socket;
//					} 
//					catch
//					{
//						if(reconnect_socket != null)
//						{
//							try
//							{
//								reconnect_socket.Close();
//								reconnect_socket = null;
//							} 
//							catch
//							{
//							}
//						}
//					} 
//				}
//				return null;
//			}
//
//		}

		public static Tunnel tunnel_new_client (string host, int host_port,string proxy, int proxy_port,int content_length,string clientName,bool ifCompatible,HTTPStructures.ProxyAuthMethod AuthMeth,bool if_warptunnel_auth_required, bool useEncryption)
		{
			Tunnel tunnel;

			Logger.writeToLog ("tunnel_new_client (\"" + host + "\", " + host_port + ", \""
				+ ((proxy != null && proxy.Trim().Length != 0)  ? proxy : "(null)") + "\", " 
				+ proxy_port + ", " + content_length + ")",false,true,"Client Initialization");

			tunnel = new Tunnel();
			tunnel.dest = new Http_destination();

			/* If content_length is 0, a value must be determined automatically. */
			/* For now, a default value will do. */
			if (content_length == 0)
				content_length = DEFAULT_BUFFER_LENGTH;

			tunnel.content_length = content_length - 1;

			tunnel.isAuthenticationRequired = if_warptunnel_auth_required;
			tunnel.myName = clientName;
			tunnel.in_fd = null;
			tunnel.out_fd = null;
			tunnel.dest.host_name = host;
			tunnel.dest.host_port = host_port;
			tunnel.dest.proxy_name = proxy;
			tunnel.dest.proxy_port = proxy_port;
			tunnel.dest.basic_proxy_authorization  = null;
			tunnel.dest.pass = null;
			tunnel.dest.username = null;
			tunnel.dest.user_agent = null;
			tunnel.dest.AuthMeth = AuthMeth;
			/* -1 to allow for TUNNEL_DISCONNECT */
			tunnel.content_length = content_length - 1;
			tunnel.in_total_raw = 0;
			tunnel.in_total_data = 0;
			tunnel.out_total_raw = 0;
			tunnel.out_total_data = 0;
			tunnel.strict_content_length = false;
			tunnel.bytes = 0;
			tunnel.compatible_mode = ifCompatible;
			tunnel.isServer = false;
			tunnel.Guid_client_ID = Guid.NewGuid();

			tunnel.ifUseAsimmetricDataEncription = useEncryption;

			create_address(tunnel);

			return tunnel;
		}

		private static void create_address(Tunnel tunnel)
		{
			string remote;
			int remote_port;

			if (tunnel.dest.proxy_name == null || tunnel.dest.proxy_name.Trim().Length == 0)
			{
				remote = tunnel.dest.host_name;
				remote_port = tunnel.dest.host_port;
			}
			else
			{
				remote = tunnel.dest.proxy_name;
				remote_port = tunnel.dest.proxy_port;
			}

            IPHostEntry lipa = Dns.GetHostEntry(remote);
			tunnel.address = new IPEndPoint(lipa.AddressList[0], remote_port);
		}

		public void tunnel_destroy ()
		{
			if (this.tunnel_is_connected() || (this.in_fd != null && this.in_fd.Connected))
				this.tunnel_close();

			if(this.tunnel_is_server())
				Server_client_store.remove_client_from_store(this.Guid_client_ID);

			if(this.tunnel_is_client())
				ConnStatRequests.set_Clearable_stat(this.ConnStatRow);

			if(this.crypt_decrypt.cryptTrasform != null)
				this.crypt_decrypt.cryptTrasform.Dispose();

			if(this.crypt_decrypt.decryptTrasform != null)
				this.crypt_decrypt.decryptTrasform.Dispose();

			if(this.crypt_decrypt.RSAEncrDecr != null)
				this.crypt_decrypt.RSAEncrDecr = null;

			if(this.crypt_decrypt.cryptRijndael != null)
			{
				this.crypt_decrypt.cryptRijndael.Clear();
				this.crypt_decrypt.cryptRijndael = null;
			}

		}

		public void destroy_server_socket()
		{
			if (server_socket != null )
			{
				server_socket.Blocking = false;
				server_socket.Close();
				server_socket = null;
			}
		}

		public int tunnel_setopt ( string opt,object data)
		{
			return tunnel_opt (opt,data, false);
		}

		public int tunnel_getopt (string opt, object data)
		{
			return tunnel_opt (opt,data, true);
		}

		public int tunnel_opt (string opt, object data, bool get_flag)
		{
			if (opt == "strict_content_length")
			{
				if (get_flag)
					data = this.strict_content_length;
				else
					this.strict_content_length = (bool)data;
			}
			else if (opt == "keep_alive")
			{
				if (get_flag)
					data = this.keep_alive;
				else
					this.keep_alive = (int)data;
			}
			else if (opt == "max_connection_age")
			{
				if (get_flag)
					data = this.max_connection_age;
				else
					this.max_connection_age = (int)data;
			}
			else if (opt == "proxy_authorization")
			{
				if (get_flag)
				{
					if (this.dest.basic_proxy_authorization == null)
						data = null;
					else
						data = this.dest.basic_proxy_authorization;
				}
				else
				{
					if (this.dest.basic_proxy_authorization != null)
						this.dest.basic_proxy_authorization = null;

					this.dest.basic_proxy_authorization = (string)data;

					if (this.dest.basic_proxy_authorization == null)
						return -1;
				}
			}
			else if (opt == "user_agent")
			{
				if (get_flag)
				{
					if (this.dest.user_agent == null)
						data = null;
					else
						data = this.dest.user_agent;
				}
				else
				{
					if (this.dest.user_agent != null)
						this.dest.user_agent = null;

					this.dest.user_agent = (string)data;

					if (this.dest.user_agent == null)
						return -1;
				}
			}
			else
			{
				return -1;
			}

			return 0;
		}	
	}
}
