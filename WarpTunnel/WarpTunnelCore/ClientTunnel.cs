using System;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Net.Sockets;
using System.Threading;
using System.Security.Cryptography;
using WarpTunnelUtils;
using ConnectionStatistics;
using ClientSocksHTTPConnections;

namespace WarpTunnel
{
	/// <summary>
	/// Summary description for ClientTunnel.
	/// </summary>
	/// 
	delegate void AsyncClientReader(ref byte[] RBuf,Socket RSocket,Tunnel RTunnel, ref bool IfClose, DecompressBuffer decBuf);
	delegate void AsyncClientWriter(ref byte[] WBuf,Socket WSocket,Tunnel WTunnel, ref bool IfClose);

	[Serializable]
	public class ClientTunnel
	{

		public enum TunnelProtocol
		{
			TCP,
			UDP
		}

		public enum Client_Emulate
		{
			NONE = 0,
			CONNECT = 1,
			SOCKS4 = 2,
			SOCKS5 = 3
		}

		private const int DEFAULT_PROXY_PORT = 8080;
		private const int DEFAULT_PROXY_BUFFER_TIMEOUT = 500; /* milliseconds */
		private const int NO_PROXY_BUFFER = 0;
		private const string NO_PROXY = null;
		private Socket foward_Socket = null;
		private int max_readable_data = 0;

		public bool ClientStop = false;
		private Thread ClientServerThread;
		
		private Hashtable AcceptSockets = Hashtable.Synchronized (new Hashtable(100));
		
		public string me;
		public bool ifCompressData;
		public bool ifCompatible;
		public string host_name;
		public int host_port;
		public string proxy_name;
		public int proxy_port;
		public int proxy_buffer_size;
		public int proxy_buffer_timeout;
		public HTTPStructures.ProxyAuthMethod proxy_auth;
		public int content_length;
		public int forward_port;
		public string forward_host;
		public bool strict_content_length;
		public int keep_alive;
		public int max_connection_age;
		public string proxy_authorization;
		public string user_agent;
		public TunnelProtocol ClientProtocol;
		public bool IsTunnelAuthRequired;
		public string tunnelAuthUsername;
		public string tunnelAuthPasword;
		public string tunnelDestHost;
		public int tunnelDestPort;
		public bool ifUseStats;
		public bool stealthModeEnable;
		public Client_Emulate EmulatorKind;
		public Tunnel.Thread_Mode clientThreadMode;
		public bool proxy_Autodetection;
		public bool autoProxyConfiguration;
		public bool ifUseAsimmetricDataEncription;

		private UDPInputBridge UPDServerForClient = null;

		public ClientTunnel()
		{
			this.me = "htc";
			this.ifCompressData = false;
			this.ifCompatible = false;
			this.forward_port = -1;
			this.host_name = null;
			this.host_port = Tunnel.DEFAULT_HOST_PORT;
			this.proxy_name = NO_PROXY;
			this.proxy_port = DEFAULT_PROXY_PORT;
			this.proxy_buffer_size = NO_PROXY_BUFFER;
			this.proxy_buffer_timeout = -1;
			this.proxy_auth = HTTPStructures.ProxyAuthMethod.BASIC_AUTENTICATION;
			this.proxy_Autodetection = false;
			this.content_length = Tunnel.DEFAULT_BUFFER_LENGTH;
			this.strict_content_length = false;
			this.keep_alive = Tunnel.DEFAULT_KEEP_ALIVE;
			this.max_connection_age = Tunnel.DEFAULT_CONNECTION_MAX_TIME;
			this.proxy_authorization = null;
			this.user_agent = null;
			this.IsTunnelAuthRequired = false;
			this.tunnelAuthPasword = null;
			this.tunnelAuthUsername = null;
			this.tunnelDestHost = null;
			this.tunnelDestPort = 0;
			this.ifUseStats = false;
			this.EmulatorKind = Client_Emulate.NONE;
			this.clientThreadMode = Tunnel.Thread_Mode.SoftThreading;
			this.stealthModeEnable = false;
			this.autoProxyConfiguration = false;
			this.ifUseAsimmetricDataEncription = false;
		}

		~ClientTunnel()
		{
			this.stopClient();
		}

		private string toBase64Pass(string pass)
		{
			MD5 passDig = new MD5CryptoServiceProvider();
			passDig.Initialize();

			byte[] bytes_pass = Encoding.ASCII.GetBytes(pass);
			return Convert.ToBase64String(passDig.ComputeHash(bytes_pass,0,bytes_pass.Length));
		}


		public void stopClient()
		{
			if(this.ClientServerThread != null)
			{
				this.ClientStop = true;
				this.ClientServerThread.Abort();

				if(this.foward_Socket != null)
					this.foward_Socket.Close();

				if(this.ClientProtocol == TunnelProtocol.UDP)
				{
					if(this.UPDServerForClient != null)
					{
						if(!this.UPDServerForClient.IfClose)
						{
							this.UPDServerForClient.stop_bridge();
							this.UPDServerForClient = null;
						}
					}
				}

				this.ClientServerThread.Join();
				this.ClientServerThread = null;
				this.foward_Socket = null;


				GC.Collect();
			}
		}

		public void startClient()
		{
			GC.Collect();			
			this.ClientStop = false;
			ConnStatRequests.enableStatistics(this.ifUseStats);

			if(this.autoProxyConfiguration)
			{
                WebProxy proxyconf = (WebProxy)WebRequest.DefaultWebProxy;

				this.proxy_name = proxyconf.Address.Host;
				this.proxy_port = proxyconf.Address.Port;

				this.proxy_authorization = ((NetworkCredential)proxyconf.Credentials).UserName +
					":" + ((NetworkCredential)proxyconf.Credentials).Password;
			}

			if(this.proxy_auth == HTTPStructures.ProxyAuthMethod.DIGEST_AUTHENTICATION)
				ProxyAuth.DigestAuth.clear_digestDataColl();

			try 
			{
				if(this.foward_Socket == null)
				{
					if (this.forward_host == null || this.forward_host.Length == 0)
						this.forward_host = "localhost";

                    IPHostEntry lipa = Dns.GetHostEntry(this.forward_host);
					IPEndPoint lep = new IPEndPoint(lipa.AddressList[0], this.forward_port);
					this.foward_Socket = new Socket(lep.Address.AddressFamily,SocketType.Stream,ProtocolType.Tcp);

					this.foward_Socket.Bind(lep);
					this.foward_Socket.Listen(100);
				}
			} 
			catch
			{
				if(foward_Socket != null)
				{
					Logger.writeToLog ("closing server socket",false,false,this.me);
					this.foward_Socket.Close();
					this.foward_Socket = null;
				}
				ClientStop = true;
				GC.Collect();
			}

			ClientServerThread = new Thread(new ThreadStart(this.ClientServerTunnelThread));
			ClientServerThread.Start();
		}

		private Socket wait_for_connection_on_socket (bool isfirst)
		{
			if(this.ClientProtocol == TunnelProtocol.UDP && isfirst)
				this.UPDServerForClient.start_bridge();

			return this.foward_Socket.Accept();
		}

		private void ClientTunnelThread()
		{
			if(this.AcceptSockets.Count > 0)
			{

				bool closed = false;
				ArrayList sockList = new ArrayList();
				DateTime last_tunnel_write;
				byte[] WBuf = null;
				byte[] RBuf = null;
				Socket fd = null;
				Tunnel tunnel = null;
				DecompressBuffer decBuf = new DecompressBuffer();
				int n_tunnel_pad = 0;

				fd = (Socket)this.AcceptSockets[Thread.CurrentThread];
				this.AcceptSockets.Remove(Thread.CurrentThread);

				Logger.writeToLog ("creating a new tunnel",false,false,this.me);
				tunnel = Tunnel.tunnel_new_client (this.host_name, this.host_port,
					this.proxy_name, this.proxy_port,
					this.content_length,this.me,this.ifCompatible,this.proxy_auth,this.IsTunnelAuthRequired,this.ifUseAsimmetricDataEncription);

				tunnel.tunnel_setsockopts(fd);

				if (tunnel.tunnel_setopt ("strict_content_length",this.strict_content_length) == -1)
					Logger.writeToLog ("tunnel_setopt strict_content_length error !",true,false,this.me);

				if (tunnel.tunnel_setopt ("keep_alive",this.keep_alive) == -1)
					Logger.writeToLog ("tunnel_setopt keep_alive error !", true,false,this.me);

				if (tunnel.tunnel_setopt ("max_connection_age",this.max_connection_age) == -1)
					Logger.writeToLog ("tunnel_setopt max_connection_age error !",true,false,this.me);

				if (this.proxy_authorization != null)
				{
					string auth;

					auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(this.proxy_authorization));
					auth = "Basic "  + auth;

					if (tunnel.tunnel_setopt ("proxy_authorization", auth) == -1)
						Logger.writeToLog ("tunnel_setopt proxy_authorization error !",true,false,this.me);

					tunnel.dest.username = this.proxy_authorization.Split(':')[0];
					tunnel.dest.pass = this.proxy_authorization.Split(':')[1];
					auth = null;
				}

				if (this.user_agent != null)
				{
					if (tunnel.tunnel_setopt ("user_agent", this.user_agent) == -1)
						Logger.writeToLog ("tunnel_setopt user_agent error !",true,false,this.me);
				}

				Tunnel_Client_AuthData clientAuth = null;

				if(!this.ifCompatible && this.IsTunnelAuthRequired)
				{
					clientAuth = new Tunnel_Client_AuthData();
					clientAuth.username = this.tunnelAuthUsername;
					clientAuth.base64digPassword = toBase64Pass(this.tunnelAuthPasword);

					switch (this.EmulatorKind)
					{
						case Client_Emulate.NONE:
							clientAuth.destHost = this.tunnelDestHost;
							clientAuth.destPort = this.tunnelDestPort;
							break;
						case Client_Emulate.CONNECT:
							if(HTTPTunnelConnection.ProcessConnectionRequest(ref clientAuth,fd) == -1)
								closed = true;
							break;
						case Client_Emulate.SOCKS4:
							if(SocksConnection.ProcessConnectionRequest(ref clientAuth,fd) == -1)
								closed = true;
							break;
						case Client_Emulate.SOCKS5:
							if(SocksConnection.ProcessConnectionRequest(ref clientAuth,fd) == -1)
								closed = true;
							break;
					}
				}
					


				if (tunnel.tunnel_connect(this.ifCompressData,this.stealthModeEnable,(this.ClientProtocol == TunnelProtocol.UDP ? true : false),clientAuth) == -1)
				{
					Logger.writeToLog ("couldn't open tunnel !", true,false,this.me);
					return;
				}

				if (this.proxy_name != null)
					Logger.writeToLog ("connected to " + this.host_name + ":" + this.host_port 
						+ " via " + this.proxy_name + ":" + this.proxy_port,false,false,this.me);
				else
					Logger.writeToLog ("connected to " + this.host_name + ":" + this.host_port ,false,false,this.me);


				last_tunnel_write = DateTime.Now.ToUniversalTime();
				bool keep_alive_timeout;
				int timeout;
				DateTime t;

				AsyncClientReader Reader = null;
				AsyncClientWriter Writer = null;

				if(this.clientThreadMode == Tunnel.Thread_Mode.HeavyThreading)
				{
					Reader = new AsyncClientReader(this.TunnelReader);
					Writer = new AsyncClientWriter(this.TunnelWriter);
				}

				WBuf = new byte[Tunnel.DIM_READWRITE_BUFFER_SIZE + (Tunnel.sizeInt + Tunnel.MAX_COMPRESSION_OVERHEAD_SIZE)];
				RBuf = new byte[Tunnel.DIM_READWRITE_BUFFER_SIZE + (Tunnel.sizeInt + Tunnel.MAX_COMPRESSION_OVERHEAD_SIZE)];
				IAsyncResult RRes = null;
				IAsyncResult WRes = null;

				if(!this.ifCompressData && !this.stealthModeEnable)
					max_readable_data = Tunnel.DIM_READWRITE_BUFFER_SIZE;
				else
					max_readable_data = Tunnel.DIM_READWRITE_BUFFER_SIZE < Tunnel.MAX_COPRESS_DATA?Tunnel.DIM_READWRITE_BUFFER_SIZE:Tunnel.MAX_COPRESS_DATA;

				if(this.ifUseAsimmetricDataEncription)
					max_readable_data = max_readable_data - 2*tunnel.crypt_decrypt.cryptRijndael.BlockSize;

				sockList = new ArrayList(2);

				try
				{
					while (!closed && !this.ClientStop)
					{
						sockList.Clear();
						sockList.Add(fd);

						if(tunnel.tunnel_pollin_fd() == null)
						{
							closed = true;
							continue;
						}

						sockList.Add(tunnel.tunnel_pollin_fd());

     
						t = DateTime.Now.ToUniversalTime();
						timeout = 1000 * (this.keep_alive - ((TimeSpan)t.Subtract(last_tunnel_write)).Seconds);

						keep_alive_timeout = true;
						if (timeout < 0)
							timeout = 0;

						if (this.proxy_buffer_timeout != -1 && this.proxy_buffer_timeout < timeout)
						{
							timeout = this.proxy_buffer_timeout;
							keep_alive_timeout = false;
						}

						Socket.Select(sockList,null,null,(timeout * 1000));

						if (sockList.Count == 0)
						{
							Logger.writeToLog ("poll() timed out",false,false,this.me);
							if (keep_alive_timeout)
							{
								if(tunnel.tunnel_padding (1) == -1)
								{
									closed = true;
									continue;
								}

								last_tunnel_write = DateTime.Now.ToUniversalTime();
							}  
							else 
							{
								n_tunnel_pad = tunnel.tunnel_maybe_pad (this.proxy_buffer_size);

								if (n_tunnel_pad > 0)
								{
									last_tunnel_write = DateTime.Now.ToUniversalTime();
								}
								else if(n_tunnel_pad == -1)
								{
									closed = true;
									continue;
								}
							}
							continue;
						}
      
						for (int i = 0; i < sockList.Count ; i++)
						{

							if(((Socket)sockList[i]) == tunnel.tunnel_pollin_fd())
							{
								if(this.clientThreadMode == Tunnel.Thread_Mode.HeavyThreading)
								{
									RRes = Reader.BeginInvoke(ref RBuf,fd,tunnel,ref closed,decBuf,null,null);
								}
								else if(this.clientThreadMode == Tunnel.Thread_Mode.SoftThreading)
								{
									this.TunnelReader(ref RBuf,fd,tunnel,ref closed,decBuf);
								}
							} 
							else 
							{
								if(this.clientThreadMode == Tunnel.Thread_Mode.HeavyThreading)
								{
									WRes = Writer.BeginInvoke(ref WBuf,fd,tunnel,ref closed,null,null);
								} 
								else if(this.clientThreadMode == Tunnel.Thread_Mode.SoftThreading)
								{
									this.TunnelWriter(ref WBuf,fd,tunnel,ref closed);
									last_tunnel_write = DateTime.Now.ToUniversalTime();
								}
							}
						}

						if(this.clientThreadMode == Tunnel.Thread_Mode.HeavyThreading)
						{

							if(RRes != null)
							{

								RRes.AsyncWaitHandle.WaitOne();

								if(RRes.IsCompleted)
									Reader.EndInvoke(ref RBuf,ref closed,RRes);

								RRes.AsyncWaitHandle.Close();
								RRes = null;
							}

							if(WRes != null)
							{

								WRes.AsyncWaitHandle.WaitOne();

								if(WRes.IsCompleted)
									Writer.EndInvoke(ref WBuf,ref closed,WRes);

								WRes.AsyncWaitHandle.Close();
								WRes = null;

								last_tunnel_write = DateTime.Now.ToUniversalTime();
							}
						}
					}
				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
					Logger.writeToLog("Error : " + ex.Message,true,false,this.me);
				}

				Logger.writeToLog ("destroying tunnel",false,false,this.me);
				if (fd != null)
				{
					fd.Shutdown(SocketShutdown.Both);
					fd.Close();
					fd = null;
				}

				tunnel.tunnel_destroy();
				tunnel = null;
				Logger.writeToLog ("disconnected !!",false,false,this.me); 

			}
		}

		private void TunnelWriter(ref byte[] WBuf,Socket WSocket,Tunnel WTunnel, ref bool IfClose)
		{
			int len = 0;

			try  
			{

				if(WSocket.Available == 0)
				{
					IfClose = true;
					return;
				}

				WSocket.Blocking = false;
				len = WSocket.Receive(WBuf,0,max_readable_data,SocketFlags.None);

				if (len == -1 || len == 0 )//|| !WSocket.Connected)
				{
					IfClose = true;
					return;
				} 
								
				if(len > 0)
				{
					int old_len = len;

					if(!this.ifCompatible)
					{
						if(this.ifCompressData || this.stealthModeEnable)
							CompressData.compressDatabuf(ref WBuf,ref len,this.stealthModeEnable);
					}

					if(!this.ifCompatible && this.ifUseAsimmetricDataEncription)
					{
						if(WTunnel.tunnel_write_and_crypt (WBuf, len) == -1)
						{
							IfClose = true;
							return;
						}
					} 
					else
					{
						if(WTunnel.tunnel_write (WBuf, len) == -1)
						{
							IfClose = true;
							return;
						}
					}

					ConnStatRequests.AddConnectionData(WTunnel.ConnStatRow,old_len,len,0,0,false);
				}

			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
					IfClose = true;
			}
		}

		private void TunnelReader(ref byte[] RBuf,Socket RSocket,Tunnel RTunnel, ref bool IfClose, DecompressBuffer decBuf)
		{

			int len = 0;
				
			try 
			{
				if(!this.ifCompatible && this.ifUseAsimmetricDataEncription)
					len = RTunnel.tunnel_read_and_decrypt(ref RBuf, RBuf.Length);
				else
					len = RTunnel.tunnel_read (ref RBuf, RBuf.Length);

				if(len < 0)
				{
					IfClose = true;
					return;
				}
														
				if (len > 0)
				{

					RSocket.Blocking = false; 

					int n = 0;
					int old_len = len;

					if(!this.ifCompatible)
					{

						if(this.ifCompressData || this.stealthModeEnable)
						{
							decBuf.write(RBuf,0,len);
							if(decBuf.isAllBytesIntoBuffer())
							{
								old_len = decBuf.getWriteBytes();
								decBuf.decompressBytes(ref RBuf,ref len);
							}
							else
								return;
						}
					}

					ConnStatRequests.AddConnectionData(RTunnel.ConnStatRow,0,0,len,old_len,false);

					while(n < len)
					{
						if(RSocket.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectWrite))
							n += RSocket.Send(RBuf,n,(len -n),SocketFlags.None);
						else
						{
							IfClose = true;
							return;
						}
					}

				}

			}
			catch
			{
				IfClose = true;
			}
		}

		private void ClientServerTunnelThread()
		{

			bool isfirst = true;

			if(this.ClientProtocol == TunnelProtocol.UDP)
				this.UPDServerForClient = new UDPInputBridge(this.forward_host,this.forward_port,this.content_length);

			while (!this.ClientStop)
			{

				if (this.forward_port != -1)
				{
					Logger.writeToLog ("waiting for connection on port " +  this.forward_port,false,false,this.me);

					try 
					{
						Thread ClientThread = new Thread(new ThreadStart(this.ClientTunnelThread));
						this.AcceptSockets.Add(ClientThread,wait_for_connection_on_socket(isfirst));

						if(isfirst)
							isfirst = !isfirst;

						ClientThread.Start();
					} 
					catch (Exception ex)
					{
						Logger.writeToLog ("couldn't forward port " + this.forward_port + ": " + ex.Message,true,false,this.me);
						return;
					}
				}
				else 
				{
					ClientStop = true;
					return;
				}
			}
		}
	}

}
