using System;
using System.Threading;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using WarpTunnel;

namespace WarpTunnel
{
	internal class UDPToHTTPTunnel
	{

		private Socket TCPOut = null;
		private Thread OutUDPDataThread = null;
		private int total_sleep_time = 0;
		private readonly int  max_wait_time = 30000;
		private bool ifFirstDGramArrived = false;
		private const int sizeByte = sizeof(byte);
		private const int sizeShort = sizeof(ushort);
		private UDPInputBridge fatherClass;
		private EndPoint remoteEP = null;
		private readonly int timer_sleep_time = 10000;
		private Timer bridge_timer = null;
		private bool ifReset = false;
		private byte[] tunnel_message = new byte[0];

		public UDPToHTTPTunnel(IPEndPoint LocalIP,int content_lenght,UDPInputBridge bridgeClass,EndPoint remEP)
		{
			this.fatherClass = bridgeClass;
			this.bridge_timer = new Timer(new TimerCallback(this.timer_thread),new AutoResetEvent(false),0,timer_sleep_time);
			this.remoteEP = remEP;

			this.TCPOut = new Socket(LocalIP.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
			this.TCPOut.Connect(LocalIP);

			this.TCPOut.Blocking = false;

			this.TCPOut.Send(new byte[]{254},0,1,SocketFlags.None);
			this.TCPOut.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead);
			this.TCPOut.Receive(new byte[1],0,1,SocketFlags.None);

			this.TCPOut.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.Linger,new LingerOption (true, 0));
			this.TCPOut.SetSocketOption(SocketOptionLevel.Tcp,SocketOptionName.NoDelay,1);
			this.TCPOut.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.ReceiveBuffer,Tunnel.SOCKET_BUFFER_SIZE);
			this.TCPOut.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.SendBuffer,Tunnel.SOCKET_BUFFER_SIZE);
			this.TCPOut.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.KeepAlive,1);
			this.OutUDPDataThread = new Thread(new ThreadStart(this.OutUDPDataThread_routine));
			this.OutUDPDataThread.Start();
		}

		public void close()
		{
			if(!this.ifReset)
			{
				this.reset_bridge(false);
			}
		}

		public void sendUDPPacketData(byte[] udp_data,int n)
		{
			if(!this.ifFirstDGramArrived)
				this.ifFirstDGramArrived = true;

			//UDP Packet :
			//1 byte (start UDPPacket)
			//2 bytes (length of data)
			//n bytes (UDP Data)

			if(this.tunnel_message.Length < (sizeByte + sizeShort + n))
				this.tunnel_message = new byte[sizeByte + sizeShort + n];

			tunnel_message[0] = UDPInputBridge.UDPPacketStartByte;
			Array.Copy(BitConverter.GetBytes(((ushort)n)),0,tunnel_message,1,sizeShort);
			Array.Copy(udp_data,0,tunnel_message,sizeByte + sizeShort,n);

			n = (sizeByte + sizeShort + n);
			int sended = 0;

			try
			{
				while(sended < n)
				{
					sended += this.TCPOut.Send(tunnel_message,sended,(n - sended),SocketFlags.None);
					this.total_sleep_time = 0;
				}


				this.TCPOut.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead);
				this.TCPOut.Receive(new byte[1],0,1,SocketFlags.None);

			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}


		}


		private void timer_thread(Object stateInfo)
		{
			if(this.ifFirstDGramArrived)
			{
				this.total_sleep_time += this.timer_sleep_time;

				if(this.total_sleep_time >= this.max_wait_time)
				{
					this.reset_bridge(true);
				}
			}

		}

		private void OutUDPDataThread_routine()
		{
			byte[] dataRead = null;
			int n = 0;

			while(!(this.fatherClass.IfClose || ifReset))
			{
				try
				{
					if(!this.TCPOut.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
					{
						continue;
						//this.reset_bridge(true);
						//break;
					}

					if(this.TCPOut.Available == 0)
					{
						this.reset_bridge(true);
						break;
					}

					dataRead = new byte[1];
 
					this.TCPOut.Receive(dataRead,0,1,SocketFlags.None);

					if(dataRead[0] == UDPInputBridge.UDPPacketStartByte)
					{
					
						dataRead = new byte[4];

						n = 0;
						while(n < sizeShort)
						{
							if(!this.TCPOut.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
							{
								this.reset_bridge(true);
								break;
							}

							if(this.TCPOut.Available == 0)
							{
								this.reset_bridge(true);
								break;
							}

							n += this.TCPOut.Receive(dataRead,n,(sizeShort - n),SocketFlags.None );
						}

						ushort dataLen = BitConverter.ToUInt16(dataRead,0);
						dataRead = new byte[dataLen];
						n = 0;

						while(n < dataLen)
						{
							if(!this.TCPOut.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
							{
								this.reset_bridge(true);
								break;
							}

							if(this.TCPOut.Available == 0)
							{
								this.reset_bridge(true);
								break;
							}

							n += this.TCPOut.Receive(dataRead,n,(dataLen - n),SocketFlags.None );
						}

						this.total_sleep_time = 0;
						this.fatherClass.UDPListenSocket.SendTo(dataRead,0,dataLen,SocketFlags.None,this.remoteEP);
					}

				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
		}

		private void reset_bridge(bool ifLocal)
		{
			lock(this)
			{
				ifReset = true;

				if(this.TCPOut != null)
				{
					try
					{
						if(this.TCPOut.Connected)
							this.TCPOut.Shutdown(SocketShutdown.Both);

						this.TCPOut.Close();
					} 
					catch 
					{
					}

					this.TCPOut = null;
					ifFirstDGramArrived = false;
				}


				if(this.OutUDPDataThread != null)
				{
					this.OutUDPDataThread.Abort();
					this.OutUDPDataThread.Join();
					this.OutUDPDataThread = null;
				}

				if(ifLocal)
					this.fatherClass.removeConnfromTable((IPEndPoint)this.remoteEP);
			}

		}

	}


	public class UDPInputBridge
	{
		public Socket UDPListenSocket = null;
		private IPEndPoint LocalIP = null;
		public bool IfClose = false;
		public const byte UDPPacketStartByte = 0x50;
		private Thread InUDPDataThread = null;
		private EndPoint remoteEP = null;
		private Hashtable connTable = null;
		private string LastHashKey = null;
		private UDPToHTTPTunnel LastHashObj = null;

		public void removeConnfromTable(IPEndPoint Key)
		{
			try
			{
				this.connTable.Remove(Key);
			} 
			catch 
			{
			}
		}

		public UDPInputBridge(string WarpClientAddress, int WarpClientPort,int content_lenght)
		{
            this.LocalIP = new IPEndPoint(Dns.GetHostEntry(WarpClientAddress).AddressList[0], WarpClientPort);
			this.IfClose = false;
			this.connTable = Hashtable.Synchronized(new Hashtable(10));

			this.UDPListenSocket = new Socket(AddressFamily.InterNetwork,SocketType.Dgram,ProtocolType.Udp);
			this.UDPListenSocket.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.ReceiveBuffer,Tunnel.SOCKET_BUFFER_SIZE);
			this.UDPListenSocket.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.SendBuffer,Tunnel.SOCKET_BUFFER_SIZE);
			this.UDPListenSocket.Blocking = true;
			this.UDPListenSocket.Bind(this.LocalIP);

		}

		private void InUDPDataThread_routine()
		{
			byte[] udp_data = new byte[Tunnel.SOCKET_BUFFER_SIZE];
			string hashKey = "";
			int n = 0;

			while(!IfClose)
			{
				try
				{
					this.remoteEP = (EndPoint)(new IPEndPoint(IPAddress.Any, 0));
					n = this.UDPListenSocket.ReceiveFrom(udp_data,ref this.remoteEP);
					hashKey = ((IPEndPoint)this.remoteEP).Address.ToString() + ":" + Convert.ToString(((IPEndPoint)this.remoteEP).Port);

					if(n > 0)
					{
						if(this.LastHashKey != null && this.LastHashKey == hashKey && this.connTable.ContainsKey(hashKey))
						{
							this.LastHashObj.sendUDPPacketData(udp_data,n);
						}
						else
						{
							if(!this.connTable.ContainsKey(hashKey))
								this.connTable.Add(hashKey,new UDPToHTTPTunnel(this.LocalIP,Tunnel.SOCKET_BUFFER_SIZE,this,this.remoteEP));

							this.LastHashKey = hashKey;
							this.LastHashObj = ((UDPToHTTPTunnel)this.connTable[hashKey]);
							this.LastHashObj.sendUDPPacketData(udp_data,n);

						}

					} 
				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
		}

		public void start_bridge()
		{
			this.InUDPDataThread = new Thread(new ThreadStart(this.InUDPDataThread_routine));
			this.InUDPDataThread.Start();
		}

		
		public void stop_bridge()
		{
			this.IfClose = true;

			if(this.UDPListenSocket != null)
			{
				try 
				{
					this.UDPListenSocket.Shutdown(SocketShutdown.Both);
					this.UDPListenSocket.Close();
				} 
				catch
				{
				}

				this.UDPListenSocket = null;
			}

			IDictionaryEnumerator tableEnum = this.connTable.GetEnumerator();

			while(tableEnum.MoveNext())
				((UDPToHTTPTunnel)tableEnum.Value).close();

			this.connTable.Clear();

			if(this.InUDPDataThread != null)
			{
				this.InUDPDataThread.Abort();
				this.InUDPDataThread.Join();
				this.InUDPDataThread = null;
			}
		}
	}
}
