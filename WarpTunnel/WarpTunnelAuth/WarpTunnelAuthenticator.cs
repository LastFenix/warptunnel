using System;
using System.IO;
using ProxyAuth;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using WarpTunnel.WarpTunnelAuth;

namespace WarpTunnelAuth
{
	public class WarpTunnelAuthenticator
	{
        public WarpTunnelAuthData AuthData = new WarpTunnelAuthData();
		private string current_AuthFile = null;

		public void loadAuthData(string fileAuth)
		{
			try
			{
				current_AuthFile = fileAuth;
				AuthData.Clear();
				AuthData.ReadXml(fileAuth);
				AuthData.AcceptChanges();
			} 
			catch
			{
			}
		}

		private void saveAuthData()
		{
			try 
			{
				if(current_AuthFile != null)
				{
					if(File.Exists(current_AuthFile))
					{
						File.Delete(current_AuthFile);
					}

					AuthData.AcceptChanges();
					AuthData.WriteXml(current_AuthFile);
				}
			} 
			catch
			{
			}
		}

		public static string getUnivoqueHandShakeString()
		{
			return DigestAuth.GetCurrentNonce();
		}

		public void addUpdateUserPass(string username,string password)
		{
			lock(typeof(WarpTunnelAuthenticator))
			{
				DataRow[] select_ret = (AuthData.WarpTunnelUsersData.Select("Username = '" + username + "'"));
				MD5 calc_pass = new MD5CryptoServiceProvider();
				calc_pass.Initialize();
				string base64digPass = Convert.ToBase64String(calc_pass.ComputeHash(Encoding.ASCII.GetBytes(password)));
				calc_pass = null;

				if(select_ret.Length == 0)
				{
					WarpTunnelAuthData.WarpTunnelUsersDataRow new_row = AuthData.WarpTunnelUsersData.NewWarpTunnelUsersDataRow();
					new_row.Username = username;
					new_row.DigestPass = base64digPass;
					AuthData.WarpTunnelUsersData.AddWarpTunnelUsersDataRow(new_row);
					AuthData.AcceptChanges();
				}
				else
				{

					((WarpTunnelAuthData.WarpTunnelUsersDataRow)select_ret[0]).DigestPass = base64digPass; 
					AuthData.AcceptChanges();
				}

				saveAuthData();
			}
		}

		public void delete_user(string username)
		{
			lock(typeof(WarpTunnelAuthenticator))
			{
				foreach(WarpTunnelAuthData.WarpTunnelUsersDataRow curr_row in AuthData.WarpTunnelUsersData)
				{
					if (curr_row.Username == username)
					{
						curr_row.Delete();
					}
				}

				AuthData.WarpTunnelUsersData.AcceptChanges();
				saveAuthData();
			}
		}

		public static string getAuthString(string username,string base64digPassword,string handShakeString,string destHost,int destPort)
		{
			MD5 try_response = new MD5CryptoServiceProvider();
			try_response.Initialize();

			byte[] clear_resp = Encoding.ASCII.GetBytes(String.Format("{0}:{1}:{2}:{3}:{4}",username,base64digPassword,handShakeString,destHost,Convert.ToString(destPort)));
			string calc_resp = Convert.ToBase64String(try_response.ComputeHash(clear_resp,0,clear_resp.Length));

			return String.Format("{0}:{1}:{2}:{3}:{4}",username,handShakeString,calc_resp,destHost,Convert.ToString(destPort));
		}

		public bool isResponseCorrect(string username,string handShakeString,string response,string destHost,string destPort)
		{
			lock(typeof(WarpTunnelAuthenticator))
			{
				MD5 try_response = new MD5CryptoServiceProvider();
				try_response.Initialize();

				string pass = null;

				try
				{
					pass = ((WarpTunnelAuthData.WarpTunnelUsersDataRow)(AuthData.WarpTunnelUsersData.Select("Username = '" + username + "'"))[0]).DigestPass;
				} 
				catch
				{
					return false;
				}

				byte[] clear_resp = Encoding.ASCII.GetBytes(String.Format("{0}:{1}:{2}:{3}:{4}",username,pass,handShakeString,destHost,destPort));
				string calc_resp = Convert.ToBase64String(try_response.ComputeHash(clear_resp,0,clear_resp.Length));

				if(response == calc_resp)
					return true;

				WarpTunnelUtils.Logger.writeToLog(String.Format("Invalid login user : {0}",username),true,true,"Authenticator");
				return false;
			}

		}
	}
}
