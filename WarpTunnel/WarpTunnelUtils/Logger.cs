using System;
using System.Text;
using System.IO;

namespace WarpTunnelUtils
{
	/// <summary>
	/// Summary description for Logger.
	/// </summary>
	public class Logger
	{
		private static string logFile = "HTTPTunel.log";
		private static FileStream logStream = null;
		private static bool IfLogEnable = false;
		private static bool IfLogErrorsOnly = true;

		~Logger()
		{
			stopLoggerStream();
		}

		public static void startLoggerStream(string sLogFile)
		{
			try
			{
				if(logStream == null)
				{
					if(sLogFile.Trim().Length != 0)
						logFile = sLogFile;

					if(!File.Exists(logFile))
						logStream = new FileStream(Directory.GetCurrentDirectory() + @"\" + logFile, FileMode.CreateNew);
					else
						logStream = new FileStream(Directory.GetCurrentDirectory() + @"\" + logFile,FileMode.Append);
				}
			} 
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		public static void writeToLog(string toWrite,bool ifError,bool isServer,string reqName)
		{
			try
			{
				if((IfLogErrorsOnly && ifError) || (!IfLogErrorsOnly))
				{
					if(IfLogEnable) 
					{
						lock(logStream)
						{
							if(logStream == null)
								startLoggerStream("");

							if(ifError)
								toWrite = "ERROR : " + toWrite;

							toWrite = "( " + reqName + " ) " + toWrite;

							if(isServer)
								toWrite = "Server : " + toWrite;

							byte[] buf = Encoding.ASCII.GetBytes(toWrite + Environment.NewLine);

							logStream.Write(buf,0,buf.Length);
						}
					}
				}
			} 
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

		}

		public static void stopLoggerStream()
		{
			if(logStream != null)
			{
				logStream.Close();
				logStream = null;
			}

		}

		public static void setLoggerEnabled(bool ifEnable)
		{
			if(logStream != null && IfLogEnable) 
				stopLoggerStream();

			IfLogEnable = ifEnable;

		}

		public static void setLogErrorOnly(bool ifEnable)
		{
			IfLogErrorsOnly = ifEnable;
		}

	}

}
