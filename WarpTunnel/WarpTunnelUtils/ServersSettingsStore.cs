using System;
using System.Xml;
using System.IO;
using System.Text;
using WarpTunnelUtils;
using WarpTunnel.WarpTunnelUtils;

namespace WarpTunnel
{
	/// <summary>
	/// Summary description for ServersSettingsStore.
	/// </summary>
	public class ServersSettingsStore
	{
		public static bool saveServersSettings(ServerSetting[] settings,string fileWithPath)
		{
			try
			{
				if(File.Exists(fileWithPath))
					File.Delete(fileWithPath);

				ServerSettingTable serverTable = new ServerSettingTable();

				for(int i = 0; i < settings.Length; i++)
				{
					ServerSettingTable.ServerSettingsRow row = serverTable.ServerSettings.NewServerSettingsRow();

					row.me = settings[i].me;
					row.host = settings[i].host;
					row.port = settings[i].port;
					row.forward_host = settings[i].forward_host;
					row.forward_port = settings[i].forward_port;
					row.content_length = settings[i].content_length;
					row.strict_content_length = settings[i].strict_content_length;
					row.keep_alive = settings[i].keep_alive;
					row.max_connection_age = settings[i].max_connection_age; 
					row.ifCompressData = settings[i].ifCompressData;
					row.ifCompatible = settings[i].ifCompatible;
					row.IfSoftThreadingMode = settings[i].serverThreadingMode == Tunnel.Thread_Mode.SoftThreading?true:false;

					serverTable.ServerSettings.AddServerSettingsRow(row);
					serverTable.AcceptChanges();
				}
			
				serverTable.WriteXml(fileWithPath);

				return true;
			} 
			catch(Exception ex)
			{
				Console.Write(ex.Message);
				return false;
			}
		}

		public static ServerSetting[] loadServersSettings(string fileWithPath)
		{
			try 
			{
				ServerSettingTable serverTable = new ServerSettingTable();
				serverTable.ReadXml(fileWithPath);
				ServerSetting[] settings = new ServerSetting[serverTable.ServerSettings.Rows.Count];
				ServerSettingTable.ServerSettingsRow curr_row;

				for(int i = 0; i < serverTable.ServerSettings.Rows.Count; i++)
				{
					try
					{
						curr_row = (ServerSettingTable.ServerSettingsRow)serverTable.ServerSettings.Rows[i];
						settings[i] = new ServerSetting();

						settings[i].me = curr_row.me;
						settings[i].host = curr_row.host;
						settings[i].port = curr_row.port;
						settings[i].forward_host = curr_row.forward_host;
						settings[i].forward_port = curr_row.forward_port;
						settings[i].content_length = curr_row.content_length;
						settings[i].strict_content_length = curr_row.strict_content_length;
						settings[i].keep_alive = curr_row.keep_alive;
						settings[i].max_connection_age = curr_row.max_connection_age;
						settings[i].ifCompressData = curr_row.ifCompressData;
						settings[i].ifCompatible = curr_row.ifCompatible;
						settings[i].serverThreadingMode = curr_row.IfSoftThreadingMode == true?Tunnel.Thread_Mode.SoftThreading:Tunnel.Thread_Mode.HeavyThreading;

						curr_row = null;
					} 
					catch
					{
					}
				}

				return settings;
			} 
			catch (Exception ex)
			{
				Console.Write(ex.Message);
				return null;
			}
		}
	}
}
