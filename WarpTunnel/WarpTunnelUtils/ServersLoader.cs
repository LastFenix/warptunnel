using System;
using System.Collections;
using WarpTunnelAuth;

namespace WarpTunnel
{
	public class ServersLoader
	{
		private ServersLoader()
		{
		}

		public const string StopServerState = "Stopped";
		public const string RunningServerState = "Running";
		private static ArrayList serverList = new ArrayList();

		public static bool startServersFromSettingsFile(string fileWithPath,string Auth_data)
		{
			serverList.Clear();
			ServerTunnel newServer = null;
			ServerSetting[] servers = ServersSettingsStore.loadServersSettings(fileWithPath);

			if(servers != null)
			{
				for(int i = 0; i < servers.Length; i++)
				{
					newServer = new ServerTunnel();
					servers[i].setTunnelParameters(newServer);
					newServer.tunnelAuth = new WarpTunnelAuthenticator();
					newServer.tunnelAuth.loadAuthData(Auth_data);
					newServer.startServer();
					serverList.Add(newServer);
				}

				return true;
			}
			else
				return false;
		}

		public static void stopServers()
		{
			for(int i = 0; i < serverList.Count; i++)
			{
				((ServerTunnel)serverList[i]).stopServer();
			}
		}

		public static Hashtable getServersStatus()
		{
			if(serverList.Count == 0)
				return null;

			Hashtable retTable = new Hashtable();

			for(int i = 0; i < serverList.Count; i++)
			{
				if(!((ServerTunnel)serverList[i]).disconnect)
					retTable.Add(((ServerTunnel)serverList[i]).me,ServersLoader.RunningServerState);
				else
					retTable.Add(((ServerTunnel)serverList[i]).me,ServersLoader.StopServerState);
			}

			return retTable;
		}
	}
}
