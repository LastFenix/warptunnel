using System;

namespace WarpTunnel
{
	/// <summary>
	/// Summary description for ServerSetting.
	/// </summary>
	[Serializable]
	public class ServerSetting
	{

		public string me;
		public bool ifCompressData;
		public string host;
		public int port;
		public string forward_host;
		public int forward_port;
		public int content_length;
		public bool strict_content_length;
		public int keep_alive;
		public int max_connection_age;
		public bool ifCompatible;
		public Tunnel.Thread_Mode serverThreadingMode;

		public ServerSetting()
		{
			this.me = "";
			this.ifCompressData = false;
			this.host = null;
			this.port = Tunnel.DEFAULT_HOST_PORT;
			this.forward_host = "localhost";
			this.forward_port = 80;
			this.content_length = Tunnel.DEFAULT_BUFFER_LENGTH;
			this.strict_content_length = false;
			this.keep_alive = Tunnel.DEFAULT_KEEP_ALIVE;
			this.max_connection_age = Tunnel.DEFAULT_CONNECTION_MAX_TIME;
			this.ifCompatible = false;
			this.serverThreadingMode = Tunnel.Thread_Mode.SoftThreading;
		}

		public void setTunnelParameters(ServerTunnel tunnel)
		{
			 tunnel.me = this.me;
			 tunnel.host = this.host;
			 tunnel.port = this.port;
			 tunnel.forward_host = this.forward_host;
			 tunnel.forward_port = this.forward_port;
			 tunnel.content_length = this.content_length;
			 tunnel.strict_content_length = this.strict_content_length;
			 tunnel.keep_alive = this.keep_alive;
			 tunnel.max_connection_age = this.max_connection_age;
			 tunnel.ifCompatible = this.ifCompatible;
			 tunnel.serverThreadingMode = this.serverThreadingMode;
		}
	}
}
