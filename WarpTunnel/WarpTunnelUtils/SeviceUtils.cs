using System;
using System.ServiceProcess;

namespace WarpTunnelUtils
{
	/// <summary>
	/// Summary description for SeviceUtils.
	/// </summary>
	public class SeviceUtils
	{
		public static bool isServiceRunning(string srvName)
		{
			if(!isServiceInstalled(srvName))
				return false;

			ServiceController serv = new ServiceController();
			serv.ServiceName = srvName;

			if(serv.Status == ServiceControllerStatus.Running)
				return true;
			else
				return false;

		}

		public static void removeService(string srvName)
		{
			if(isServiceInstalled(srvName))
			{
				uninstallService(srvName);
			}
		}

		public static bool startService(string srvName,string srvDescr,string srvFilePath,bool ifReinstall)
		{
			if(isServiceRunning(srvName))
				stopService(srvName,false);

			if(isServiceInstalled(srvName) && ifReinstall)
			{
				uninstallService(srvName);
				installService(srvName,srvDescr,srvFilePath);
			}

			if(!isServiceInstalled(srvName))
				installService(srvName,srvDescr,srvFilePath);



			ServiceController serv = new ServiceController();
			serv.ServiceName = srvName;

			if(serv.Status == ServiceControllerStatus.Stopped)
			{

				// Start the service
				try
				{
					serv.Start();
					serv.WaitForStatus(ServiceControllerStatus.Running);
					return true;
				} 
				catch
				{
					throw new ArgumentException("Unable to stop service !");
				}

			}

			serv.Close();
			serv.Dispose();
			return true;
		}

		public static void stopService(string srvName,bool ifUninstall)
		{
			ServiceController serv = new ServiceController();
			serv.ServiceName = srvName;

			if(serv.Status == ServiceControllerStatus.Running)
			{

				// Start the service
				try
				{
					serv.Stop();
					serv.WaitForStatus(ServiceControllerStatus.Stopped);
				} 
				catch
				{
					throw new ArgumentException("Unable to stop service !");
				}


				serv.Close();
				serv.Dispose();

				if(ifUninstall)
					removeService(srvName);

			}
		}

		public static void restartService(string srvName,string srvDescr,string srvFilePath)
		{
			stopService(srvName,true);
			startService(srvName,srvDescr,srvFilePath,true);
		}

		public static void installService(string srvName,string srvDescr,string srvFilePath)
		{
			ServiceInstaller.InstallService(srvFilePath ,srvName,srvDescr);		
		}

		public static void uninstallService(string srvName)
		{
			ServiceInstaller.UnInstallService(srvName);

		}

		public static bool  isServiceInstalled(string srvName)
		{
			bool ifFind = false;
			ServiceController[] scServices;
			scServices = ServiceController.GetServices();

			// Display the list of services currently running on this computer.

			foreach (ServiceController scTemp in scServices)
			{
				if(scTemp.ServiceName == srvName)
				{
					ifFind = true;
					break;
				}
			}

			return ifFind;
		}
	}
}
