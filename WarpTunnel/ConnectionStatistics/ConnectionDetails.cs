using System;

namespace ConnectionStatistics
{

	[Serializable]
	public class ConnectionDetails
	{
		public int IDconn;
		public string ServerHost;
		public string ServerPort;

		public string DestinationHost;
		public string DestinationPort;

		public DateTime connTIME;

		public ulong bytesSend;
		public ulong nConncetion;
		public ulong bytesReceive;
		public ulong nRecPackets;

		public DateTime lastTimePacket;

		public ulong nSendPackets;
		public ulong bytesCompressSend;
		public ulong bytesCompressReceive;

		public bool is_clearable;

		public ConnectionDetails(string serverhost,int serverport,string desthost,int destport ,int ID)
		{
			this.IDconn = ID;
			this.ServerHost = serverhost;
			this.ServerPort = Convert.ToString(serverport);
			this.DestinationHost = desthost;
			this.DestinationPort = Convert.ToString(destport);
			this.connTIME = DateTime.Now;
			this.bytesReceive = 0;
			this.bytesSend = 0;
			this.nConncetion = 0;
			this.nRecPackets = 0;
			this.nSendPackets = 0;
			this.bytesCompressReceive = 0;
			this.bytesCompressSend = 0;
			this.lastTimePacket = DateTime.Now;
			this.is_clearable = false;
		}
	}
}
