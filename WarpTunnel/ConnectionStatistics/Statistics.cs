using System;

namespace ConnectionStatistics
{

	[Serializable]
	public class Statistics
	{
		public string Total_Connection_Time = "";

		public ulong Total_Packets_Send = 0;
		public ulong Total_Packets_Receive = 0;
		public ulong Total_Send_Bytes = 0;
		public ulong Total_Receive_Bytes = 0;
		public ulong Compress_Send_Bytes = 0;
		public ulong Compress_Receive_Bytes = 0;
		public ulong Total_Connections = 0;

		public int Avg_Send_Packet_Size = 0;
		public int Avg_Receive_Packet_Size = 0;
		public int Avg_Connections_Seconds = 0;

		public float Avg_Compression_Ratio = 0;
	}
}
