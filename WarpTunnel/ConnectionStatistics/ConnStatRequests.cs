using System;
using System.Text;
using System.Data;
using System.Collections;

namespace ConnectionStatistics
{
	/// <summary>
	/// Summary description for ConnStatRequests.
	/// </summary>
	public class ConnStatRequests
	{

		private static bool ifUseStats = false;
		private static int lastID = -1;
		private static SortedList connDetailsColl =  SortedList.Synchronized(new SortedList());

		public ConnStatRequests()
		{
		}

		public static void enableStatistics(bool ifEnable)
		{
			ifUseStats = ifEnable;
		}

		public static SortedList getConnectionsLogEntries()
		{
			lock(typeof(ConnStatRequests))
			{
				return connDetailsColl;
			}
		}

		public static void clearConnectionsLogEntries()
		{
			lock(typeof(ConnStatRequests))
			{
				for( int i = (connDetailsColl.Count - 1); i >= 0; i--)
				{
					ConnectionDetails curr_det = (ConnectionDetails)connDetailsColl.GetByIndex(i);

					if( curr_det.is_clearable)
						connDetailsColl.RemoveAt(i);
				}
			}
		}

		public static Statistics getConnectionStatsFromConnectionID(int id)
		{

			DateTime FirstTime;
			DateTime LastTime = DateTime.Now;
			ulong totSend = 0;
			ulong totReceive = 0;
			ulong totReconnect = 0;
			ulong totSendPackets = 0;
			ulong totRecPackets = 0;

			ConnectionDetails curr_conn = (ConnectionDetails)connDetailsColl[Convert.ToString(id)];
			Statistics ret_row = new Statistics();

			if(curr_conn != null)
			{

				FirstTime = curr_conn.connTIME;
				LastTime = curr_conn.lastTimePacket;
				totSend = curr_conn.bytesSend;
				totReceive = curr_conn.bytesReceive;
				totReconnect = curr_conn.nConncetion;
				totRecPackets = curr_conn.nRecPackets;
				totSendPackets = curr_conn.nSendPackets;


				TimeSpan timeDiff = ((TimeSpan)LastTime.Subtract(FirstTime));
				ret_row.Total_Connection_Time  = String.Format("{0}:{1}.{2}",timeDiff.Hours,timeDiff.Minutes,timeDiff.Seconds);
				ret_row.Total_Connections = totReconnect;
				ret_row.Total_Receive_Bytes = totReceive;
				ret_row.Total_Send_Bytes = totSend;
				ret_row.Compress_Receive_Bytes = curr_conn.bytesCompressReceive;
				ret_row.Compress_Send_Bytes = curr_conn.bytesCompressSend;
				ret_row.Total_Packets_Send = (ulong)totSendPackets;
				ret_row.Total_Packets_Receive = (ulong)totRecPackets;

				ret_row.Avg_Connections_Seconds = (int)((double)timeDiff.TotalSeconds / (double)totReconnect);
				ret_row.Avg_Receive_Packet_Size = (int)((double)totReceive / (double)totRecPackets);
				ret_row.Avg_Send_Packet_Size = (int)((double)totSend / (double)totSendPackets);
				ret_row.Avg_Compression_Ratio = ((float)1 - ((float)(curr_conn.bytesCompressReceive + curr_conn.bytesCompressSend) / (float)(totReceive + totSend))) * (float)100;

			}

			return ret_row;
		}


		public static ConnectionDetails AddConnectionEntry(string serverhost,int serverport,string desthost,int destport)
		{
			if(ifUseStats)
			{
				lock(typeof(ConnStatRequests))
				{
					lastID++;
					ConnectionDetails new_detail = new ConnectionDetails(serverhost,serverport,desthost,destport,lastID);
					connDetailsColl.Add(Convert.ToString(lastID),new_detail);
					return new_detail;
				}
			} else
				return null;
		}

		public static void set_Clearable_stat(ConnectionDetails current_conn)
		{
			current_conn.is_clearable = true;
		}

		public static void AddConnectionData(ConnectionDetails current_conn,int bytesSend,int bytesUncompressSend,int bytesReceive,int bytesUncompressReceive,bool ifReconnect)
		{
			if(ifUseStats)
			{
				if(ifReconnect)
				{
					current_conn.nConncetion++;
				} 
				else
				{

					if(bytesSend > 0)
					{
						current_conn.nSendPackets++;
						current_conn.bytesSend += (ulong)bytesSend;
						current_conn.bytesCompressSend += (ulong)bytesUncompressSend;
					}
					else
					{
						current_conn.nRecPackets++;
						current_conn.bytesReceive += (ulong)bytesReceive;
						current_conn.bytesCompressReceive += (ulong)bytesUncompressReceive;
					}
				}

				current_conn.lastTimePacket = DateTime.Now;
			
			}
		}
	}
}
