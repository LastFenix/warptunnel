using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WarpTunnel;

namespace ClientSocksHTTPConnections
{
	/// <summary>
	/// Summary description for HTTPConnection.
	/// </summary>
	public class HTTPTunnelConnection 
	{
		private const int MAX_SOCKET_TIMEOUT = 3000000; //MICROSECONDS
		private const int MAX_LEN_REQUEST = 1024 * 10; //BYTE
		private const string Method = "CONNECT";
		private const string EndReq = "\r\n" + "\r\n";
		private const string EndLine = "\r\n";

		public static int ProcessConnectionRequest(ref Tunnel_Client_AuthData clientAuth, Socket ReqSocket)
		{
			StringBuilder req = new StringBuilder();
			int recv = 0;
			int elemFind = 0;
			string HTTPVer = "";
			string RemoteAddressPort = "";
			byte[] recArr = new byte[Tunnel.DEFAULT_BUFFER_LENGTH];

			while(ReqSocket.Poll(MAX_SOCKET_TIMEOUT,SelectMode.SelectRead))
			{
				if(!ReqSocket.Connected || ReqSocket.Available == 0)
					return -1;

				recv = ReqSocket.Receive(recArr,0,recArr.Length,SocketFlags.None);

				req.Append(Encoding.ASCII.GetString(recArr,0,recv));

				if(req.Length > MAX_LEN_REQUEST)
					return -1;

				if(req.ToString().EndsWith(EndReq))
					break;
			}

			if(!req.ToString().StartsWith(Method))
				return -1;

			req.Replace(Method + " ","");

			string[] reqFirstLineElements = (req.ToString().Split(EndLine.ToCharArray()))[0].Split(new char[] {' '});
 
			for(int i = 0; i < reqFirstLineElements.Length; i++)
			{
				if(reqFirstLineElements[i].Length > 0)
				{
					if(elemFind == 0)
					{
						RemoteAddressPort = reqFirstLineElements[i];
						elemFind++;
					} 
					else
					{
						HTTPVer = reqFirstLineElements[i];
						break;
					}
				}
			}

			string[] addressElems = RemoteAddressPort.Split(new char[] {':'});
			clientAuth.destHost = addressElems[0];
			clientAuth.destPort = Convert.ToInt32(addressElems[1]);


			string response = HTTPVer + " 200 OK" + EndReq;
			try
			{
				ReqSocket.Send(Encoding.ASCII.GetBytes(response),0,response.Length,SocketFlags.None);
			} 
			catch
			{
				return -1;
			}

			req =null;

			return 0;
		}

	}
}
