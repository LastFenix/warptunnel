using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WarpTunnel;

namespace ClientSocksHTTPConnections
{
	/// <summary>
	/// Summary description for SocksConnection.
	/// </summary>
	public class SocksConnection
	{
		private const int MAX_SOCKET_TIMEOUT = 3000000; //MICROSECONDS
		private const byte SOCK4Byte = (byte)4;
		private const byte SOCK5Byte = (byte)5;

		private static string get_IPV6_string_rappresentation(byte[] aIP)
		{
			byte[] bIPV6 = new byte[16];

			if(aIP.Length < 16)
				return "";
			else
			{
				try
				{
					Array.Copy(aIP,0,bIPV6,0,16);
					IPAddress ipv6Address = new IPAddress(bIPV6);
					return ipv6Address.ToString();
				} 
				catch
				{
					return "";
				}
			}
		}

		private static int read_n_bytes(ref byte[] recArr,Socket ReqSocket, int nBytes)
		{
			if(!ReqSocket.Poll(MAX_SOCKET_TIMEOUT,SelectMode.SelectRead))
				return -1;

			if(!ReqSocket.Connected)
				return -1;

			try
			{
				ReqSocket.Receive(recArr,0,nBytes,SocketFlags.None);
			} 
			catch
			{
				return -1;
			}

			return 0;

		}

		private static int SOCKS4Process(ref Tunnel_Client_AuthData clientAuth, Socket ReqSocket)
		{
			byte[] recArr = new byte[Tunnel.DEFAULT_BUFFER_LENGTH];


			if(read_n_bytes(ref recArr,ReqSocket,7) == -1) // read connect method,dest port and dest IP
				return -1;

			if(recArr[0] == 1) // connect method
			{
				byte[] resp = new byte[8];
				resp[0] = 0;
				resp[1] = 90;

				//dest port
				resp[2] = recArr[1];
				resp[3] = recArr[2];

				clientAuth.destPort = (int)BitConverter.ToUInt16( new byte[] {recArr[2],recArr[1]},0);

				//dest IP
				resp[4] = recArr[3];
				resp[5] = recArr[4];
				resp[6] = recArr[5];
				resp[7] = recArr[6];


				if(recArr[3] == 0 && recArr[4] == 0 && recArr[5] == 0) // Socks4a
				{
					string domainName = "";
					bool ifFinish = false;

					//read username
					while(!ifFinish)
					{
						if(read_n_bytes(ref recArr,ReqSocket,Tunnel.sizeByte) == -1)
							return -1;

						if(recArr[0] == 0)
							ifFinish = true;
					}

					//read domainname

					ifFinish = false;
					while(!ifFinish)
					{
						if(read_n_bytes(ref recArr,ReqSocket,Tunnel.sizeByte) == -1)
							return -1;

						if(recArr[0] == 0)
							ifFinish = true;
						else
							domainName = domainName + Encoding.ASCII.GetString(recArr,0,1);
					}

					clientAuth.destHost = domainName;

				} 
				else // socks4
				{

					clientAuth.destHost = String.Format("{0}.{1}.{2}.{3}",recArr[3],recArr[4],recArr[5],recArr[6]);

					if(ReqSocket.Available > 0)
					{
						if(read_n_bytes(ref recArr,ReqSocket,ReqSocket.Available) == -1)
							return -1;
					}
				}

				try
				{
					ReqSocket.Send(resp,0,resp.Length,SocketFlags.None);
				} 
				catch
				{
					return -1;
				}


			} 
			else
			{
				return -1;
			}

			return 0;
		}

		private static int SOCKS5Process(ref Tunnel_Client_AuthData clientAuth, Socket ReqSocket)
		{
			byte[] recArr = new byte[Tunnel.DEFAULT_BUFFER_LENGTH];
			byte[] respArr = new byte[Tunnel.DEFAULT_BUFFER_LENGTH];

			respArr[0] = (byte)5;
			respArr[1] = (byte)0;
			respArr[2] = (byte)0;

			//get number of othentication method supported by the client
			if(read_n_bytes(ref recArr,ReqSocket,Tunnel.sizeByte) == -1)
				return -1;

			int nAuthMethods = (int)recArr[0];

			//get authentication methods
			if(read_n_bytes(ref recArr,ReqSocket,nAuthMethods) == -1)
				return -1;

			//control that NO AUTHENTICATION REQUIRED flag is present
			bool onlyAuthPassUserRequired = false;
			bool noAuthPresent = false;

			for(int i = 0; i < nAuthMethods; i++)
			{
				if(recArr[i] == (byte)2)
				{
					onlyAuthPassUserRequired = true;
					continue;
				}
				else if(recArr[i] == (byte)0)
				{
					noAuthPresent = true;
					break;
				}
			}

			if(!noAuthPresent && !onlyAuthPassUserRequired)
			{
				//send failire response to negotiation
				recArr[0] = (byte)5;
				recArr[1] = (byte)0xFF;
				ReqSocket.Send(recArr,0,2,SocketFlags.None);
				return -1;
			} 
			else if(!noAuthPresent && onlyAuthPassUserRequired)
			{
				//send response to negotiation
				recArr[0] = (byte)5;
				recArr[1] = (byte)2;
				ReqSocket.Send(recArr,0,2,SocketFlags.None);

				//get username
				if(read_n_bytes(ref recArr,ReqSocket,2) == -1)
					return -1;

				if(recArr[0] != SOCK5Byte)
					return -1;
				if(read_n_bytes(ref recArr,ReqSocket,(int)recArr[1]) == -1)
					return -1;

				//get password
				if(read_n_bytes(ref recArr,ReqSocket,1) == -1)
					return -1;
				if(read_n_bytes(ref recArr,ReqSocket,(int)recArr[0]) == -1)
					return -1;

				//send response 
				recArr[0] = (byte)5;
				recArr[1] = (byte)0;
				ReqSocket.Send(recArr,0,2,SocketFlags.None);

			}
			else
			{
				//send response to negotiation
				recArr[0] = (byte)5;
				recArr[1] = (byte)0;
				ReqSocket.Send(recArr,0,2,SocketFlags.None);
			}



			//get four bytes of response
			if(read_n_bytes(ref recArr,ReqSocket,4) == -1)
				return -1;

			//control version and command
			if(recArr[0] == (byte)5 && recArr[1] == (byte)1)
			{
				int startIndexResp = 0;
				respArr[3] = recArr[3];

				switch (recArr[3])
				{
					case 0x01: //IPV4

						if(read_n_bytes(ref recArr,ReqSocket,4) == -1)
							return -1;

						Array.Copy(recArr,0,respArr,4,4);
						startIndexResp = 8;

						clientAuth.destHost = String.Format("{0}.{1}.{2}.{3}",recArr[0],recArr[1],recArr[2],recArr[3]);
						break;

					case 0x03: //DOMAIN
						if(read_n_bytes(ref recArr,ReqSocket,1) == -1) //len of domain name
							return -1;

						int domLen = (int)recArr[0];
						respArr[4] = recArr[0];
						if(read_n_bytes(ref recArr,ReqSocket,domLen) == -1)
							return -1;

						Array.Copy(recArr,0,respArr,5,domLen);
						startIndexResp = 5 + domLen;

						clientAuth.destHost = Encoding.ASCII.GetString(recArr,0,domLen);
						break;

					case 0x04: //IPV6	
						if(read_n_bytes(ref recArr,ReqSocket,16) == -1)
							return -1;

						Array.Copy(recArr,0,respArr,4,16);
						startIndexResp = 20;

						clientAuth.destHost = get_IPV6_string_rappresentation(recArr);

						break;
				}

				//GET Dest Port
				if(read_n_bytes(ref recArr,ReqSocket,2) == -1)
					return -1;

				Array.Copy(recArr,0,respArr,startIndexResp,2);

				clientAuth.destPort = (int)BitConverter.ToUInt16( new byte[] {recArr[1],recArr[0]},0);

				//sedn response to client
				try
				{
					ReqSocket.Send(respArr,0,(startIndexResp + 2),SocketFlags.None);
				} 
				catch
				{
					return -1;
				}


			} 
			else
			{
				return -1;
			}

			return 0;
		}

		public static int ProcessConnectionRequest(ref Tunnel_Client_AuthData clientAuth, Socket ReqSocket)
		{
			byte[] recArr = new byte[Tunnel.sizeByte];

			if(read_n_bytes(ref recArr,ReqSocket,Tunnel.sizeByte) == -1)
				return -1;

			if(recArr[0] == SOCK4Byte)
			{
				return SOCKS4Process(ref clientAuth, ReqSocket);
			} 
			else if (recArr[0] == SOCK5Byte)
			{
				return SOCKS5Process(ref clientAuth, ReqSocket);
			} 
			else
			{
				return -1;
			}
		}
	}
}
