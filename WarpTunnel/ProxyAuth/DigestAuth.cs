using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Security.Cryptography;
using WarpTunnel;

namespace ProxyAuth
{
	internal class DigestAuthData
	{
		public string user = null;
		public string pass = null;
		public string warpservURI = null;
		public ListDictionary request_header = null;
		public string req_method = null;

		public DigestAuthData(string username,string password,string WarptunnelURI,string method)
		{
			this.user = username;
			this.pass = password;
			Uri warpUri = new Uri(WarptunnelURI);
			this.warpservURI = warpUri.AbsoluteUri;
			this.req_method = method;
		}
	}

	public class DigestAuth
	{
		private static Hashtable digestDataColl = new Hashtable();

		public static void clear_digestDataColl()
		{
			digestDataColl.Clear();
		}

		private static void tunnel_setsockopts(Socket fd,int content_length)
		{
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.Linger,new LingerOption (true, 0));
			fd.SetSocketOption(SocketOptionLevel.Tcp,SocketOptionName.NoDelay,1);
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.ReceiveBuffer,content_length   * 2);
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.SendBuffer,content_length  * 2);
			fd.SetSocketOption(SocketOptionLevel.Socket,SocketOptionName.KeepAlive,1);
		}

		private static string getDigestTableKey(EndPoint prxEndPoint,string method)
		{
			IPEndPoint prxIPEndPoint = (IPEndPoint)prxEndPoint;

			return prxIPEndPoint.Address.ToString() + ":" + Convert.ToString(prxIPEndPoint.Port) + ":" +  method;
		}

		public static Socket Get_Authentication_Header(int content_length,Socket AuthSck,string username,string password,string WarptunnelURI,string method)
		{
			if(digestDataColl.ContainsKey(getDigestTableKey(AuthSck.RemoteEndPoint,method)))
			{
				return AuthSck;
			}

			DigestAuthData requ_AuthData = new DigestAuthData(username,password,WarptunnelURI,method);

			byte[] buffer = null;
			EndPoint currentEndPoint = AuthSck.RemoteEndPoint;
			AddressFamily currentAddressFamily = AuthSck.AddressFamily;
			SocketType currentSocketType = AuthSck.SocketType;
			ProtocolType currentProtocolType = AuthSck.ProtocolType;

			string firstGET = String.Format("{0} {1} HTTP/1.1" + Environment.NewLine +
											"Proxy-Connection: Keep-Alive" + Environment.NewLine +
											(requ_AuthData.req_method == "POST" || requ_AuthData.req_method == "PUT"? "Content-Length: 3" + Environment.NewLine : "") + 
											"Host: localhost" +
											(requ_AuthData.req_method == "POST"  || requ_AuthData.req_method == "PUT"? Environment.NewLine +  Environment.NewLine + "abc" : "")
											+ Environment.NewLine + Environment.NewLine,requ_AuthData.req_method,requ_AuthData.warpservURI);
			
			AuthSck.Send(Encoding.ASCII.GetBytes(firstGET),0,firstGET.Length,SocketFlags.None);

			if(!AuthSck.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
			{
				AuthSck.Close();
				return null;
			}

			if(AuthSck.Available == 0)
			{
				AuthSck.Close();
				return null;
			}

			string resp_str = "";
			buffer = new byte[AuthSck.Available];

			AuthSck.Receive(buffer,0,buffer.Length,SocketFlags.None);

			resp_str += Encoding.ASCII.GetString(buffer);
			
			int tag_index = resp_str.ToLower().IndexOf("proxy-authenticate: digest");

			if(tag_index != -1)
			{
				construct_digest_response(resp_str.Substring(tag_index,resp_str.IndexOf(Environment.NewLine,tag_index + 1) - tag_index),requ_AuthData);
				//Content-Length: n
				//Get all content response
				if(resp_str.ToLower().IndexOf("content-length") >= 0)
				{
					int content_tag_index = resp_str.ToLower().IndexOf("content-length");
					string content_lenght_string = resp_str.Substring(content_tag_index,resp_str.IndexOf(Environment.NewLine,content_tag_index + 1) - content_tag_index);
					int content_lenght = Convert.ToInt32(content_lenght_string.ToLower().Replace("content-length: ",""));

					int end_response_header = resp_str.ToLower().IndexOf(Environment.NewLine + Environment.NewLine) + 2* Environment.NewLine.Length;
					int curret_lenght = resp_str.Substring(end_response_header).Length ;
					byte[] resp_buf = new byte[content_lenght-curret_lenght];

					if(resp_buf.Length != 0)
					{
						if(!AuthSck.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
						{
							AuthSck.Close();
							return null;
						}

						while(AuthSck.Available < resp_buf.Length);

						AuthSck.Receive(resp_buf,0,content_lenght,SocketFlags.None);
					}
				}
				
				
			} 
			else
			{
				AuthSck.Close();
				return null;
			}

			if(!AuthSck.Connected || resp_str.ToLower().IndexOf("proxy-authenticate") != resp_str.ToLower().LastIndexOf("proxy-authenticate"))
			{
				try
				{
					if(AuthSck.Connected)
						AuthSck.Close();

					AuthSck = new Socket(currentAddressFamily,currentSocketType,currentProtocolType);
					AuthSck.Connect(currentEndPoint);
					tunnel_setsockopts(AuthSck,content_length);
				} 
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
					return null;
				}
			}

			digestDataColl.Add(getDigestTableKey(AuthSck.RemoteEndPoint,method),requ_AuthData);
			return AuthSck;

		}

		public static string[] calculate_digest_response(Socket AuthSck,string method)
		{
			DigestAuthData requ_AuthData = null;

			if(!digestDataColl.ContainsKey(getDigestTableKey(AuthSck.RemoteEndPoint,method)))
			{
				return null;
			}
			else
			{
				requ_AuthData = (DigestAuthData)digestDataColl[getDigestTableKey(AuthSck.RemoteEndPoint,method)];
			}

			MD5 md5Calc = new MD5CryptoServiceProvider();
			md5Calc.Initialize();
			if(!requ_AuthData.request_header.Contains("cnonce"))
			{ 
				requ_AuthData.request_header.Add("cnonce",GetCurrentNonce());
			}

			if(!requ_AuthData.request_header.Contains("nc"))
				requ_AuthData.request_header.Add("nc","00000001");
			else
			{
				string new_nc = Convert.ToString(Convert.ToInt32((string)requ_AuthData.request_header["nc"]) + 1);
				new_nc = "00000000".Substring(0,"00000000".Length - new_nc.Length) + new_nc;
				requ_AuthData.request_header["nc"] = new_nc;
			}

			string A1  = null;

			A1 = String.Format("{0}:{1}:{2}",requ_AuthData.user,(string)requ_AuthData.request_header["realm"],requ_AuthData.pass);

			if(requ_AuthData.request_header.Contains("algorithm") && ((string)requ_AuthData.request_header["algorithm"]) == "MD5-sess")
			{
				//     A1       = H( unq(username-value) ":" unq(realm-value)
				//                ":" passwd )
				//                ":" unq(nonce-value) ":" unq(cnonce-value)

				byte[] A1Dig = (new MD5CryptoServiceProvider()).ComputeHash(Encoding.ASCII.GetBytes(A1));
				String sA1Dig = "";

				for(int i = 0; i < A1Dig.Length; i++)
					sA1Dig += Convert.ToString(A1Dig[i]);

				A1 = GetMD5HashBinHex(sA1Dig + String.Format(":{0}:{1}",(string)requ_AuthData.request_header["nonce"],(string)requ_AuthData.request_header["cnonce"]));
 			} 
			else
			{
				//A1       = unq(username-value) ":" unq(realm-value) ":" passwd

				A1 = GetMD5HashBinHex(A1);
			}

			//A2       = Method ":" digest-uri-value

			string A2 = GetMD5HashBinHex(String.Format("{0}:{1}",requ_AuthData.req_method,requ_AuthData.warpservURI));

			string response = "";
			
			if(requ_AuthData.request_header.Contains("qop"))
			{
				response = GetMD5HashBinHex(String.Format("{0}:{1}:{2}:{3}:{4}:{5}",A1,(string)requ_AuthData.request_header["nonce"],(string)requ_AuthData.request_header["nc"],(string)requ_AuthData.request_header["cnonce"],(string)requ_AuthData.request_header["qop"],A2));
			} 
			else
			{
				response = GetMD5HashBinHex(String.Format("{0}:{1}:{2}",A1,(string)requ_AuthData.request_header["nonce"],A2));
			}

			StringBuilder auth_header = new StringBuilder();
			auth_header.Append("Digest ");

			IDictionaryEnumerator enumColl = requ_AuthData.request_header.GetEnumerator();

			while(enumColl.MoveNext())
			{
				if(((string)enumColl.Key) == "nc")
				{
					auth_header.Append((string)enumColl.Key + "=" + (string)enumColl.Value + ", "); 
				} 
				else
				{
					auth_header.Append((string)enumColl.Key + "=\"" + (string)enumColl.Value + "\", "); 
				}
			}

			auth_header.Append("response=\"" + response + "\"");

			return new string[2] {"Proxy-Authorization",auth_header.ToString()};

			 
		}

		private static string GetMD5HashBinHex(string val)
		{
			Encoding enc = new ASCIIEncoding();
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] bHA1 = md5.ComputeHash(enc.GetBytes(val));
			StringBuilder  HA1 = new StringBuilder("");
			for (int i = 0 ; i < 16 ; i++)
				HA1.Append(String.Format("{0:x02}",bHA1[i]));
			return HA1.ToString();
		}

		public static string GetCurrentNonce()
		{
			// This implementation will create a nonce which is the text 
			// representation of the current time, plus one minute.  The
			// nonce will be valid for this one minute.
			DateTime nonceTime = DateTime.Now + TimeSpan.FromMinutes(10);
			string expireStr = nonceTime.ToString("G");

			Encoding enc = new ASCIIEncoding();
			byte[] expireBytes = enc.GetBytes(expireStr);
			string nonce = Convert.ToBase64String(expireBytes);

			// nonce can't end in '=', so trim them from the end
			nonce = nonce.TrimEnd(new Char[] {'='});
			return nonce;
		}

		private static void construct_digest_response(string proxyauthenticateHdr,DigestAuthData requ_AuthData)
		{
			requ_AuthData.request_header =  new ListDictionary();
			string hdr_name = null;
			string hdr_value = null;

			requ_AuthData.request_header.Add("username",requ_AuthData.user);
			requ_AuthData.request_header.Add("uri",requ_AuthData.warpservURI);

			proxyauthenticateHdr = proxyauthenticateHdr.Substring("Proxy-Authenticate: Digest".Length + 1);

				string[] elems = proxyauthenticateHdr.Split(new char[] {','});
				foreach (string elem in elems)
				{
					// form key="value"
					string[] parts = elem.Split(new char[] {'='}, 2);
					hdr_name = parts[0].Trim(new char[] {' ','\"'});
					hdr_value = parts[1].Trim(new char[] {' ','\"'});
					requ_AuthData.request_header.Add(hdr_name.ToLower(),hdr_value);
				}					

		}
	}
}
