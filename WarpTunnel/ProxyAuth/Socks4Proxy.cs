using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using WarpTunnel;


namespace ProxyAuth
{
	/// <summary>
	/// Summary description for Socks4Proxy.
	/// </summary>
	public class Socks4Proxy
	{
		public static bool ConnectToSocks4Proxy(Socket s, string destAddress, ushort destPort,
			string userName, string password)
		{
			bool ifSocks4A = false;
			byte[] request = null;
			byte[] response = null;
			IPAddress destIP = null;

			try
			{
                destIP = Dns.GetHostEntry(destAddress).AddressList[0];
				request = new byte[userName == null?8:9+userName.Length];
			} 
			catch
			{
				ifSocks4A = true;
				request = new byte[userName == null?8:9+userName.Length+(destAddress.Length + 1)];
			}

			response = new byte[request.Length];
			ushort nIndex;
			bool ret = false;
			byte[] rawBytes;
			nIndex = 0;
			request[nIndex++]=(byte)4;	// version 4.
			request[nIndex++]=0x01;	// command = connect.
			
			request[nIndex++]=BitConverter.GetBytes(destPort)[1];	//  destPort
			request[nIndex++]=BitConverter.GetBytes(destPort)[0];	//  destPort

			if(!ifSocks4A)
			{
				if (destIP != null)
				{// Destination adress in an IP.
					// Address is IPV4 format
					rawBytes = destIP.GetAddressBytes();
					rawBytes.CopyTo(request,nIndex);
					nIndex+=4;
				}
			} 
			else
			{
				request[nIndex++] = 0;
				request[nIndex++] = 0;
				request[nIndex++] = 0;
				request[nIndex++] = 1;
			}

			if(userName != null)
			{
				for(int i = 0; i < userName.Length ; i++)
				{
					request[nIndex++] = Convert.ToByte(userName[i]);
				}

				request[nIndex++] = 0x00;
			}

			if(ifSocks4A)
			{
				for(int i = 0; i < destAddress.Length ; i++)
				{
					request[nIndex++] = Convert.ToByte(destAddress[i]);
				}

				request[nIndex++] = 0x00;
			}
   
			// send connect request.
			s.Send(request,nIndex,SocketFlags.None);
			s.Blocking = false;
			if(!s.Poll(Tunnel.MAX_TIMEOUT_READ_WAIT,SelectMode.SelectRead))
				return false;

			if(s.Available == 0)
				return false;

			s.Receive(response,0,s.Available,SocketFlags.None);	// Get variable length response...
			if (response[1] != 90)
				ret = false;
			else
				ret = true;
			// Success Connected...

			return ret;
		}	
	}
}
