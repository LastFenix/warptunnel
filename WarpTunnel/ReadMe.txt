WarpTunnel is a porting of httptunnel-3.3 from C into .NET C# platform. 

Current Author of WarpTunnel is :
	Stefano Tubini (porting,testing,developing)
	
I take no responsibility for what you do with this software.  It has
the potential to do dangerous things, like disabling the protection
you system administrator has set up for the local network.

WarpTunnel creates a bidirectional virtual data path tunnelled in HTTP
requests.  The requests can be sent via an HTTP proxy if so desired.

This can be useful for users behind restrictive firewalls.  If WWW
access is allowed through an HTTP proxy, it's possible to use
WarpTunnel and, say, telnet or PPP to connect to a computer outside
the firewall.

Original Authors of httptunnel are :

Programming:		Lars Brinkhoff <lars@nocrew.org>
Research:		Stefan Berndtsson <stefan@nocrew.org>
			Magnus Lundstr�m <logic@nocrew.org>
Disclaimer:		Barak Pearlmutter <bap@cs.unm.edu>
Documentation:		Teemu Hukkanen <tjhukkan@fishpool.fi>
getopt_long usage:	Taken from the manual page.
getopt_long code:	Taken from GNU fileutils-4.0.
Patches:		Tomas Berndtsson <tomas@nocrew.org> (Solaris)
			Andrew Gray <agray@cryogen.com> (Solaris, AIX)
			Larry Gensch <larry.gensch@digital.com> (Digital UNIX)
			John Bley <jbb6@acpub.duke.edu>
			Andrew Mobbs <andrewm@chiark.greenend.org.uk>
			Krishna	Swaroop <krishna@pnc.cmc.stph.net>
			Philip Craig <philip@pobox.com> (Windows)
			Jeffrey S Laing <lain@ns.jfl.com>
			Ludovic Rousseau <rousseau@wallace.gemplus.fr>
			Shimayoshi Takao <simayosi@img.sdl.melco.co.jp>
			Chris Lesiak <clesiak@licor.com>
			Albert Chin-A-Young <china@thewrittenword.com>
			Raphael Manfredi <Raphael.Manfredi@st.com>
			Fumitoshi UKAI <ukai@debian.or.jp>
			Brian Somers <brian@Awfulhak.org>
			Sampo Niskanen <sampo.niskanen@iki.fi>
Testing:		Philip Craig <philip@pobox.com>
FAQ:			Lars Brinkhoff <lars@nocrew.org>
			Christian Brideau <cbrideau@newbridge.com>
			... and many novice users.
			
Original site of httptunnel is :

	http://www.nocrew.org/software/httptunnel.html
	http://www.gnu.org/software/httptunnel/httptunnel.html
	ftp://ftp.nocrew.org/pub/nocrew/unix
	ftp://ftp.gnu.org/pub/httptunnel

