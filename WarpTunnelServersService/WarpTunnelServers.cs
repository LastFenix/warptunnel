using System;
using System.ServiceProcess;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Configuration.Install;
using WarpTunnel;
using WarpTunnelUtils;
using WarpTunnel.WarpTunnelUtils;

//System.ServiceProcess.ServiceController myController = 
//new System.ServiceProcess.ServiceController("IISAdmin");

namespace WarpTunnelServersService
{
	public class WarpTunnelServers : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public WarpTunnelServers()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
	
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new WarpTunnelServers() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "WarpTunnelServers";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnStart(string[] args)
		{
			try 
			{
				string ServersSettingsFile = Process.GetCurrentProcess().MainModule.FileName.Substring(0,Process.GetCurrentProcess().MainModule.FileName.LastIndexOf(@"\")); 

				try 
				{
					try 
					{
						ServersLoggerSettings LoggerTable = new ServersLoggerSettings();
						LoggerTable.ReadXml(ServersSettingsFile + @"\" + "ServersLoggerSettings.xml");

						Logger.setLoggerEnabled(LoggerTable.ServersLoggerSetting[0].ifEnabled);
						Logger.setLogErrorOnly(LoggerTable.ServersLoggerSetting[0].LoggerLevel == 0 ? true : false);
						Logger.startLoggerStream(ServersSettingsFile + @"\" + "Logger.log");		
					} 
					catch 
					{
					}
				} 
				catch 
				{
				}

				ServersLoader.startServersFromSettingsFile( ServersSettingsFile + @"\" + "ServersSettings.xml", ServersSettingsFile + @"\" + "AuthData.xml");
			} 
			catch{}
		}
 
		protected override void OnStop() 
		{
			ServersLoader.stopServers();
			Logger.stopLoggerStream();	
			Logger.setLoggerEnabled(false);
			Logger.setLogErrorOnly(true);
		}

		protected override void OnShutdown() 
		{
		}	
	}
}
