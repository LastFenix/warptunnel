using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using WarpTunnel;
using WarpTunnelUtils;
using WarpTunnel.WarpTunnelUtils;
using WarpTunnel.WarpTunnelAuth;

namespace WarpTunnelServerGUI
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class WarpTunnelServer : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.ContextMenu ltMenu;
		private System.Windows.Forms.MenuItem nmuDelServer;
		private System.Windows.Forms.Timer timerServerState;

		private ServerSetting[] serversSetting;
		private const string srvName = "WarpTunnelServers";
		private const string srvDescr = "Warp Tunnel Servers";
		private System.Windows.Forms.TabControl tabControls;
		private System.Windows.Forms.TabPage tabServer;
		private System.Windows.Forms.TabPage tabOptions;
		private System.Windows.Forms.TextBox txServerName;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button btClear;
		private System.Windows.Forms.Button btSaveServer;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txFowPort;
		private System.Windows.Forms.TextBox txFowAddress;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txLocPort;
		private System.Windows.Forms.TextBox txLocAddress;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btRestartService;
		private System.Windows.Forms.Button btStopService;
		private System.Windows.Forms.Button btStartService;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button btStopServers;
		private System.Windows.Forms.Button btStartServers;
		private System.Windows.Forms.ListView ltServers;
		private System.Windows.Forms.ColumnHeader clmServerName;
		private System.Windows.Forms.ColumnHeader clmLocalAddress;
		private System.Windows.Forms.ColumnHeader clmFowardAddress;
		private System.Windows.Forms.ColumnHeader clmServerState;
		private System.Windows.Forms.ColumnHeader clmCompatible;
		private System.Windows.Forms.CheckBox chkCompatible;
		private System.Windows.Forms.GroupBox groupLogger;
		private System.Windows.Forms.ComboBox cmbLoggerLevel;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox chkLogger;
		private System.Windows.Forms.GroupBox groupServer;
		private System.Windows.Forms.GroupBox groupOptions;
		private System.Windows.Forms.TabPage tabAuthenticate;
		private System.Windows.Forms.GroupBox groupAuthenticate;
		private System.Windows.Forms.Label label7;
		public System.Windows.Forms.ColumnHeader clmTunnelUsername;
		private System.Windows.Forms.Button btAuthDelete;
		private System.Windows.Forms.ListView ltAuthList;
		private System.Windows.Forms.GroupBox groupNewAuthUser;
		private System.Windows.Forms.Label lb43;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txAuthUsername;
		private System.Windows.Forms.TextBox txAuthPass;
		private System.Windows.Forms.TextBox txConfirmAuthPass;
		private System.Windows.Forms.Button btClearAtuh;
		private System.Windows.Forms.Button btAddUpdateAuth;
		private System.Windows.Forms.RadioButton radioSoftThread;
		private System.Windows.Forms.RadioButton radioHeavyThread;
		private System.Windows.Forms.ContextMenu AuthDeleteMenu;
		private System.Windows.Forms.MenuItem menuDeleteUser;
		private const string srvPartPathFile = @"\WarpTunnelServersService.exe";
		private System.Windows.Forms.NotifyIcon nIcon;
		private WarpTunnelAuth.WarpTunnelAuthenticator tunnelAuth = new WarpTunnelAuth.WarpTunnelAuthenticator();

		public bool ifStartAtStartup = false;

		public WarpTunnelServer()
		{
			//
			// Required for Windows Form Designer support
			//

			InitializeComponent();

			this.cmbLoggerLevel.SelectedIndex = 0;

			loadServersSettings();

			if(serversSetting != null)
			{
				updateServersList();
				if(SeviceUtils.isServiceRunning(WarpTunnelServer.srvName))
					updateServerState(true);
				else
					updateServerState(false);

				load_auth_data();
			}

			if(!SeviceUtils.isServiceRunning(WarpTunnelServer.srvName) && SeviceUtils.isServiceInstalled(WarpTunnelServer.srvName))
				SeviceUtils.removeService(WarpTunnelServer.srvName);

			if(SeviceUtils.isServiceRunning(WarpTunnelServer.srvName))
			{
				this.btStartService.Enabled = false;
				this.btRestartService.Enabled = true;
				this.btStopService.Enabled = true;
				this.btStopServers.Enabled = false;
				this.btStartServers.Enabled = false;
			}
			else
			{
				this.btStartService.Enabled = true;
				this.btRestartService.Enabled = false;
				this.btStopService.Enabled = false;
				this.btStopServers.Enabled = false;
				this.btStartServers.Enabled = true;
			}		

			this.nIcon.Text = this.Text;
			this.nIcon.Icon = this.Icon;
		}

		private void updateServersList()
		{
			this.ltServers.Items.Clear();

			for(int i = 0; i < serversSetting.Length; i++)
			{
				ListViewItem newItem = this.ltServers.Items.Add(serversSetting[i].me);
				newItem.SubItems.Add(serversSetting[i].host + ":" + Convert.ToString(serversSetting[i].port));
				newItem.SubItems.Add(serversSetting[i].forward_host + ":" + Convert.ToString(serversSetting[i].forward_port));
				newItem.SubItems.Add("");
				newItem.SubItems.Add(serversSetting[i].ifCompatible.ToString());
			}
		}

		private void updateServerState(bool ifService)
		{
			if(this.ltServers.Items.Count > 0)
			{

				this.ltServers.BeginUpdate();

				if(!ifService)
				{

					Hashtable newServerState = ServersLoader.getServersStatus();


					if(newServerState == null)
					{
						for(int i = 0; i < this.ltServers.Items.Count; i++)
							this.ltServers.Items[i].SubItems[3].Text = ServersLoader.StopServerState;
					} 
					else 
					{
						for(int i = 0; i < this.ltServers.Items.Count; i++)
							this.ltServers.Items[i].SubItems[3].Text = (string)newServerState[this.ltServers.Items[i].Text];
					}
				} 
				else 
				{
					for(int i = 0; i < this.ltServers.Items.Count; i++)
						this.ltServers.Items[i].SubItems[3].Text = ServersLoader.RunningServerState;
				}

				this.ltServers.EndUpdate();
			}
		}

		private void loadLoggerSettings()
		{
			try 
			{
				ServersLoggerSettings LoggerTable = new ServersLoggerSettings();
				LoggerTable.ReadXml(Application.StartupPath + @"\" + "ServersLoggerSettings.xml");
				this.chkLogger.Checked = LoggerTable.ServersLoggerSetting[0].ifEnabled;
				this.cmbLoggerLevel.SelectedIndex  = LoggerTable.ServersLoggerSetting[0].LoggerLevel;
			} 
			catch 
			{
			}
		}

		private void saveLoggerSettings()
		{
			ServersLoggerSettings LoggerTable = new ServersLoggerSettings();
			ServersLoggerSettings.ServersLoggerSettingRow row = LoggerTable.ServersLoggerSetting.NewServersLoggerSettingRow();
			row.ifEnabled = this.chkLogger.Checked;
			row.LoggerFile = "";
			row.LoggerLevel = this.cmbLoggerLevel.SelectedIndex;
			LoggerTable.ServersLoggerSetting.AddServersLoggerSettingRow(row);
			LoggerTable.ServersLoggerSetting.AcceptChanges();
			LoggerTable.WriteXml(Application.StartupPath + @"\" + "ServersLoggerSettings.xml");
		}

		private void loadServersSettings()
		{
			serversSetting = ServersSettingsStore.loadServersSettings(Application.StartupPath + @"\" + "ServersSettings.xml");
			this.loadLoggerSettings();
		}

		private void saveServersSettings()
		{
			ServersSettingsStore.saveServersSettings(serversSetting,Application.StartupPath + @"\" + "ServersSettings.xml");
			this.saveLoggerSettings();
		}

		private int findExistIndex(string serverName)
		{
			if(this.serversSetting == null)
				return -1;

			for(int i = 0; i < this.serversSetting.Length; i++)
			{
				if(this.serversSetting[i].me == serverName)
					return i;

			}

			return -1;
		}

		private void deletememorizedServersSetting(string serverName)
		{
			ServerSetting[] newSettings = new ServerSetting[this.serversSetting.Length - 1];
			int j = 0;
		
			for(int i = 0; i < this.serversSetting.Length; i++)
			{
				if(this.serversSetting[i].me != serverName)
				{
					newSettings[j] = this.serversSetting[i];
					j++;
				}
			}

			this.serversSetting = newSettings;

			this.saveServersSettings();
			this.updateServersList();
		}

		private void memorizedServersSettings()
		{
			int newIndex = findExistIndex(this.txServerName.Text);

			if(newIndex < 0)
			{
				if(this.serversSetting != null)
				{
					newIndex = this.serversSetting.Length;
					ServerSetting[] newSettings = new ServerSetting[newIndex + 1];
					this.serversSetting.CopyTo(newSettings,0);
					this.serversSetting = newSettings;
					this.serversSetting[newIndex] = new ServerSetting();
				} 
				else 
				{
					newIndex = 0;
					this.serversSetting = new ServerSetting[newIndex + 1];
					this.serversSetting[newIndex] = new ServerSetting();
				}
			}

			this.serversSetting[newIndex].me = this.txServerName.Text;
			this.serversSetting[newIndex].ifCompressData = false;
			this.serversSetting[newIndex].host = this.txLocAddress.Text;
			this.serversSetting[newIndex].port = Convert.ToInt32(this.txLocPort.Text);
			this.serversSetting[newIndex].forward_host = this.txFowAddress.Text;
			this.serversSetting[newIndex].forward_port = Convert.ToInt32(this.txFowPort.Text);
			this.serversSetting[newIndex].ifCompatible = this.chkCompatible.Checked;
			this.serversSetting[newIndex].serverThreadingMode = this.radioSoftThread.Checked == true?Tunnel.Thread_Mode.SoftThreading:Tunnel.Thread_Mode.HeavyThreading;

			this.saveServersSettings();
			this.updateServersList();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(WarpTunnelServer));
			this.ltMenu = new System.Windows.Forms.ContextMenu();
			this.nmuDelServer = new System.Windows.Forms.MenuItem();
			this.timerServerState = new System.Windows.Forms.Timer(this.components);
			this.tabControls = new System.Windows.Forms.TabControl();
			this.tabServer = new System.Windows.Forms.TabPage();
			this.ltServers = new System.Windows.Forms.ListView();
			this.clmServerName = new System.Windows.Forms.ColumnHeader();
			this.clmLocalAddress = new System.Windows.Forms.ColumnHeader();
			this.clmFowardAddress = new System.Windows.Forms.ColumnHeader();
			this.clmServerState = new System.Windows.Forms.ColumnHeader();
			this.clmCompatible = new System.Windows.Forms.ColumnHeader();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.btStopServers = new System.Windows.Forms.Button();
			this.btStartServers = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.btRestartService = new System.Windows.Forms.Button();
			this.btStopService = new System.Windows.Forms.Button();
			this.btStartService = new System.Windows.Forms.Button();
			this.groupServer = new System.Windows.Forms.GroupBox();
			this.txServerName = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.btClear = new System.Windows.Forms.Button();
			this.btSaveServer = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.txFowPort = new System.Windows.Forms.TextBox();
			this.txFowAddress = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txLocPort = new System.Windows.Forms.TextBox();
			this.txLocAddress = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tabOptions = new System.Windows.Forms.TabPage();
			this.groupOptions = new System.Windows.Forms.GroupBox();
			this.radioSoftThread = new System.Windows.Forms.RadioButton();
			this.radioHeavyThread = new System.Windows.Forms.RadioButton();
			this.chkCompatible = new System.Windows.Forms.CheckBox();
			this.groupLogger = new System.Windows.Forms.GroupBox();
			this.cmbLoggerLevel = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.chkLogger = new System.Windows.Forms.CheckBox();
			this.tabAuthenticate = new System.Windows.Forms.TabPage();
			this.groupAuthenticate = new System.Windows.Forms.GroupBox();
			this.groupNewAuthUser = new System.Windows.Forms.GroupBox();
			this.txConfirmAuthPass = new System.Windows.Forms.TextBox();
			this.txAuthPass = new System.Windows.Forms.TextBox();
			this.txAuthUsername = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.btClearAtuh = new System.Windows.Forms.Button();
			this.btAddUpdateAuth = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.lb43 = new System.Windows.Forms.Label();
			this.btAuthDelete = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.ltAuthList = new System.Windows.Forms.ListView();
			this.clmTunnelUsername = new System.Windows.Forms.ColumnHeader();
			this.AuthDeleteMenu = new System.Windows.Forms.ContextMenu();
			this.menuDeleteUser = new System.Windows.Forms.MenuItem();
			this.nIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.tabControls.SuspendLayout();
			this.tabServer.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupServer.SuspendLayout();
			this.tabOptions.SuspendLayout();
			this.groupOptions.SuspendLayout();
			this.groupLogger.SuspendLayout();
			this.tabAuthenticate.SuspendLayout();
			this.groupAuthenticate.SuspendLayout();
			this.groupNewAuthUser.SuspendLayout();
			this.SuspendLayout();
			// 
			// ltMenu
			// 
			this.ltMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				   this.nmuDelServer});
			this.ltMenu.Popup += new System.EventHandler(this.ltMenu_Popup);
			// 
			// nmuDelServer
			// 
			this.nmuDelServer.Index = 0;
			this.nmuDelServer.Text = "Delete Server";
			this.nmuDelServer.Click += new System.EventHandler(this.nmuDelServer_Click);
			// 
			// timerServerState
			// 
			this.timerServerState.Enabled = true;
			this.timerServerState.Interval = 10000;
			this.timerServerState.Tick += new System.EventHandler(this.timerServerState_Tick);
			// 
			// tabControls
			// 
			this.tabControls.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControls.Controls.Add(this.tabServer);
			this.tabControls.Controls.Add(this.tabOptions);
			this.tabControls.Controls.Add(this.tabAuthenticate);
			this.tabControls.Location = new System.Drawing.Point(0, 8);
			this.tabControls.Name = "tabControls";
			this.tabControls.SelectedIndex = 0;
			this.tabControls.Size = new System.Drawing.Size(504, 448);
			this.tabControls.TabIndex = 9;
			this.tabControls.SelectedIndexChanged += new System.EventHandler(this.tabControls_SelectedIndexChanged);
			// 
			// tabServer
			// 
			this.tabServer.Controls.Add(this.ltServers);
			this.tabServer.Controls.Add(this.groupBox3);
			this.tabServer.Controls.Add(this.groupBox2);
			this.tabServer.Controls.Add(this.groupServer);
			this.tabServer.Location = new System.Drawing.Point(4, 22);
			this.tabServer.Name = "tabServer";
			this.tabServer.Size = new System.Drawing.Size(496, 422);
			this.tabServer.TabIndex = 0;
			this.tabServer.Text = "Server Settings";
			// 
			// ltServers
			// 
			this.ltServers.Activation = System.Windows.Forms.ItemActivation.TwoClick;
			this.ltServers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ltServers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.clmServerName,
																						this.clmLocalAddress,
																						this.clmFowardAddress,
																						this.clmServerState,
																						this.clmCompatible});
			this.ltServers.ContextMenu = this.ltMenu;
			this.ltServers.FullRowSelect = true;
			this.ltServers.GridLines = true;
			this.ltServers.HideSelection = false;
			this.ltServers.Location = new System.Drawing.Point(8, 200);
			this.ltServers.MultiSelect = false;
			this.ltServers.Name = "ltServers";
			this.ltServers.Size = new System.Drawing.Size(480, 184);
			this.ltServers.TabIndex = 8;
			this.ltServers.View = System.Windows.Forms.View.Details;
			this.ltServers.ItemActivate += new System.EventHandler(this.ltServers_ItemActivate);
			// 
			// clmServerName
			// 
			this.clmServerName.Text = "Server Name";
			this.clmServerName.Width = 82;
			// 
			// clmLocalAddress
			// 
			this.clmLocalAddress.Text = "Server Address";
			this.clmLocalAddress.Width = 89;
			// 
			// clmFowardAddress
			// 
			this.clmFowardAddress.Text = "Foward Address";
			this.clmFowardAddress.Width = 96;
			// 
			// clmServerState
			// 
			this.clmServerState.Text = "Server State";
			this.clmServerState.Width = 79;
			// 
			// clmCompatible
			// 
			this.clmCompatible.Text = "Compatible";
			this.clmCompatible.Width = 76;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.btStopServers);
			this.groupBox3.Controls.Add(this.btStartServers);
			this.groupBox3.Location = new System.Drawing.Point(360, 120);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(134, 80);
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			// 
			// btStopServers
			// 
			this.btStopServers.Location = new System.Drawing.Point(8, 48);
			this.btStopServers.Name = "btStopServers";
			this.btStopServers.Size = new System.Drawing.Size(120, 24);
			this.btStopServers.TabIndex = 1;
			this.btStopServers.Text = "Stop Servers";
			this.btStopServers.Click += new System.EventHandler(this.btStopServers_Click);
			// 
			// btStartServers
			// 
			this.btStartServers.Location = new System.Drawing.Point(8, 16);
			this.btStartServers.Name = "btStartServers";
			this.btStartServers.Size = new System.Drawing.Size(120, 24);
			this.btStartServers.TabIndex = 0;
			this.btStartServers.Text = "Start Servers";
			this.btStartServers.Click += new System.EventHandler(this.btStartServers_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.btRestartService);
			this.groupBox2.Controls.Add(this.btStopService);
			this.groupBox2.Controls.Add(this.btStartService);
			this.groupBox2.Location = new System.Drawing.Point(360, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(136, 112);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			// 
			// btRestartService
			// 
			this.btRestartService.Location = new System.Drawing.Point(8, 48);
			this.btRestartService.Name = "btRestartService";
			this.btRestartService.Size = new System.Drawing.Size(120, 24);
			this.btRestartService.TabIndex = 1;
			this.btRestartService.Text = "Restart Service";
			this.btRestartService.Click += new System.EventHandler(this.btRestartService_Click);
			// 
			// btStopService
			// 
			this.btStopService.Location = new System.Drawing.Point(8, 80);
			this.btStopService.Name = "btStopService";
			this.btStopService.Size = new System.Drawing.Size(120, 24);
			this.btStopService.TabIndex = 2;
			this.btStopService.Text = "Stop Service";
			this.btStopService.Click += new System.EventHandler(this.btStopService_Click);
			// 
			// btStartService
			// 
			this.btStartService.Location = new System.Drawing.Point(8, 16);
			this.btStartService.Name = "btStartService";
			this.btStartService.Size = new System.Drawing.Size(120, 24);
			this.btStartService.TabIndex = 0;
			this.btStartService.Text = "Start as a Service";
			this.btStartService.Click += new System.EventHandler(this.btStartService_Click);
			// 
			// groupServer
			// 
			this.groupServer.Controls.Add(this.txServerName);
			this.groupServer.Controls.Add(this.label6);
			this.groupServer.Controls.Add(this.btClear);
			this.groupServer.Controls.Add(this.btSaveServer);
			this.groupServer.Controls.Add(this.label4);
			this.groupServer.Controls.Add(this.txFowPort);
			this.groupServer.Controls.Add(this.txFowAddress);
			this.groupServer.Controls.Add(this.label3);
			this.groupServer.Controls.Add(this.txLocPort);
			this.groupServer.Controls.Add(this.txLocAddress);
			this.groupServer.Controls.Add(this.label2);
			this.groupServer.Controls.Add(this.label1);
			this.groupServer.Location = new System.Drawing.Point(8, 8);
			this.groupServer.Name = "groupServer";
			this.groupServer.Size = new System.Drawing.Size(352, 192);
			this.groupServer.TabIndex = 1;
			this.groupServer.TabStop = false;
			// 
			// txServerName
			// 
			this.txServerName.Location = new System.Drawing.Point(96, 16);
			this.txServerName.Name = "txServerName";
			this.txServerName.Size = new System.Drawing.Size(248, 20);
			this.txServerName.TabIndex = 1;
			this.txServerName.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(112, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "Server Name :";
			// 
			// btClear
			// 
			this.btClear.Location = new System.Drawing.Point(144, 160);
			this.btClear.Name = "btClear";
			this.btClear.Size = new System.Drawing.Size(120, 24);
			this.btClear.TabIndex = 9;
			this.btClear.Text = "Clear Settings";
			this.btClear.Click += new System.EventHandler(this.btClear_Click);
			// 
			// btSaveServer
			// 
			this.btSaveServer.Location = new System.Drawing.Point(8, 160);
			this.btSaveServer.Name = "btSaveServer";
			this.btSaveServer.Size = new System.Drawing.Size(128, 24);
			this.btSaveServer.TabIndex = 8;
			this.btSaveServer.Text = "Save Server Settings";
			this.btSaveServer.Click += new System.EventHandler(this.btSaveServer_Click);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(272, 128);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(16, 24);
			this.label4.TabIndex = 7;
			this.label4.Text = ":";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txFowPort
			// 
			this.txFowPort.Location = new System.Drawing.Point(288, 128);
			this.txFowPort.Name = "txFowPort";
			this.txFowPort.Size = new System.Drawing.Size(56, 20);
			this.txFowPort.TabIndex = 6;
			this.txFowPort.Text = "";
			this.txFowPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txFowPort.TextChanged += new System.EventHandler(this.txFowPort_TextChanged);
			// 
			// txFowAddress
			// 
			this.txFowAddress.Location = new System.Drawing.Point(8, 128);
			this.txFowAddress.Name = "txFowAddress";
			this.txFowAddress.Size = new System.Drawing.Size(264, 20);
			this.txFowAddress.TabIndex = 5;
			this.txFowAddress.Text = "";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(272, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(16, 24);
			this.label3.TabIndex = 4;
			this.label3.Text = ":";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txLocPort
			// 
			this.txLocPort.Location = new System.Drawing.Point(288, 72);
			this.txLocPort.MaxLength = 5;
			this.txLocPort.Name = "txLocPort";
			this.txLocPort.Size = new System.Drawing.Size(56, 20);
			this.txLocPort.TabIndex = 3;
			this.txLocPort.Text = "8888";
			this.txLocPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txLocPort.TextChanged += new System.EventHandler(this.txLocPort_TextChanged);
			// 
			// txLocAddress
			// 
			this.txLocAddress.Location = new System.Drawing.Point(8, 72);
			this.txLocAddress.Name = "txLocAddress";
			this.txLocAddress.Size = new System.Drawing.Size(264, 20);
			this.txLocAddress.TabIndex = 2;
			this.txLocAddress.Text = "0.0.0.0";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 104);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(144, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Foward Host/Port :";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Server Host/Port :";
			// 
			// tabOptions
			// 
			this.tabOptions.Controls.Add(this.groupOptions);
			this.tabOptions.Controls.Add(this.groupLogger);
			this.tabOptions.Location = new System.Drawing.Point(4, 22);
			this.tabOptions.Name = "tabOptions";
			this.tabOptions.Size = new System.Drawing.Size(496, 422);
			this.tabOptions.TabIndex = 1;
			this.tabOptions.Text = "Options";
			// 
			// groupOptions
			// 
			this.groupOptions.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupOptions.Controls.Add(this.radioSoftThread);
			this.groupOptions.Controls.Add(this.radioHeavyThread);
			this.groupOptions.Controls.Add(this.chkCompatible);
			this.groupOptions.Location = new System.Drawing.Point(4, 8);
			this.groupOptions.Name = "groupOptions";
			this.groupOptions.Size = new System.Drawing.Size(484, 88);
			this.groupOptions.TabIndex = 1;
			this.groupOptions.TabStop = false;
			this.groupOptions.Text = "Options :";
			// 
			// radioSoftThread
			// 
			this.radioSoftThread.Checked = true;
			this.radioSoftThread.Location = new System.Drawing.Point(296, 56);
			this.radioSoftThread.Name = "radioSoftThread";
			this.radioSoftThread.Size = new System.Drawing.Size(144, 16);
			this.radioSoftThread.TabIndex = 17;
			this.radioSoftThread.TabStop = true;
			this.radioSoftThread.Text = "SoftThread mode";
			// 
			// radioHeavyThread
			// 
			this.radioHeavyThread.Location = new System.Drawing.Point(24, 56);
			this.radioHeavyThread.Name = "radioHeavyThread";
			this.radioHeavyThread.Size = new System.Drawing.Size(152, 16);
			this.radioHeavyThread.TabIndex = 16;
			this.radioHeavyThread.Text = "HeavyThread mode";
			// 
			// chkCompatible
			// 
			this.chkCompatible.Location = new System.Drawing.Point(20, 24);
			this.chkCompatible.Name = "chkCompatible";
			this.chkCompatible.Size = new System.Drawing.Size(176, 16);
			this.chkCompatible.TabIndex = 13;
			this.chkCompatible.Text = "Run in HTC Compatible Mode";
			// 
			// groupLogger
			// 
			this.groupLogger.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupLogger.Controls.Add(this.cmbLoggerLevel);
			this.groupLogger.Controls.Add(this.label5);
			this.groupLogger.Controls.Add(this.chkLogger);
			this.groupLogger.Location = new System.Drawing.Point(4, 96);
			this.groupLogger.Name = "groupLogger";
			this.groupLogger.Size = new System.Drawing.Size(484, 56);
			this.groupLogger.TabIndex = 2;
			this.groupLogger.TabStop = false;
			this.groupLogger.Text = "Logger :";
			// 
			// cmbLoggerLevel
			// 
			this.cmbLoggerLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbLoggerLevel.Enabled = false;
			this.cmbLoggerLevel.Items.AddRange(new object[] {
																"0 - Log Only Errors",
																"1 - Log Every Actions "});
			this.cmbLoggerLevel.Location = new System.Drawing.Point(232, 24);
			this.cmbLoggerLevel.Name = "cmbLoggerLevel";
			this.cmbLoggerLevel.Size = new System.Drawing.Size(248, 21);
			this.cmbLoggerLevel.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(152, 32);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 16);
			this.label5.TabIndex = 1;
			this.label5.Text = "Logger Level :";
			// 
			// chkLogger
			// 
			this.chkLogger.Location = new System.Drawing.Point(16, 32);
			this.chkLogger.Name = "chkLogger";
			this.chkLogger.Size = new System.Drawing.Size(112, 16);
			this.chkLogger.TabIndex = 0;
			this.chkLogger.Text = "Enable Logger";
			this.chkLogger.CheckedChanged += new System.EventHandler(this.chkLogger_CheckedChanged);
			// 
			// tabAuthenticate
			// 
			this.tabAuthenticate.Controls.Add(this.groupAuthenticate);
			this.tabAuthenticate.Location = new System.Drawing.Point(4, 22);
			this.tabAuthenticate.Name = "tabAuthenticate";
			this.tabAuthenticate.Size = new System.Drawing.Size(496, 422);
			this.tabAuthenticate.TabIndex = 2;
			this.tabAuthenticate.Text = "Authenticate Users";
			// 
			// groupAuthenticate
			// 
			this.groupAuthenticate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupAuthenticate.Controls.Add(this.groupNewAuthUser);
			this.groupAuthenticate.Controls.Add(this.btAuthDelete);
			this.groupAuthenticate.Controls.Add(this.label7);
			this.groupAuthenticate.Controls.Add(this.ltAuthList);
			this.groupAuthenticate.Location = new System.Drawing.Point(0, 0);
			this.groupAuthenticate.Name = "groupAuthenticate";
			this.groupAuthenticate.Size = new System.Drawing.Size(496, 416);
			this.groupAuthenticate.TabIndex = 0;
			this.groupAuthenticate.TabStop = false;
			// 
			// groupNewAuthUser
			// 
			this.groupNewAuthUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupNewAuthUser.Controls.Add(this.txConfirmAuthPass);
			this.groupNewAuthUser.Controls.Add(this.txAuthPass);
			this.groupNewAuthUser.Controls.Add(this.txAuthUsername);
			this.groupNewAuthUser.Controls.Add(this.label9);
			this.groupNewAuthUser.Controls.Add(this.btClearAtuh);
			this.groupNewAuthUser.Controls.Add(this.btAddUpdateAuth);
			this.groupNewAuthUser.Controls.Add(this.label8);
			this.groupNewAuthUser.Controls.Add(this.lb43);
			this.groupNewAuthUser.Location = new System.Drawing.Point(248, 24);
			this.groupNewAuthUser.Name = "groupNewAuthUser";
			this.groupNewAuthUser.Size = new System.Drawing.Size(240, 136);
			this.groupNewAuthUser.TabIndex = 3;
			this.groupNewAuthUser.TabStop = false;
			this.groupNewAuthUser.Text = "User :";
			// 
			// txConfirmAuthPass
			// 
			this.txConfirmAuthPass.Location = new System.Drawing.Point(88, 80);
			this.txConfirmAuthPass.Name = "txConfirmAuthPass";
			this.txConfirmAuthPass.PasswordChar = '*';
			this.txConfirmAuthPass.Size = new System.Drawing.Size(144, 20);
			this.txConfirmAuthPass.TabIndex = 7;
			this.txConfirmAuthPass.Text = "";
			// 
			// txAuthPass
			// 
			this.txAuthPass.Location = new System.Drawing.Point(88, 56);
			this.txAuthPass.Name = "txAuthPass";
			this.txAuthPass.PasswordChar = '*';
			this.txAuthPass.Size = new System.Drawing.Size(144, 20);
			this.txAuthPass.TabIndex = 6;
			this.txAuthPass.Text = "";
			// 
			// txAuthUsername
			// 
			this.txAuthUsername.Location = new System.Drawing.Point(88, 32);
			this.txAuthUsername.Name = "txAuthUsername";
			this.txAuthUsername.Size = new System.Drawing.Size(144, 20);
			this.txAuthUsername.TabIndex = 5;
			this.txAuthUsername.Text = "";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(8, 80);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(88, 16);
			this.label9.TabIndex = 4;
			this.label9.Text = "Confirm Pass :";
			// 
			// btClearAtuh
			// 
			this.btClearAtuh.Location = new System.Drawing.Point(144, 104);
			this.btClearAtuh.Name = "btClearAtuh";
			this.btClearAtuh.Size = new System.Drawing.Size(88, 24);
			this.btClearAtuh.TabIndex = 9;
			this.btClearAtuh.Text = "Clear";
			this.btClearAtuh.Click += new System.EventHandler(this.btClearAtuh_Click);
			// 
			// btAddUpdateAuth
			// 
			this.btAddUpdateAuth.Location = new System.Drawing.Point(48, 104);
			this.btAddUpdateAuth.Name = "btAddUpdateAuth";
			this.btAddUpdateAuth.Size = new System.Drawing.Size(88, 24);
			this.btAddUpdateAuth.TabIndex = 8;
			this.btAddUpdateAuth.Text = "Add/Update";
			this.btAddUpdateAuth.Click += new System.EventHandler(this.btAddUpdateAuth_Click);
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 56);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 16);
			this.label8.TabIndex = 1;
			this.label8.Text = "Password :";
			// 
			// lb43
			// 
			this.lb43.Location = new System.Drawing.Point(8, 32);
			this.lb43.Name = "lb43";
			this.lb43.Size = new System.Drawing.Size(88, 16);
			this.lb43.TabIndex = 0;
			this.lb43.Text = "Username :";
			// 
			// btAuthDelete
			// 
			this.btAuthDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btAuthDelete.Location = new System.Drawing.Point(256, 352);
			this.btAuthDelete.Name = "btAuthDelete";
			this.btAuthDelete.Size = new System.Drawing.Size(120, 24);
			this.btAuthDelete.TabIndex = 2;
			this.btAuthDelete.Text = "Delete User";
			this.btAuthDelete.Click += new System.EventHandler(this.btAuthDelete_Click);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 16);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(144, 16);
			this.label7.TabIndex = 1;
			this.label7.Text = "Username List :";
			// 
			// ltAuthList
			// 
			this.ltAuthList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ltAuthList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.clmTunnelUsername});
			this.ltAuthList.ContextMenu = this.AuthDeleteMenu;
			this.ltAuthList.FullRowSelect = true;
			this.ltAuthList.GridLines = true;
			this.ltAuthList.Location = new System.Drawing.Point(8, 32);
			this.ltAuthList.MultiSelect = false;
			this.ltAuthList.Name = "ltAuthList";
			this.ltAuthList.Size = new System.Drawing.Size(232, 352);
			this.ltAuthList.TabIndex = 0;
			this.ltAuthList.View = System.Windows.Forms.View.Details;
			this.ltAuthList.SelectedIndexChanged += new System.EventHandler(this.ltAuthList_SelectedIndexChanged);
			// 
			// clmTunnelUsername
			// 
			this.clmTunnelUsername.Text = "Username";
			this.clmTunnelUsername.Width = 192;
			// 
			// AuthDeleteMenu
			// 
			this.AuthDeleteMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						   this.menuDeleteUser});
			this.AuthDeleteMenu.Popup += new System.EventHandler(this.AuthDeleteMenu_Popup);
			// 
			// menuDeleteUser
			// 
			this.menuDeleteUser.Index = 0;
			this.menuDeleteUser.Text = "Delete User";
			this.menuDeleteUser.Click += new System.EventHandler(this.menuDeleteUser_Click);
			// 
			// nIcon
			// 
			this.nIcon.Text = "";
			this.nIcon.DoubleClick += new System.EventHandler(this.nIcon_DoubleClick);
			// 
			// WarpTunnelServer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(506, 424);
			this.Controls.Add(this.tabControls);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "WarpTunnelServer";
			this.Text = " Warp Tunnel Servers v 1.4.7.0";
			this.Resize += new System.EventHandler(this.WarpTunnelServer_Resize);
			this.Load += new System.EventHandler(this.WarpTunnelServer_Load);
			this.Closed += new System.EventHandler(this.WarpTunnelServer_Closed);
			this.Activated += new System.EventHandler(this.WarpTunnelServer_Activated);
			this.tabControls.ResumeLayout(false);
			this.tabServer.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupServer.ResumeLayout(false);
			this.tabOptions.ResumeLayout(false);
			this.groupOptions.ResumeLayout(false);
			this.groupLogger.ResumeLayout(false);
			this.tabAuthenticate.ResumeLayout(false);
			this.groupAuthenticate.ResumeLayout(false);
			this.groupNewAuthUser.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) 
		{
			WarpTunnelServer startApp = new WarpTunnelServer();
			
			if(args != null && args.Length == 1)
			{
				if(args[0].ToLower() == "-s")
					startApp.ifStartAtStartup = true;
			}

			Application.Run(startApp);

		}

		private void WarpTunnelServer_Closed(object sender, System.EventArgs e)
		{
			this.saveLoggerSettings();

			this.nIcon.Dispose();

			Process.GetCurrentProcess().Close();
			Process.GetCurrentProcess().Kill();
		}

		private void btStartServers_Click(object sender, System.EventArgs e)
		{
			startServers();
		}

		private void startServers()
		{	
			if(this.chkLogger.Checked)
			{
				if(this.cmbLoggerLevel.SelectedIndex == 0)
					Logger.setLogErrorOnly(true);
				else
					Logger.setLogErrorOnly(false);

				Logger.setLoggerEnabled(true);
				Logger.startLoggerStream("");
			}

			this.groupOptions.Enabled = false;
			this.groupServer.Enabled = false;
			this.groupLogger.Enabled = false;
			this.groupAuthenticate.Enabled = false;

			saveServersSettings();

			ServersLoader.startServersFromSettingsFile(Application.StartupPath + @"\" + "ServersSettings.xml",Application.StartupPath + @"\" + "AuthData.xml");
			this.btStartServers.Enabled = false;
			this.btStopServers.Enabled = true;
			this.btStartService.Enabled = false;
			this.btRestartService.Enabled = false;
			this.btStopService.Enabled = false;

			updateServerState(false);
		}

		private void btStopServers_Click(object sender, System.EventArgs e)
		{
			ServersLoader.stopServers();

			if(this.chkLogger.Checked)
			{
				Logger.setLoggerEnabled(false);
				Logger.stopLoggerStream();
			}

			this.groupLogger.Enabled = true;
			this.groupOptions.Enabled = true;
			this.groupServer.Enabled = true;
			this.groupAuthenticate.Enabled = true;

			this.btStartServers.Enabled = true;
			this.btStopServers.Enabled = false;
			this.btStartService.Enabled = true;
			this.btRestartService.Enabled = false;
			this.btStopService.Enabled = false;

			updateServerState(false);
		}

		private void btClear_Click(object sender, System.EventArgs e)
		{
			this.txServerName.Text = "";
			this.txLocPort.Text = "";
			this.txLocAddress.Text = "";
			this.txFowPort.Text = "";
			this.txFowAddress.Text = "";
		}

		private void btSaveServer_Click(object sender, System.EventArgs e)
		{
			if(	this.txServerName.Text.Trim() != "" &&
				this.txLocPort.Text.Trim() != "" &&
				this.txLocAddress.Text.Trim() != "" &&
				this.txFowPort.Text.Trim() != "" &&
				this.txFowAddress.Text.Trim() != "")
			{
				this.memorizedServersSettings();
			} 
			else 
			{
				MessageBox.Show(this,"Incorrect Input Parameter(s) !",this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		private void ltMenu_Popup(object sender, System.EventArgs e)
		{
			if(this.ltServers.Items.Count > 0 && this.ltServers.SelectedItems.Count > 0)
				this.nmuDelServer.Enabled = true;
			else
				this.nmuDelServer.Enabled = false;
		}

		private void nmuDelServer_Click(object sender, System.EventArgs e)
		{
			this.deletememorizedServersSetting(this.ltServers.SelectedItems[0].Text);
		}

		private void ltServers_ItemActivate(object sender, System.EventArgs e)
		{
			if(this.ltServers.SelectedItems.Count > 0)
			{
				int itemIndex = this.ltServers.SelectedItems[0].Index;
				this.txServerName.Text = this.serversSetting[itemIndex].me;
				this.txLocPort.Text = Convert.ToString(this.serversSetting[itemIndex].port);
				this.txLocAddress.Text = this.serversSetting[itemIndex].host;
				this.txFowPort.Text = Convert.ToString(this.serversSetting[itemIndex].forward_port);
				this.txFowAddress.Text = this.serversSetting[itemIndex].forward_host;
				this.chkCompatible.Checked = this.serversSetting[itemIndex].ifCompatible;
				this.radioSoftThread.Checked = this.serversSetting[itemIndex].serverThreadingMode == Tunnel.Thread_Mode.SoftThreading?true:false;
				this.radioHeavyThread.Checked = !this.radioSoftThread.Checked;
			}
		}

		private void btStartService_Click(object sender, System.EventArgs e)
		{
			this.btStartServers.Enabled = false;
			this.btStopServers.Enabled = false;
			this.btStartService.Enabled = false;
			this.btRestartService.Enabled = false;
			this.btStopService.Enabled = false;

			saveServersSettings();

			try 
			{
				if(SeviceUtils.startService(WarpTunnelServer.srvName,WarpTunnelServer.srvDescr,Application.StartupPath + WarpTunnelServer.srvPartPathFile,true))
				{
					this.btStartServers.Enabled = false;
					this.btStopServers.Enabled = false;
					this.btStartService.Enabled = false;
					this.btRestartService.Enabled = true;
					this.btStopService.Enabled = true;
					this.timerServerState.Enabled = false;
				} 
				else 
				{
					this.btStartServers.Enabled = true;
					this.btStopServers.Enabled = false;
					this.btStartService.Enabled = true;
					this.btRestartService.Enabled = false;
					this.btStopService.Enabled = false;
				}
			} 
			catch (Exception ex)
			{
				MessageBox.Show(this,ex.Message,this.Text);

				this.btStartServers.Enabled = true;
				this.btStopServers.Enabled = false;
				this.btStartService.Enabled = true;
				this.btRestartService.Enabled = false;
				this.btStopService.Enabled = false;
			}

			updateServerState(true);
		}

		private void btRestartService_Click(object sender, System.EventArgs e)
		{
			this.btStartServers.Enabled = false;
			this.btStopServers.Enabled = false;
			this.btStartService.Enabled = false;
			this.btRestartService.Enabled = false;
			this.btStopService.Enabled = false;

			SeviceUtils.restartService(WarpTunnelServer.srvName,WarpTunnelServer.srvDescr,Application.StartupPath + WarpTunnelServer.srvPartPathFile);

			this.btStartServers.Enabled = false;
			this.btStopServers.Enabled = false;
			this.btStartService.Enabled = false;
			this.btRestartService.Enabled = true;
			this.btStopService.Enabled = true;

			updateServerState(true);
		}

		private void btStopService_Click(object sender, System.EventArgs e)
		{
			try 
			{
				SeviceUtils.stopService(WarpTunnelServer.srvName,true);
			} 
			catch (Exception ex)
			{
				MessageBox.Show(this,ex.Message,this.Text);
			}

			this.btStartServers.Enabled = true;
			this.btStopServers.Enabled = false;
			this.btStartService.Enabled = true;
			this.btRestartService.Enabled = false;
			this.btStopService.Enabled = false;
			this.timerServerState.Enabled = true;

			updateServerState(true);
		}

		private void timerServerState_Tick(object sender, System.EventArgs e)
		{
			if(SeviceUtils.isServiceRunning(WarpTunnelServer.srvName))
				updateServerState(true);
			else
				updateServerState(false);
		}

		private void chkLogger_CheckedChanged(object sender, System.EventArgs e)
		{
			this.cmbLoggerLevel.Enabled = this.chkLogger.Checked;
		}

		private void txLocPort_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int temp = Convert.ToInt32(this.txLocPort.Text);
			} 
			catch
			{
				this.txLocPort.Text = "0";
			}


			if(this.txLocPort.Text  == "")
				this.txLocPort.Text = "0";
		}

		private void txFowPort_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int temp = Convert.ToInt32(this.txFowPort.Text);
			} 
			catch
			{
				this.txFowPort.Text = "0";
			}


			if(this.txFowPort.Text  == "")
				this.txFowPort.Text = "0";
		}

		private void load_auth_data()
		{
			this.tunnelAuth.loadAuthData(Application.StartupPath + @"\" + "AuthData.xml");
		}

		private void delete_username_from_auth(string username)
		{
			this.tunnelAuth.delete_user(username);
		}

		private void refreshAuthList()
		{
			this.ltAuthList.BeginUpdate();
			this.ltAuthList.Items.Clear();

			foreach(WarpTunnelAuthData.WarpTunnelUsersDataRow curr_row in this.tunnelAuth.AuthData.WarpTunnelUsersData)
			{
				this.ltAuthList.Items.Add(curr_row.Username);
			}

			this.ltAuthList.EndUpdate();
		}

		private void btAuthDelete_Click(object sender, System.EventArgs e)
		{
			this.deleteselectUserName();
		}

		private void deleteselectUserName()
		{
			if(this.ltAuthList.SelectedItems.Count > 0)
			{
				for(int i = 0; i < this.ltAuthList.SelectedItems.Count; i++)
				{
					delete_username_from_auth(this.ltAuthList.SelectedItems[i].Text);
				}

				refreshAuthList();
			}
		}

		private void btClearAtuh_Click(object sender, System.EventArgs e)
		{
			this.txAuthUsername.Text = "";
			this.txAuthPass.Text = "";
			this.txConfirmAuthPass.Text = "";
		}

		private void btAddUpdateAuth_Click(object sender, System.EventArgs e)
		{
			if((this.txAuthPass.Text == this.txConfirmAuthPass.Text) && this.txAuthPass.Text.Length > 0 && this.txConfirmAuthPass.Text.Length > 0)
			{
				this.tunnelAuth.addUpdateUserPass(this.txAuthUsername.Text,this.txAuthPass.Text);
				this.txAuthUsername.Text = "";
				this.txAuthPass.Text = "";
				this.txConfirmAuthPass.Text = "";
				refreshAuthList();
			} 
			else
			{
				MessageBox.Show(this,"Invalid inserted password !!",this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		private void tabControls_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.tabControls.SelectedIndex == 2)
				this.refreshAuthList();
		}

		private void ltAuthList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.ltAuthList.SelectedItems.Count > 0)
			{
				this.txAuthUsername.Text = this.ltAuthList.SelectedItems[0].Text;
				this.txAuthPass.Text = "";
				this.txConfirmAuthPass.Text = "";
			}
		}

		private void WarpTunnelServer_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(this.ifStartAtStartup)
				{
					this.WindowState = FormWindowState.Minimized;
					this.startServers();
				}

				if(Process.GetProcessesByName("WarpTunnelServerGUI").Length > 1)
				{
					MessageBox.Show(this,"Another instance of WarpTunnelServerGUI is already running on this machine. You cannot run TWO instances at a time.",this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
					this.Close();
				}

			} 
			catch
			{
			}

			GC.Collect();
		}

		private void menuDeleteUser_Click(object sender, System.EventArgs e)
		{
			this.deleteselectUserName();
		}

		private void AuthDeleteMenu_Popup(object sender, System.EventArgs e)
		{
			if(this.ltAuthList.SelectedItems.Count > 0)
			{
				this.menuDeleteUser.Enabled = true;
			} 
			else
			{
				this.menuDeleteUser.Enabled = false;
			}
		
		}

		private void WarpTunnelServer_Resize(object sender, System.EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				if(this.Visible)
					this.Visible = false;

				if(!this.nIcon.Visible)
					this.nIcon.Visible = true;
			}
			else if(this.WindowState == FormWindowState.Maximized || this.WindowState == FormWindowState.Normal)
			{
				if(!this.Visible)
					this.Visible = true;

				if(this.nIcon.Visible)
					this.nIcon.Visible = false;
			}
		}

		private void nIcon_DoubleClick(object sender, System.EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				if(!this.Visible)
					this.Visible = !this.Visible;

				this.WindowState = FormWindowState.Normal;
			} 
			else
			{
				if(this.Visible)
					this.Visible = !this.Visible;

				this.WindowState = FormWindowState.Minimized;
			}

		}

		private void WarpTunnelServer_Activated(object sender, System.EventArgs e)
		{
			if(this.ifStartAtStartup)
			{
				this.Visible = false;
				this.ifStartAtStartup = false;
			}
		}


	}
}
